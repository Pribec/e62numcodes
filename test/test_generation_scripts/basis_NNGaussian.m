clc;
val = @(x,y,z,sx,sy,sz) exp(-(x^2/sx+y^2/sy+z^2/sz) ) ;



sx=4.34;
sy=2.54;
sz=1.65;

disp('TEST(BasisFunc, NNGaussians){ ')
disp(['NNGaussians<Vec3d> g({',num2str(sx),',',num2str(sy),',',num2str(sz),'});'])
for x=[0.45,1.55]
for y=[0.22,5.4]
for z=[2.4,0.98]
disp(['EXPECT_NEAR(',num2str(val(x,y,z,sx,sy,sz),'%5.7f'),...
    ',g({', num2str(x),',', num2str(y),',', num2str(z),'}), 1e-6);'])
end
end
end
disp('}');

disp('TEST(BasisFunc, NNGaussiansFirstDerivatives){ ')
p='xyz';
disp(['NNGaussians<Vec3d> g({',num2str(sx),',',num2str(sy),',',num2str(sz),'});'])
for i = 1:3 %1:25
    syms x y z
    d='0,0,0';
    d(2*i-1)='1';
    f=eval(['diff(val,',p(i),')']);
    x=1.4;y=4.2;z=2.3;
    f=eval(subs(f));
    
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
    ',g({', num2str(x),',', num2str(y),',', num2str(z),'},{',d,'}), 1e-6);'])
end    
disp('}');

disp('TEST(BasisFunc, NNGaussiansSecondDerivatives){ ')
p='xyz';
disp(['NNGaussians<Vec3d> g({',num2str(sx),',',num2str(sy),',',num2str(sz),'});'])
for i = 1:3 %1:25
    syms x y z
    d='0,0,0';
    d(2*i-1)='2';
    f=eval(['diff(diff(val,',p(i),'),',p(i),')']);
    x=1.4;y=4.2;z=2.3;
    f=eval(subs(f));
    
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
    ',g({', num2str(x),',', num2str(y),',', num2str(z),'},{',d,'}), 1e-6);'])
end  
disp('}');

disp('TEST(BasisFunc, NNGaussiansMixedDerivatives){ ')
p='xyz';
disp(['NNGaussians<Vec3d> g({',num2str(sx),',',num2str(sy),',',num2str(sz),'});'])

    syms x y z
    d='1,1,0';
    f=eval(['diff(diff(val,',p(1),'),',p(2),')']);
    x=1.4;y=4.2;z=2.3;
    f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
    ',g({', num2str(x),',', num2str(y),',', num2str(z),'},{',d,'}), 1e-6);'])

    syms x y z
    d='1,0,1';
    f=eval(['diff(diff(val,',p(1),'),',p(3),')']);
    x=1.4;y=4.2;z=2.3;
    f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
    ',g({', num2str(x),',', num2str(y),',', num2str(z),'},{',d,'}), 1e-6);'])

    syms x y z
    d='0,1,1';
    f=eval(['diff(diff(val,',p(2),'),',p(3),')']);
    x=1.4;y=4.2;z=2.3;
    f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
    ',g({', num2str(x),',', num2str(y),',', num2str(z),'},{',d,'}), 1e-6);'])

disp('}');
