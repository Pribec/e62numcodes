#include "domain.hpp"

#include "gtest/gtest.h"

namespace mm {

TEST(Domain, RectangleArea) {
    RectangleDomain<Vec1d> domain1d({1.0}, {2.0});
    RectangleDomain<Vec2d> domain2d({1, 3}, {1.2, 3.5});
    RectangleDomain<Vec3d> domain3d({0, 1, 2}, {-1, 2, 3});
    // Test default and copy constructor
    RectangleDomain<Vec3d> domain3d_ = RectangleDomain<Vec3d>({0, 1, 2}, {-1, 2, 3});
    EXPECT_DOUBLE_EQ(2, std::abs(domain1d.surface_area()));
    EXPECT_DOUBLE_EQ(1.4, std::abs(domain2d.surface_area()));
    EXPECT_DOUBLE_EQ(6, std::abs(domain3d.surface_area()));
    EXPECT_DOUBLE_EQ(6, std::abs(domain3d_.surface_area()));
}

TEST(Domain, CircleArea) {
    CircleDomain<Vec1d> cd1({3}, 1);
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    CircleDomain<Vec3d> cd3({4, -2, 0}, 4);
    CircleDomain<Vec3d> cd3_ = CircleDomain<Vec3d>({4, -2, 0}, 4);

    EXPECT_DOUBLE_EQ(2, std::abs(cd1.surface_area()));
    EXPECT_DOUBLE_EQ(2 * M_PI * 5, std::abs(cd2.surface_area()));
    EXPECT_DOUBLE_EQ(4 * M_PI * 4 * 4, std::abs(cd3.surface_area()));
    EXPECT_DOUBLE_EQ(4 * M_PI * 4 * 4, std::abs(cd3_.surface_area()));
}

TEST(Domain, RectangleVolume) {
    RectangleDomain<Vec1d> domain1d({1.0}, {2.0});
    RectangleDomain<Vec2d> domain2d({1, 3}, {1.2, 3.5});
    RectangleDomain<Vec3d> domain3d({0, 1, 2}, {-1, 2, 3});

    EXPECT_DOUBLE_EQ(1, std::abs(domain1d.volume()));
    EXPECT_DOUBLE_EQ(0.2 * 0.5, std::abs(domain2d.volume()));
    EXPECT_DOUBLE_EQ(1, std::abs(domain3d.volume()));
}

TEST(Domain, CircleVolume) {
    CircleDomain<Vec1d> cd1({3}, 1);
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    CircleDomain<Vec3d> cd3({4, -2, 0}, 4);

    EXPECT_DOUBLE_EQ(2, std::abs(cd1.volume()));
    EXPECT_DOUBLE_EQ(M_PI * 5 * 5, std::abs(cd2.volume()));
    EXPECT_DOUBLE_EQ(4.0 / 3 * M_PI * 4 * 4 * 4, std::abs(cd3.volume()));
}

TEST(Domain, RectangleContainsBasic) {
    RectangleDomain<Vec1d> domain1d({1.0}, {2.0});
    RectangleDomain<Vec2d> domain2d({1, 3}, {1.2, 3.5});
    RectangleDomain<Vec3d> domain3d({0, 1, 2}, {-1, 2, 3});

    EXPECT_TRUE(domain1d.contains({1.5}));
    EXPECT_TRUE(domain1d.contains({1.01}));
    EXPECT_TRUE(domain1d.contains({1.99}));
    EXPECT_TRUE(domain1d.contains({1}));
    EXPECT_TRUE(domain1d.contains({2}));
    EXPECT_TRUE(domain1d.contains({1 - 1e-13}));  // thick
    EXPECT_TRUE(domain1d.contains({2 + 1e-13}));
    EXPECT_FALSE(domain1d.contains({3}));
    EXPECT_FALSE(domain1d.contains({0}));
    EXPECT_FALSE(domain1d.contains({-1}));

    EXPECT_TRUE(domain2d.contains({1.1, 3.4}));
    EXPECT_FALSE(domain2d.contains({0.9, 3.4}));
    EXPECT_FALSE(domain2d.contains({1.1, 2.3}));
    EXPECT_FALSE(domain2d.contains({0.6, -1.4}));

    EXPECT_TRUE(domain3d.contains({-0.5, 1.5, 2.5}));
    EXPECT_TRUE(domain3d.contains({0, 1, 2}));
    EXPECT_FALSE(domain3d.contains({-1.5, 1.5, 2.5}));
    EXPECT_FALSE(domain3d.contains({-0.5, 2.5, 2.5}));
    EXPECT_FALSE(domain3d.contains({-0.5, 1.5, 3.5}));
}

TEST(Domain, CircleContainsBasic) {
    CircleDomain<Vec1d> cd1({3}, 1);
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    CircleDomain<Vec3d> cd3({4, -2, 0}, 4);
    EXPECT_TRUE(cd1.contains({3.5}));
    EXPECT_TRUE(cd1.contains({4}));
    EXPECT_FALSE(cd1.contains({1}));
    EXPECT_FALSE(cd1.contains({5}));

    EXPECT_TRUE(cd2.contains({-0.9, 1.8}));
    EXPECT_TRUE(cd2.contains({-1.1, 2.2}));
    EXPECT_TRUE(cd2.contains({0, 3}));
    EXPECT_TRUE(cd2.contains({4, 2}));
    EXPECT_TRUE(cd2.contains({4 + 1e-13, 2}));  // thick
    EXPECT_FALSE(cd2.contains({-8, 3}));
    EXPECT_FALSE(cd2.contains({12, 3}));

    EXPECT_TRUE(cd3.contains({4, 2, 0}));
    EXPECT_TRUE(cd3.contains({4, -2, 3}));
    EXPECT_FALSE(cd3.contains({10, 3, 4}));
    EXPECT_FALSE(cd3.contains({-5, 3, 0}));
}

TEST(Domain, ContainsWithObstacles) {
    // domains with holes
    RectangleDomain<Vec2d> rd({-1, 1}, {5, 3});
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    rd.fillUniformBoundary({20, 5});
    cd2.subtract(rd);

    EXPECT_FALSE(cd2.contains({1, 2}));
    EXPECT_FALSE(cd2.contains({3.9, 2}));
    EXPECT_TRUE(cd2.contains({3, 3.5}));
    EXPECT_TRUE(cd2.contains({0, 4}));

    RectangleDomain<Vec2d> domain2d({0, 0}, {4, 3});
    domain2d.subtract(RectangleDomain<Vec2d>(Vec2d{0, 0}, Vec2d{1, 1}));
    domain2d.subtract(CircleDomain<Vec2d>(Vec2d{4, 3}, 2));
    EXPECT_TRUE(domain2d.contains({2, 1}));
    EXPECT_FALSE(domain2d.contains({0.5, 0.5}));
    EXPECT_FALSE(domain2d.contains({3, 2}));
}

TEST(Domain, ContainsUnion) {
    RectangleDomain<Vec2d> rd({-1, 1}, {5, 3});
    rd = RectangleDomain<Vec2d>({0, 0}, {4, 3});
    rd.subtract(RectangleDomain<Vec2d>(Vec2d{0, 0}, Vec2d{1, 1}));
    rd.subtract(CircleDomain<Vec2d>(Vec2d{4, 3}, 2));
    CircleDomain<Vec2d> circle_rect_union({4, 3}, 1);
    circle_rect_union.add(rd);
    EXPECT_TRUE(circle_rect_union.contains({4.1, 3.3}));
    EXPECT_TRUE(circle_rect_union.contains({1, 2}));
    EXPECT_FALSE(circle_rect_union.contains({0.5, 0.5}))
        << "CHECK: Are normal vectors copied on clone?";
    EXPECT_FALSE(circle_rect_union.contains({3, 2}));
    EXPECT_FALSE(circle_rect_union.contains({6.2, 3}));
}

TEST(Domain, ContainsNested) {
    // test inclusion - exclusion
    RectangleDomain<Vec2d> inner({1.5, 1.5}, {2.5, 2.5});
    inner.fillUniform({10, 10}, {10, 10});
    CircleDomain<Vec2d> middle({2, 2}, 1);
    middle.fillUniformBoundary(20);
    RectangleDomain<Vec2d> outer({0, 0}, {4, 4});
    outer.fillUniform({20, 20}, {20, 20});
    outer.subtract(middle);
    outer.add(inner);
    assert(outer.contains({1, 1}));
    assert(!outer.contains({1.2, 2}));
    assert(outer.contains({2, 2}));

    // test inclusion - exclusion
    inner.fillUniform({10, 10}, {10, 10});
    middle = CircleDomain<Vec2d>({2, 2}, 1);
    middle.fillUniform(80, 40);
    middle.subtract(inner);
    outer = RectangleDomain<Vec2d>({0, 0}, {4, 4});
    outer.fillUniform({50, 50}, {50, 50});
    outer.subtract(middle);
    assert(outer.contains({1, 1}));
    assert(!outer.contains({1.2, 2}));
    assert(outer.contains({2, 2}));
}

TEST(Domain, RectangleFillUniform) {
    // fill with count
    RectangleDomain<Vec1d> domain1d(Vec1d(0.0), {1});
    domain1d.fillUniform({3}, {40});
    Range<Vec1d> expected = {0.25, 0.5, 0.75};
    Range<Vec1d> actual = domain1d.getInternalNodes();
    ASSERT_EQ(expected.size(), actual.size());
    for (int i = 0; i < expected.size(); ++i) EXPECT_DOUBLE_EQ(expected[i][0], actual[i][0]);

    expected = {0., 1.};
    actual = domain1d.getBoundaryNodes();
    ASSERT_EQ(expected.size(), actual.size());
    for (int i = 0; i < expected.size(); ++i) EXPECT_DOUBLE_EQ(expected[i][0], actual[i][0]);

    // fill with count
    RectangleDomain<Vec2d> domain2d({0, 0}, {1, 1});
    domain2d.fillUniform({1, 1}, {3, 3});
    Range<Vec2d> expected2 = {{0.5, 0.5}};
    Range<Vec2d> actual2 = domain2d.getInternalNodes();
    ASSERT_EQ(expected2.size(), actual2.size());
    for (int i = 0; i < expected2.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected2[i][0], actual2[i][0]);
        EXPECT_DOUBLE_EQ(expected2[i][1], actual2[i][1]);
    }

    expected2 = {{0, 0}, {0, 0.5}, {0, 1}, {0.5, 0}, {0.5, 1}, {1, 0}, {1, 0.5}, {1, 1}};
    actual2 = domain2d.getBoundaryNodes();
    std::sort(actual2.begin(), actual2.end());
    ASSERT_EQ(expected2.size(), actual2.size());
    for (int i = 0; i < expected2.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected2[i][0], actual2[i][0]);
        EXPECT_DOUBLE_EQ(expected2[i][1], actual2[i][1]);
    }

    // fill with step
    domain2d = RectangleDomain<Vec2d>({0, 0}, {1, 1});
    domain2d.fillUniformWithStep(0.5, 0.5);
    expected2 = {{0.5, 0.5}};
    actual2 = domain2d.getInternalNodes();
    ASSERT_EQ(expected2.size(), actual2.size());
    for (int i = 0; i < expected2.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected2[i][0], actual2[i][0]);
        EXPECT_DOUBLE_EQ(expected2[i][1], actual2[i][1]);
    }

    expected2 = {{0, 0}, {0, 0.5}, {0, 1}, {0.5, 0}, {0.5, 1}, {1, 0}, {1, 0.5}, {1, 1}};
    actual2 = domain2d.getBoundaryNodes();
    std::sort(actual2.begin(), actual2.end());
    ASSERT_EQ(expected2.size(), actual2.size());
    for (int i = 0; i < expected2.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected2[i][0], actual2[i][0]);
        EXPECT_DOUBLE_EQ(expected2[i][1], actual2[i][1]);
    }

    RectangleDomain<Vec3d> domain3d({0., 0., 0.}, {1., 1., 1.});
    domain3d.fillUniform({1, 1, 1}, {3, 3, 3});
    Range<Vec3d> expected3 = {{0.5, 0.5, 0.5}};
    Range<Vec3d> actual3 = domain3d.getInternalNodes();
    ASSERT_EQ(expected3.size(), actual3.size());
    for (int i = 0; i < expected3.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected3[i][0], actual3[i][0]);
        EXPECT_DOUBLE_EQ(expected3[i][1], actual3[i][1]);
        EXPECT_DOUBLE_EQ(expected3[i][2], actual3[i][2]);
    }

    expected3 = {{0, 0, 0}, {0, 0, 0.5}, {0, 0, 1},
                 {0, 0.5, 0}, {0, 0.5, 0.5}, {0, 0.5, 1},
                 {0, 1, 0}, {0, 1, 0.5}, {0, 1, 1},
                 {0.5, 0, 0}, {0.5, 0, 0.5}, {0.5, 0, 1},
                 {0.5, 0.5, 0}             , {0.5, 0.5, 1},
                 {0.5, 1, 0}, {0.5, 1, 0.5}, {0.5, 1, 1},
                 {1, 0, 0}, {1, 0, 0.5}, {1, 0, 1},
                 {1, 0.5, 0}, {1, 0.5, 0.5}, {1, 0.5, 1},
                 {1, 1, 0}, {1, 1, 0.5}, {1, 1, 1}};
    actual3 = domain3d.getBoundaryNodes();
    std::sort(actual3.begin(), actual3.end());
    ASSERT_EQ(expected3.size(), actual3.size());
    for (int i = 0; i < expected3.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected3[i][0], actual3[i][0]);
        EXPECT_DOUBLE_EQ(expected3[i][1], actual3[i][1]);
        EXPECT_DOUBLE_EQ(expected3[i][2], actual3[i][2]);
    }
}

TEST(Domain, CircleFillUniform) {
    CircleDomain<Vec2d> cd2({0, 0}, 1);
    cd2.fillUniformBoundary(4);
    Range<Vec2d> expected2 = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
    Range<Vec2d> actual2 = cd2.getBoundaryNodes();
    ASSERT_EQ(expected2.size(), actual2.size());
    for (int i = 0; i < expected2.size(); ++i) {
        EXPECT_NEAR(expected2[i][0], actual2[i][0], 1e-15);
        EXPECT_NEAR(expected2[i][1], actual2[i][1], 1e-15);
    }

    // fill with step
    cd2.fillUniformBoundaryWithStep(M_PI / 2);
    expected2 = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
    actual2 = cd2.getBoundaryNodes();
    ASSERT_EQ(expected2.size(), actual2.size());
    for (int i = 0; i < expected2.size(); ++i) {
        EXPECT_NEAR(expected2[i][0], actual2[i][0], 1e-15);
        EXPECT_NEAR(expected2[i][1], actual2[i][1], 1e-15);
    }
}

TEST(Domain, RectangleUniformConsistency) {
    RectangleDomain<Vec1d> domain1d({1.0}, {2.0});
    RectangleDomain<Vec2d> domain2d({1, 3}, {1.2, 3.5});
    RectangleDomain<Vec3d> domain3d({0, 1, 2}, {-1, 2, 3});

    domain1d.fillUniformInterior({10});
    EXPECT_TRUE(domain1d.valid());
    domain2d.fillUniformInterior({10, 10});
    EXPECT_TRUE(domain2d.valid());
    domain3d.fillUniformInterior({10, 10, 10});
    EXPECT_TRUE(domain3d.valid());
    domain1d.fillUniformBoundary({12});
    EXPECT_TRUE(domain1d.valid());
    domain2d.fillUniformBoundary({12, 12});
    EXPECT_TRUE(domain2d.valid());
    domain3d.fillUniformBoundary({12, 12, 12});
    EXPECT_TRUE(domain3d.valid());
}

TEST(Domain, CircleUniformConsistency) {
    CircleDomain<Vec1d> cd1({3}, 1);
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    CircleDomain<Vec3d> cd3({4, -2, 0}, 4);

    // test consistency of fills
    cd1.fillUniformBoundary(100);
    EXPECT_TRUE(cd1.valid());
    cd2.fillUniformBoundary(100);
    EXPECT_TRUE(cd2.valid());
    cd3.fillUniformBoundary(1000);
    EXPECT_TRUE(cd3.valid());
    cd1.fillUniformInterior(100);
    EXPECT_TRUE(cd1.valid());
    cd2.fillUniformInterior(100);
    EXPECT_TRUE(cd2.valid());
    cd3.fillUniformInterior(1000);
    EXPECT_TRUE(cd3.valid());
}

TEST(Domain, RandomRectangleConsistency) {
    RectangleDomain<Vec1d> domain1d({1.0}, {2.0});
    RectangleDomain<Vec2d> domain2d({1, 3}, {1.2, 3.5});
    RectangleDomain<Vec3d> domain3d({0, 1, 2}, {-1, 2, 3});

    domain1d.fillRandomInterior(100);
    EXPECT_TRUE(domain1d.valid());
    domain2d.fillRandomInterior(100);
    EXPECT_TRUE(domain2d.valid());
    domain3d.fillRandomInterior(100);
    EXPECT_TRUE(domain3d.valid());

    domain1d.fillRandomBoundary(100);
    EXPECT_TRUE(domain1d.valid());
    domain2d.fillRandomBoundary(100);
    EXPECT_TRUE(domain2d.valid());
    domain3d.fillRandomBoundary(100);
    EXPECT_TRUE(domain3d.valid());
}

TEST(Domain, RandomCircleConsistency) {
    CircleDomain<Vec1d> cd1({3}, 1);
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    CircleDomain<Vec3d> cd3({4, -2, 0}, 4);

    cd1.fillRandomInterior(100);
    EXPECT_TRUE(cd1.valid());
    cd2.fillRandomInterior(100);
    EXPECT_TRUE(cd2.valid());
    cd3.fillRandomInterior(1000);
    EXPECT_TRUE(cd3.valid());
    cd1.fillRandomBoundary(100);
    EXPECT_TRUE(cd1.valid());
    cd2.fillRandomBoundary(100);
    EXPECT_TRUE(cd2.valid());
    cd3.fillRandomBoundary(1000);
    EXPECT_TRUE(cd3.valid());
}

TEST(Domain, RandomFillsRectangle) {
    RectangleDomain<Vec1d> domain1d({1.0}, {2.0});
    domain1d.fillRandom(10, 10);

    RectangleDomain<Vec2d> domain2d({1, 3}, {1.2, 3.5});
    domain2d.fillRandom(10, 10);

    RectangleDomain<Vec3d> domain3d({0, 1, 2}, {-1, 2, 3});
    domain3d.fillRandom(10, 10);
}
TEST(Domain, RandomFillsCircle) {
    CircleDomain<Vec1d> cd1({3}, 1);
    cd1.fillRandom(10, 10);

    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    cd2.fillRandom(10, 10);

    CircleDomain<Vec3d> cd3({4, -2, 0}, 4);
    cd3.fillRandom(10, 10);
}

template<typename vec_t>
void testRectangleNormals(const RectangleDomain<vec_t>& domain) {
    double tol = 1e-15;
    for (int i : domain.types < 0) {
        int on_bnd = 0;
        for (int j = 0; j < domain.dim; ++j) {
            if (std::abs(domain.positions[i][j] - domain.beg[j]) < tol) on_bnd++;
            if (std::abs(domain.positions[i][j] - domain.end[j]) < tol) on_bnd++;
        }
        if (on_bnd == 1) {  // uniquely defined normal
            for (int j = 0; j < domain.dim; ++j) {
                if (std::abs(domain.positions[i][j] - domain.beg[j]) < tol) {
                    EXPECT_EQ(-1, domain.normal(i)[j]);
                } else if (std::abs(domain.positions[i][j] - domain.end[j]) < tol) {
                    EXPECT_EQ(1, domain.normal(i)[j]);
                } else {
                    EXPECT_EQ(0, domain.normal(i)[j]);
                }
            }
        }
    }
}

TEST(Domain, RectangleNormalsUniform) {
    RectangleDomain<Vec1d> domain1d({1}, {2});
    domain1d.fillUniform({1}, {3});
    testRectangleNormals(domain1d);

    RectangleDomain<Vec2d> domain2d({1, 3}, {1.2, 3.5});
    domain2d.fillUniform({1, 1}, {3, 3});
    testRectangleNormals(domain2d);

    RectangleDomain<Vec3d> domain3d({0, 1, 2}, {-1, 2, 3});
    domain3d.fillUniform({1, 1, 1}, {3, 3, 3});
    testRectangleNormals(domain3d);
}

TEST(Domain, RectangleNormalsRandom) {
    RectangleDomain<Vec1d> domain1d({1}, {2});
    domain1d.fillRandom(20, 20);
    testRectangleNormals(domain1d);

    RectangleDomain<Vec2d> domain2d({1, 3}, {1.2, 3.5});
    domain2d.fillRandom(50, 50);
    testRectangleNormals(domain2d);

    RectangleDomain<Vec3d> domain3d({0, 1, 2}, {-1, 2, 3});
    domain3d.fillRandom(60, 60);
    testRectangleNormals(domain3d);
}

TEST(Domain, CircleNormalsRandom) {
    CircleDomain<Vec1d> cd1({3}, 1);
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    CircleDomain<Vec3d> cd3({4, -2, 0}, 4);

    cd1.fillRandomInterior(20);
    cd1.fillRandomBoundary(20);
    double tol = 1e-15;
    for (int i : cd1.types < 0) {
        Vec1d n = (cd1.positions[i] - cd1.center).normalized();
        EXPECT_NEAR(n[0], cd1.normal(i)[0], tol);
    }
    cd2.fillRandomInterior(100);
    cd2.fillRandomBoundary(100);
    for (int i : cd2.types < 0) {
        Vec2d n = (cd2.positions[i] - cd2.center).normalized();
        EXPECT_NEAR(n[0], cd2.normal(i)[0], tol);
        EXPECT_NEAR(n[1], cd2.normal(i)[1], tol);
    }

    cd3.fillRandomInterior(100);
    cd3.fillRandomBoundary(100);
    for (int i : cd3.types < 0) {
        Vec3d n = (cd3.positions[i] - cd3.center).normalized();
        EXPECT_NEAR(n[0], cd3.normal(i)[0], tol);
        EXPECT_NEAR(n[1], cd3.normal(i)[1], tol);
        EXPECT_NEAR(n[2], cd3.normal(i)[2], tol);
    }
}

TEST(Domain, CircleNormalsUniform) {
    CircleDomain<Vec1d> cd1({3}, 1);
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    CircleDomain<Vec3d> cd3({4, -2, 0}, 4);

    cd1.fillUniform(20, 20);
    double tol = 1e-15;
    for (int i : cd1.types < 0) {
        Vec1d n = (cd1.positions[i] - cd1.center).normalized();
        EXPECT_NEAR(n[0], cd1.normal(i)[0], tol);
    }
    cd1.fillUniform(50, 50);
    for (int i : cd2.types < 0) {
        Vec2d n = (cd2.positions[i] - cd2.center).normalized();
        EXPECT_NEAR(n[0], cd2.normal(i)[0], tol);
        EXPECT_NEAR(n[1], cd2.normal(i)[1], tol);
    }

    cd3.fillUniform(100, 100);
    for (int i : cd3.types < 0) {
        Vec3d n = (cd3.positions[i] - cd3.center).normalized();
        EXPECT_NEAR(n[0], cd3.normal(i)[0], tol);
        EXPECT_NEAR(n[1], cd3.normal(i)[1], tol);
        EXPECT_NEAR(n[2], cd3.normal(i)[2], tol);
    }
}

TEST(Domain, CircleDeletesOldOnRefill) {
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    cd2.fillUniformInterior(100);
    int size = cd2.positions.size();
    cd2.fillUniformInterior(50);
    EXPECT_EQ(size - 50, cd2.positions.size());

    cd2.fillUniformBoundary(100);
    size = cd2.positions.size();
    cd2.fillUniformBoundary(50);
    EXPECT_EQ(size - 50, cd2.positions.size());

    cd2.fillRandomInterior(100);
    size = cd2.positions.size();
    cd2.fillRandomInterior(50);
    EXPECT_EQ(size - 50, cd2.positions.size());

    cd2.fillRandomBoundary(100);
    size = cd2.positions.size();
    cd2.fillRandomBoundary(50);
    EXPECT_EQ(size - 50, cd2.positions.size());
}

TEST(Domain, RectangleDeletesOldOnRefill) {
    RectangleDomain<Vec2d> rd2({1, 3}, {1.2, 3.5});
    rd2.fillUniformInterior({10, 10});
    rd2.fillUniformInterior({50, 50});
    EXPECT_EQ(50u * 50, rd2.positions.size());

    int size = rd2.positions.size();
    rd2.fillUniformBoundary(100);
    rd2.fillUniformBoundary(50);
    EXPECT_EQ(size + 4 * 50 - 4, rd2.positions.size());

    rd2.fillRandomInterior(100);
    size = rd2.positions.size();
    rd2.fillRandomInterior(50);
    EXPECT_EQ(size - 50, rd2.positions.size());

    rd2.fillRandomBoundary(100);
    size = rd2.positions.size();
    rd2.fillRandomBoundary(50);
    EXPECT_EQ(size - 50, rd2.positions.size());
}

TEST(Domain, Size) {
    RectangleDomain<Vec1d> domain({1.0}, {2.0});
    EXPECT_EQ(0u, domain.size());
    domain.positions = {1., 2., 3., 4.};
    EXPECT_EQ(4u, domain.size());
}

TEST(Domain, ContainsPrecision) {
    // test contains precision
    RectangleDomain<Vec1d> domain1d({1.0}, {2.0});
    domain1d.fillUniformInterior({10});
    ASSERT_GT(domain1d.getContainsPrecision(), 1e-8);
    ASSERT_LT(domain1d.getContainsPrecision(), 1e-6);
    domain1d.fillUniformInterior({100});
    ASSERT_GT(domain1d.getContainsPrecision(), 1e-9);
    ASSERT_LT(domain1d.getContainsPrecision(), 1e-7);
    domain1d.fillUniformInterior({1000});
    ASSERT_GT(domain1d.getContainsPrecision(), 1e-10);
    ASSERT_LT(domain1d.getContainsPrecision(), 1e-8);

    CircleDomain<Vec1d> domain1dlarge({0}, 10000);
    domain1dlarge.fillUniform(100, 100);
    ASSERT_GT(domain1dlarge.getContainsPrecision(), 1e-4);
    ASSERT_LT(domain1dlarge.getContainsPrecision(), 1e-3);
}

TEST(Domain, ClearTest) {
    CircleDomain<Vec2d> cd2({4, 0}, 4);
    cd2.fillUniform(20, 42);
    cd2.clear();
    EXPECT_TRUE(cd2.positions.empty());
    EXPECT_TRUE(cd2.types.empty());
    EXPECT_TRUE(cd2.support.empty());
    EXPECT_TRUE(cd2.distances.empty());

    cd2 = CircleDomain<Vec2d>({4, -2}, 4);
    cd2.fillUniform(20, 42);
    cd2.clearBoundaryNodes();
    EXPECT_EQ(20u, cd2.positions.size());
    EXPECT_EQ(20u, cd2.types.size());
    EXPECT_EQ(20u, cd2.support.size());
    EXPECT_EQ(20u, cd2.distances.size());

    cd2 = CircleDomain<Vec2d>({-2, 0}, 4);
    cd2.fillUniform(2, 2);
    cd2.support = {{1, 2}, {0, 3}, {0, 1}, {2, 3}};  // random values
    cd2.distances = {{0, 1}, {4, 5}, {3, 5}, {3, 2}};  // random values
    cd2.clearBoundaryNodes();
    EXPECT_EQ(2u, cd2.positions.size());
    EXPECT_EQ(2u, cd2.types.size());
    EXPECT_EQ(2u, cd2.support.size());
    EXPECT_EQ(2u, cd2.distances.size());

    cd2 = CircleDomain<Vec2d>({4, -2}, 4);
    cd2.fillUniform(20, 42);
    cd2.clearInternalNodes();
    EXPECT_EQ(42u, cd2.positions.size());
    EXPECT_EQ(42u, cd2.types.size());
    EXPECT_EQ(42u, cd2.support.size());
    EXPECT_EQ(42u, cd2.distances.size());

    cd2 = CircleDomain<Vec2d>({-2, 0}, 4);
    cd2.fillUniform(2, 2);
    cd2.support = {{1, 2}, {0, 3}, {0, 1}, {2, 3}};  // random values
    cd2.distances = {{0, 1}, {4, 5}, {3, 5}, {3, 2}};  // random values
    cd2.clearInternalNodes();
    EXPECT_EQ(2u, cd2.positions.size());
    EXPECT_EQ(2u, cd2.types.size());
    EXPECT_EQ(2u, cd2.support.size());
    EXPECT_EQ(2u, cd2.distances.size());
}

TEST(Domain, RectangleCloneEqual) {  // copying -- check that copies are independant
    RectangleDomain<Vec2d> rd({0, 0}, {4, 3});
    rd.fillRandom(5, 5);
    rd.add(RectangleDomain<Vec2d>({1, 1}, {5, 5}));

    auto rd_copy = Domain<Vec2d>::makeClone(rd);
    EXPECT_EQ(rd.positions, rd_copy.positions);
    EXPECT_EQ(rd.types, rd_copy.types);
    EXPECT_EQ(rd.support, rd_copy.support);
    EXPECT_EQ(rd.distances, rd_copy.distances);
    EXPECT_EQ(rd.getBBox(), rd_copy.getBBox());
    EXPECT_EQ(rd.child_domains.size(), rd_copy.child_domains.size());
    EXPECT_EQ(rd.getContainsPrecision(), rd_copy.getContainsPrecision());
    EXPECT_EQ(rd.getThickness(), rd_copy.getThickness());
    EXPECT_EQ(rd.beg, rd_copy.beg);
    EXPECT_EQ(rd.end, rd_copy.end);
}

TEST(Domain, CircleCloneEqual) {
    CircleDomain<Vec2d> cd({0, 0}, 1);
    cd.fillRandom(5, 5);
    cd.add(RectangleDomain<Vec2d>({1, 1}, {5, 5}));

    auto cd_copy = Domain<Vec2d>::makeClone(cd);
    EXPECT_EQ(cd.positions, cd_copy.positions);
    EXPECT_EQ(cd.types, cd_copy.types);
    EXPECT_EQ(cd.getBBox(), cd_copy.getBBox());
    EXPECT_EQ(cd.support, cd_copy.support);
    EXPECT_EQ(cd.distances, cd_copy.distances);
    EXPECT_EQ(cd.child_domains.size(), cd_copy.child_domains.size());
    EXPECT_EQ(cd.getContainsPrecision(), cd_copy.getContainsPrecision());
    EXPECT_EQ(cd.getThickness(), cd_copy.getThickness());
    EXPECT_EQ(cd.center, cd_copy.center);
    EXPECT_EQ(cd.radius, cd_copy.radius);
}

TEST(Domain, CloneIndependent) {  // copying -- check that copies are independent
    RectangleDomain<Vec2d> rd({0, 0}, {4, 3});
    rd.subtract(RectangleDomain<Vec2d>(Vec2d{0, 0}, Vec2d{1, 1}));
    rd.subtract(CircleDomain<Vec2d>(Vec2d{4, 3}, 2));
    CircleDomain<Vec2d> circle_rect_union({4, 3}, 1);
    circle_rect_union.add(rd);
    auto circle_rect_union_copy = Domain<Vec2d>::makeClone(circle_rect_union);
    EXPECT_EQ(circle_rect_union.positions, circle_rect_union_copy.positions);
    EXPECT_EQ(circle_rect_union_copy.child_domains.size(), circle_rect_union.child_domains.size());
    EXPECT_EQ(circle_rect_union.types, circle_rect_union_copy.types);
    EXPECT_TRUE(circle_rect_union_copy.valid());
    circle_rect_union_copy.add(CircleDomain<Vec2d>(Vec2d{-1, -1}, 1));
    EXPECT_EQ(circle_rect_union.child_domains.size() + 1,
              circle_rect_union_copy.child_domains.size());
    EXPECT_FALSE(circle_rect_union.contains({-1, -1}));
    EXPECT_TRUE(circle_rect_union_copy.contains({-1, -1}));
    EXPECT_TRUE(circle_rect_union_copy.contains({4.1, 3.3}));
    EXPECT_TRUE(circle_rect_union_copy.contains({1, 2}));
    EXPECT_FALSE(circle_rect_union_copy.contains({0.5, 0.5}));
    EXPECT_FALSE(circle_rect_union_copy.contains({3, 2}));
    EXPECT_FALSE(circle_rect_union_copy.contains({6.2, 3}));

    circle_rect_union_copy.positions.push_back({2, 3});
    circle_rect_union_copy.types.push_back(1);
    EXPECT_EQ(circle_rect_union_copy.positions.size(), circle_rect_union.positions.size() + 1);
    EXPECT_EQ(circle_rect_union_copy.types.size(), circle_rect_union.types.size() + 1);

    circle_rect_union.positions.push_back({1, 1});
    circle_rect_union.types.push_back(1);
    circle_rect_union.positions.push_back({1, 1});
    circle_rect_union.types.push_back(1);
    EXPECT_EQ(circle_rect_union.positions.size(), circle_rect_union_copy.positions.size() + 1);
    EXPECT_EQ(circle_rect_union_copy.types.size() + 1, circle_rect_union.types.size());
}

TEST(Domain, Subtract) {  // 2D obstacle
    CircleDomain<Vec2d> circ({2, 1}, 1);
    circ.addBoundaryPoint({2 - std::sqrt(2) / 2, 1 - std::sqrt(2) / 2}, -1, {1, -1});
    circ.addPoint({1.5, 0.5}, 1);
    circ.addBoundaryPoint({3, 1}, -1, {0.4, 6.5});
    circ.addPoint({2, 1.5}, 1);
    EXPECT_TRUE(circ.valid());
    RectangleDomain<Vec2d> rect({0, 0}, {2, 1});
    rect.addPoint({1, 0.5}, 1);
    rect.addBoundaryPoint({1.5, 0}, -1, {1, 0.8});
    rect.addPoint({1.5, 0.5}, 1);
    rect.addBoundaryPoint({2, 0.5}, -1, {0.7, 1});
    EXPECT_TRUE(rect.valid());
    EXPECT_EQ(0u, rect.child_domains.size());
    Range<int> mapping = rect.subtract(circ);
    Range<int> expected = {2, -1, -1, -1};
    EXPECT_EQ(expected, mapping);
    EXPECT_TRUE(rect.valid());
    EXPECT_EQ(3u, rect.positions.size());
    EXPECT_EQ(1u, rect.child_domains.size());
    EXPECT_TRUE(rect.child_domains.front()->positions.empty());
    EXPECT_TRUE(rect.child_domains.front()->types.empty());
    EXPECT_TRUE(rect.child_domains.front()->child_domains.empty());
    Range<Vec2d> expected_boundary = {{1.5, 0}, {2 - std::sqrt(2) / 2, 1 - std::sqrt(2) / 2}};
    Range<Vec2d> boundary = rect.getBoundaryNodes();
    EXPECT_EQ(expected_boundary, expected_boundary);
    Range<Vec2d> expected_interior = {{1, 0.5}};
    Range<Vec2d> interior = rect.getInternalNodes();
    EXPECT_EQ(interior, expected_interior);
}

TEST(Domain, Add) {  // making a union in 2d
    RectangleDomain<Vec2d> rect({0, 0}, {2, 1});
    rect.addPoint({1, 0.5}, 1);
    rect.addBoundaryPoint({1.5, 0}, -1, {1, 0.8});
    rect.addPoint({1.5, 0.5}, 1);
    rect.addBoundaryPoint({2, 0.5}, -1, {0.7, 1});
    EXPECT_TRUE(rect.valid());
    CircleDomain<Vec2d> union_test({2, 1}, 1);
    EXPECT_TRUE(union_test.valid());
    EXPECT_EQ(0u, union_test.child_domains.size());
    union_test.addBoundaryPoint({2 - std::sqrt(2) / 2, 1 - std::sqrt(2) / 2}, -1, {1, -1});
    union_test.addPoint({1.5, 0.5}, 1);
    union_test.addBoundaryPoint({3, 1}, -1, {0.4, 6.5});
    union_test.addPoint({2, 1.5}, 1);
    EXPECT_TRUE(union_test.valid());
    EXPECT_EQ(0u, union_test.child_domains.size());
    union_test.add(rect);
    EXPECT_TRUE(union_test.valid());
    EXPECT_EQ(1u, union_test.child_domains.size());
    EXPECT_EQ(0u, union_test.child_domains.back()->child_domains.size());
    EXPECT_EQ(0u, union_test.child_domains.back()->types.size());
    EXPECT_EQ(0u, union_test.child_domains.back()->positions.size());
    Range<Vec2d> expected_boundary = {{3, 1}, {1.5, 0}};
    Range<Vec2d> boundary = union_test.getBoundaryNodes();
    EXPECT_EQ(expected_boundary, boundary);
    Range<Vec2d> expected_interior = {{1.5, 0.5}, {2, 1.5}, {1, 0.5}, {1.5, 0.5}};
    Range<Vec2d> interior = union_test.getInternalNodes();
    EXPECT_EQ(interior, expected_interior);
}

TEST(Domain, AddNormals) {
    Vec2d c1(1., 1.);
    CircleDomain<Vec2d> d(c1, 1);
    d.fillUniformBoundary(20);
    d.types = -2;
    Vec2d c2(1., 0.);
    CircleDomain<Vec2d> d2(c2, 1);
    d2.fillUniformBoundary(20);

    d.add(d2);

    double tol = 1e-15;
    for (int i = 0; i < d.size(); ++i) {
        Vec2d c = (d.types[i] == -2) ? c1 : c2;
        Vec2d expected = (d.positions[i] - c).normalized();
        EXPECT_NEAR(expected[0], d.normal(i)[0], tol);
        EXPECT_NEAR(expected[1], d.normal(i)[1], tol);
    }
}

TEST(Domain, SubtractNormals) {
    Vec2d c1(1., 1.);
    CircleDomain<Vec2d> d(c1, 1);
    d.fillUniformBoundary(20);
    d.types = -2;
    Vec2d c2(1., 0.);
    CircleDomain<Vec2d> d2(c2, 1);
    d2.fillUniformBoundary(20);

    d.subtract(d2);

    double tol = 1e-15;
    for (int i = 0; i < d.size(); ++i) {
        Vec2d c = (d.types[i] == -2) ? c1 : c2;
        Vec2d expected = (d.positions[i] - c).normalized();
        if (d.types[i] == -1) expected = -expected;
        EXPECT_NEAR(expected[0], d.normal(i)[0], tol);
        EXPECT_NEAR(expected[1], d.normal(i)[1], tol);
    }
}

//  TEST(Domain, Plot) { // Merging and removing 3D
//      CircleDomain<Vec3d> test1({1, 1, 1}, 0.5);
//      test1.fillUniformBoundary(400);
//      test1.fillUniformInterior(400);
//      CircleDomain<Vec3d> test5({2, 0, 0}, 0.5);
//      test5.fillUniformBoundary(400);
//      test5.fillUniformInterior(400);
//      RectangleDomain<Vec3d> test2({0, 0, 0}, {2, 2, 2});
//      test2.fillUniformBoundary({22, 22, 22});
//      test2.fillUniformInterior({20, 20, 20});
//      test2.subtract(test1);
//      test2.subtract(test5);

//      CircleDomain<Vec3d> test4({2, 0, 2}, 0.5);
//      test4.fillUniformBoundary(400);
//      test4.fillUniformInterior(400);
//      test4.add(test2);

//      std::cerr << test4.getMatlabData() << std::endl;
//  }

TEST(Domain, UnionBorderSpecification) {
    RectangleDomain<Vec2d> border_test({0, 0}, {1, 1});
    RectangleDomain<Vec2d> border_test1({1, 0}, {2, 1});
    border_test1.fillUniform({1, 1}, {3, 3});

    border_test.fillUniform({1, 1}, {3, 3});
    border_test.add(border_test1, BOUNDARY_TYPE::SINGLE);
    EXPECT_EQ(13u, border_test.getBoundaryNodes().size());

    border_test = RectangleDomain<Vec2d>({0, 0}, {1, 1});
    border_test.fillUniform({1, 1}, {3, 3});
    border_test.add(border_test1, BOUNDARY_TYPE::DOUBLE);
    EXPECT_EQ(16u, border_test.getBoundaryNodes().size());

    border_test = RectangleDomain<Vec2d>({0, 0}, {1, 1});
    border_test.fillUniform({1, 1}, {3, 3});
    border_test.add(border_test1, BOUNDARY_TYPE::NONE);
    EXPECT_EQ(10u, border_test.getBoundaryNodes().size());
}

TEST(Domain, ObstacleBorderSpecification) {
    RectangleDomain<Vec2d> border_test({0, 0}, {1, 1});
    RectangleDomain<Vec2d> border_test1({1, 0}, {2, 1});
    border_test1.fillUniform({1, 1}, {3, 3});

    border_test = RectangleDomain<Vec2d>({0, 0}, {1, 1});
    border_test.fillUniform({1, 1}, {3, 3});
    border_test.subtract(border_test1, BOUNDARY_TYPE::SINGLE);
    EXPECT_EQ(8u, border_test.getBoundaryNodes().size());

    border_test = RectangleDomain<Vec2d>({0, 0}, {1, 1});
    border_test.fillUniform({1, 1}, {3, 3});
    border_test.subtract(border_test1, BOUNDARY_TYPE::DOUBLE);
    EXPECT_EQ(11u, border_test.getBoundaryNodes().size());

    border_test = RectangleDomain<Vec2d>({0, 0}, {1, 1});
    border_test.fillUniform({1, 1}, {3, 3});
    border_test.subtract(border_test1, BOUNDARY_TYPE::NONE);
    EXPECT_EQ(5u, border_test.getBoundaryNodes().size());
}

TEST(Domain, BBox) {
    RectangleDomain<Vec2d> d(Vec2d(0.), Vec2d(1.));
    EXPECT_EQ(std::make_pair(Vec2d(0, 0), Vec2d(1, 1)), d.getBBox());
    RectangleDomain<Vec2d> d2(Vec2d(1.), Vec2d(0.));
    EXPECT_EQ(std::make_pair(Vec2d(0, 0), Vec2d(1, 1)), d2.getBBox());
    CircleDomain<Vec2d> c(2., 1.);
    EXPECT_EQ(std::make_pair(Vec2d(1, 1), Vec2d(3, 3)), c.getBBox());

    d.subtract(d2);
    EXPECT_EQ(std::make_pair(Vec2d(0, 0), Vec2d(1, 1)), d.getBBox());
    d.add(c);
    EXPECT_EQ(std::make_pair(Vec2d(0, 0), Vec2d(3, 3)), d.getBBox());

    RectangleDomain<Vec1d> d1d(Vec1d(0.), Vec1d(1.));
    EXPECT_EQ(std::make_pair(Vec1d(0.0), Vec1d(1.0)), d1d.getBBox());
    RectangleDomain<Vec1d> d1d2(Vec1d(-1.), Vec1d(0.));
    EXPECT_EQ(std::make_pair(Vec1d(-1.0), Vec1d(0.0)), d1d2.getBBox());
    d1d.add(d1d2);
    EXPECT_EQ(std::make_pair(Vec1d(-1.0), Vec1d(1.0)), d1d.getBBox());

    RectangleDomain<Vec3d> d3d(Vec3d(0.), Vec3d(1.));
    EXPECT_EQ(std::make_pair(Vec3d(0.0), Vec3d(1.0)), d3d.getBBox());
    RectangleDomain<Vec3d> d3d2(Vec3d(2.), Vec3d(0.));
    EXPECT_EQ(std::make_pair(Vec3d(0.0), Vec3d(2.0)), d3d2.getBBox());
    d3d.add(d3d2);
    EXPECT_EQ(std::make_pair(Vec3d(0.0), Vec3d(2.0)), d3d.getBBox());
}

TEST(Domain, DeathTest) {
    RectangleDomain<Vec2d> d(Vec2d(0.), Vec2d(1.));
    EXPECT_DEATH(d.fillUniformBoundary({0, 2}), "All counts must be greater than 2.");
    EXPECT_DEATH(d.fillUniformBoundary({2, 0}), "All counts must be greater than 2.");
    EXPECT_DEATH(CircleDomain<Vec3d>(2., -1.), "Circle radius must be greater than 0.");
    EXPECT_DEATH(CircleDomain<Vec3d>(2., 0.), "Circle radius must be greater than 0.");
}

TEST(Domain, DISABLED_DeathStar) {
    CircleDomain<Vec3d> death_star({0, 0, 0}, 5);
    death_star.fillUniform(10000, 1000);
    CircleDomain<Vec3d> biteoff({4.6, 5, 0}, 3.24);
    biteoff.fillUniform(400, 400);
    death_star.subtract(biteoff);
}

TEST(Domain, DISABLED_SphereInt) {
    CircleDomain<Vec3d> d(0., 1.);
    int n = 10000;
    for (int i = 1; i < n; ++i) {
        d.fillUniformInterior(i);
        std::cout << i << ' ' << d.positions.size() << std::endl;
    }
}

}  // namespace mm
