#include "domain_extended.hpp"

#include "gtest/gtest.h"

namespace mm {

TEST(Domain, FindSupportAll) {
    RectangleDomain<Vec2d> d({0, 0}, {1, 1});
    d.fillUniform({10, 10}, {10, 10});
    d.findSupport(2, d.positions.size());  // should get everything
    int num = d.positions.size();
    Range<int> expected(num);
    for (int i = 0; i < num; ++i) expected[i] = i;
    for (auto& x : d.support) {
        sort(x.begin(), x.end());
        ASSERT_EQ(expected.size(), x.size());
        EXPECT_EQ(expected, x);
    }
    d.findSupport(d.positions.size());  // should get everything
    for (auto& x : d.support) {
        sort(x.begin(), x.end());
        ASSERT_EQ(expected.size(), x.size());
        EXPECT_EQ(expected, x);
    }
}

// see img/test_case_domain_support.png
TEST(Domain, FindSupportNoLimit) {
    RectangleDomain<Vec2d> d({0, 0}, {3, 3});
    d.fillUniform({2, 2}, {4, 4});
    d.findSupport(2, 10);  // should not be limiting
    int num = d.positions.size();
    std::map<Vec2d, Range<Vec2d>> expected({
        {{0, 0}, {{0, 0}, {0, 1}, {1, 0}, {1, 1}}},
        {{1, 0}, {{0, 0}, {0, 1}, {1, 0}, {1, 1}, {2, 0}, {2, 1}}},
        {{2, 0}, {{1, 0}, {1, 1}, {2, 0}, {2, 1}, {3, 0}, {3, 1}}},
        {{3, 0}, {{2, 0}, {2, 1}, {3, 0}, {3, 1}}},
        {{0, 1}, {{0, 0}, {0, 1}, {0, 2}, {1, 0}, {1, 1}, {1, 2}}},
        {{1, 1}, {{0, 0}, {0, 1}, {0, 2}, {1, 0}, {1, 1}, {1, 2}, {2, 0}, {2, 1}, {2, 2}}},
        {{2, 1}, {{1, 0}, {1, 1}, {1, 2}, {2, 0}, {2, 1}, {2, 2}, {3, 0}, {3, 1}, {3, 2}}},
        {{3, 1}, {{2, 0}, {2, 1}, {2, 2}, {3, 0}, {3, 1}, {3, 2}}},
        {{0, 2}, {{0, 1}, {0, 2}, {0, 3}, {1, 1}, {1, 2}, {1, 3}}},
        {{1, 2}, {{0, 1}, {0, 2}, {0, 3}, {1, 1}, {1, 2}, {1, 3}, {2, 1}, {2, 2}, {2, 3}}},
        {{2, 2}, {{1, 1}, {1, 2}, {1, 3}, {2, 1}, {2, 2}, {2, 3}, {3, 1}, {3, 2}, {3, 3}}},
        {{3, 2}, {{2, 1}, {2, 2}, {2, 3}, {3, 1}, {3, 2}, {3, 3}}},
        {{0, 3}, {{0, 2}, {0, 3}, {1, 2}, {1, 3}}},
        {{1, 3}, {{0, 2}, {0, 3}, {1, 2}, {1, 3}, {2, 2}, {2, 3}}},
        {{2, 3}, {{1, 2}, {1, 3}, {2, 2}, {2, 3}, {3, 2}, {3, 3}}},
        {{3, 3}, {{2, 2}, {2, 3}, {3, 2}, {3, 3}}},
    });
    for (int i = 0; i < num; ++i) {
        Vec2d cur = d.positions[i];
        Range<Vec2d> a = d.positions[d.support[i]];
        sort(a.begin(), a.end());
        EXPECT_EQ(expected[d.positions[i]], a);
        Range<double> dist;
        for (const auto& v : expected[cur]) dist.push_back((cur - v).squaredNorm());
        sort(dist.begin(), dist.end());
        EXPECT_EQ(dist, d.distances[i]);
    }
}

// see img/test_case_domain_support.png
TEST(Domain, FindSupportLimit) {
    RectangleDomain<Vec2d> d({0, 0}, {3, 3});
    d.fillUniform({2, 2}, {4, 4});
    int limit = 5;
    d.findSupport(2, 5);  // should be limiting
    std::map<Vec2d, Range<Vec2d>> expected({
        {{1, 1}, {{0, 1}, {1, 0}, {1, 1}, {1, 2}, {2, 1}}},
        {{2, 1}, {{1, 1}, {2, 0}, {2, 1}, {2, 2}, {3, 1}}},
        {{1, 2}, {{0, 2}, {1, 1}, {1, 2}, {1, 3}, {2, 2}}},
        {{2, 2}, {{1, 2}, {2, 1}, {2, 2}, {2, 3}, {3, 2}}},
    });
    for (const auto& x : d.support) {
        EXPECT_LE(x.size(), limit);
    }
    for (int i : d.types > 0) {  // internals are well defined
        Vec2d cur = d.positions[i];
        Range<Vec2d> a = d.positions[d.support[i]];
        sort(a.begin(), a.end());
        EXPECT_EQ(expected[cur], a);
        Range<double> dist;
        for (const auto& v : expected[cur]) dist.push_back((cur - v).squaredNorm());
        sort(dist.begin(), dist.end());
        EXPECT_EQ(dist, d.distances[i]);
    }
}

TEST(Domain, FindSupportDistancesDouble) {
    RectangleDomain<Vec1d> d(Vec1d(0.2), Vec1d(0.3));
    d.fillUniformWithStep(0.1, 0.1);
    d.findSupport(2);
    Range<Range<double>> expected = {{0, 0.1*0.1}, {0, 0.1*0.1}};
    ASSERT_EQ(expected.size(), d.distances.size());
    for (int i = 0; i < d.distances.size(); ++i) {
        ASSERT_EQ(expected[i].size(), d.distances[i].size());
    }
    for (int i = 0; i < d.distances.size(); ++i) {
        for (int j = 0; j < d.distances[i].size(); ++j) {
            EXPECT_DOUBLE_EQ(expected[i][j], d.distances[i][j]);
        }
    }
}

TEST(Domain, FindSupportForWhich) {
    RectangleDomain<Vec2d> d({0, 0}, {1, 1});
    d.positions = {1., 2., 3., 4., 5., 6., 7.};
    d.types = {-1, 2, -1, 2, 2, -1, -1};
    d.findSupport(1, d.types == -1);
    Range<Range<int>> expected = {{0}, {}, {2}, {}, {}, {5}, {6}};
    EXPECT_EQ(expected, d.support);
}

TEST(Domain, FindSupportSearchAmong) {
    RectangleDomain<Vec2d> d({0, 0}, {1, 1});
    d.positions = {1., 2., 3., 4., 5., 6., 7.};
    d.types = {-1, 2, -1, 2, 2, -1, -1};
    d.findSupport(1, d.types != 0, d.types > 0);
    Range<Range<int>> expected = {{1}, {1}, {1}, {3}, {4}, {4}, {4}};
    EXPECT_EQ(expected, d.support);
}

TEST(Domain, FindSupportForceSelf) {
    RectangleDomain<Vec2d> d({0, 0}, {1, 1});
    d.positions = {1., 2., 3., 4., 5., 6., 7.};
    d.types = {-1, 2, -1, 2, 2, -1, -1};
    d.findSupport(1, d.types != 0, d.types > 0);
    Range<Range<int>> expected = {{1}, {1}, {1}, {3}, {4}, {4}, {4}};
    EXPECT_EQ(expected, d.support);
    d.findSupport(1, d.types != 0, d.types > 0, true);
    expected = {{0}, {1}, {2}, {3}, {4}, {5}, {6}};
    EXPECT_EQ(expected, d.support);
    d.findSupport(2, d.types != 0, d.types > 0, true);
    expected = {{0, 1}, {1, 3}, {2, 1}, {3, 4}, {4, 3}, {5, 4}, {6, 4}};
    EXPECT_EQ(expected, d.support);
}

TEST(Domain, FindSupportDeath) {
    RectangleDomain<Vec2d> d({0, 0}, {1, 1});
    d.fillUniform(19, 2);
    EXPECT_DEATH(d.findSupport(0, 12), "Support radius must be greater than 0");
    EXPECT_DEATH(d.findSupport(-5, 12), "Support radius must be greater than 0");
    EXPECT_DEATH(d.findSupport(1, -30), "Support size must be greater than 0");
    EXPECT_DEATH(d.findSupport(1, 0), "Support size must be greater than 0");
    EXPECT_DEATH(d.findSupport(366), "Support size \\(366) cannot exceed number of points that we "
            "are searching among \\(365).");
    EXPECT_DEATH(d.findSupport(65., 4, {1000}), "Index 1000 out of range \\[0, 365) in for_which.");
    EXPECT_DEATH(d.findSupport(65., 4, Range<int>{1}, Range<int>{5000}),
                 "Index 5000 out of range \\[0, 365) in search_among.");
}

TEST(Domain, DISABLED_Example) {
    /// [Domain creation and manipulation]
    // create domain
    RectangleDomain<Vec2d> domain({0, 0}, {1, 1});
    domain.fillUniformBoundary({50, 50});
    domain.fillUniformInterior({20, 20});
    CircleDomain<Vec2d> c({0.5, 0.5}, 0.25);
    c.fillUniformBoundary(70);
    domain.subtract(c);
    CircleDomain<Vec2d> cc({1, 1}, 0.25);
    cc.fillUniform(300, 70);
    domain.add(cc);
    std::cout << domain << std::endl;

    // do stuff with domain
    domain.volume();  // calculates basic domain volume
    domain.surface_area();  // calculates basic domain surface
    Range<Vec2d> pos = domain.getBoundaryNodes();  // get all boundary nodes
    domain.clearInternalNodes();  // remove internal nodes
    Vec2d lower, upper;
    std::tie(lower, upper) = domain.getBBox();  // get bounding box
    auto domain_copy = Domain<Vec2d>::makeClone(domain);  // deep copy
    /// [Domain creation and manipulation]
}

TEST(Domain, AddPointToBoundary1d) {
    CircleDomain<Vec1d> circ(2.2, 0.7);
    circ.fillUniform(3, 40);
    Range<Vec1d> positions = circ.positions;

    bool success;
    Vec1d added_point;
    Vec1d hint(1.5);
    std::tie(success, added_point) = circ.projectPointToBoundary(hint, {-1}, 1e-8);
    EXPECT_FALSE(success);
    EXPECT_EQ(positions, circ.positions);
}

TEST(Domain, AddPointToBoundary2d) {
    const double precision = 1e-5;

    /// [Add to boundary]
    Vec2d center(2.2, 1.3);
    CircleDomain<Vec2d> circ(center, 0.7);
    circ.fillUniform(3, 40);

    bool success;
    Vec2d added_point;
    Vec2d hint = {1.5, 0.8};
    Vec2d normal = (hint-center).normalized();
    std::tie(success, added_point) = circ.projectPointToBoundary(hint, normal, precision);
    /// [Add to boundary]

    EXPECT_TRUE(success);
    EXPECT_NEAR(1.6303909089, added_point[0], precision);
    EXPECT_NEAR(0.8931363635, added_point[1], precision);

    hint = {2.8, 1.6};
    normal = (hint-center).normalized();
    std::tie(success, added_point) = circ.projectPointToBoundary(hint, normal, precision);
    EXPECT_TRUE(success);
    EXPECT_NEAR(2.826098822, added_point[0], precision);
    EXPECT_NEAR(1.613049411, added_point[1], precision);

    hint = {-2.8, 1.6};
    normal = {1, 0};
    std::tie(success, added_point) = circ.projectPointToBoundary(hint, normal, precision);
    EXPECT_FALSE(success);
}

TEST(Domain, AddPointToBoundary3d) {
    CircleDomain<Vec3d> circ({0, 0, 0}, 1);
    circ.fillUniform(3, 150);
    double precision = 1e-2;

    bool success;
    Vec3d added_point;

    Vec3d hint = {0.8, 0.01, -0.01};
    std::tie(success, added_point) = circ.projectPointToBoundary(
            hint, hint.normalized(), precision/10);
    EXPECT_TRUE(success);
    EXPECT_NEAR(0.99947, added_point[0], precision);
    EXPECT_NEAR(0.0117135, added_point[1], precision);
    EXPECT_NEAR(-0.0105922, added_point[2], precision);
    EXPECT_NEAR(added_point.squaredNorm(), 1, precision);
}

}  // namespace mm
