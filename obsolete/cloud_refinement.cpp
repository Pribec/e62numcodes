#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "types.hpp"
#include <Eigen/Sparse>

using namespace mm;
using namespace Eigen;

/*
This file contains the various prototype refinement functions
initially developed for a circular geometry. The prototypes include
refinement by:
    - half links,
    - closest point
    - average of closest points
    - hybrid with closest point in interior, and half link
        at the boundaries

Some of the results of these functions can be found at the
following website:
http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/
wiki/index.php/Positioning_of_computational_nodes
*/

void RefineCircleRegion(Range<int> subregion, int nmax, int nbmax, CircleDomain<Vec2d> &domain,
                        int radius_size, double fraction);
bool PointFarEnough(Vec2d my_point, double radius, Range<Vec2d> all_points);
double GetRadius(double fraction, Range<double> distances);
void RefineCircleSubRegion(Range<int> subregion, int nmax, int nbmax, CircleDomain<Vec2d> &domain,
                           int radius_size, double fraction, bool verbose = true);
void RefineWithHalfLinks(const Range<int> subregion, int nmax, int nbmax, double fraction,
                            CircleDomain<Vec2d> &domain, bool verbose = true);
void RefineHybrid(const Range<int> subregion, int nmax, int nbmax, double fraction,
                            CircleDomain<Vec2d> &domain, bool verbose = true);

int main(int argc, char const *argv[])
{

    // number of refinements
    int nref = atoi(argv[1]);

    // support size
    int support_size = atoi(argv[2]);
    
    // 
    int radius_size = atoi(argv[3]);
    
    // radius fraction for refinement
    double radius_fraction = atof(argv[4]);

    int size = 200;

    Timer mytimer;

    CircleDomain<Vec2d> domain({0,0},1.0);
    domain.fillUniformInterior(size);
    domain.fillUniformBoundary(size/5);
    domain.relax(10, 1e-2, 1.0, 3, 500);

    

    Range<int> subreg = domain.positions.filter([](Vec2d p) {return p.norm() > 0.5; });
    
    Range<int> upleft = domain.positions.filter([](Vec2d p) {return p[0] <= 0 && p[1] > 0;});

    for (int n = 0; n <= nref; n++) {
        Range<int> all = domain.types != 0;
        Range<int> subreg = domain.positions.filter([](Vec2d p) {return p.norm() > 0.5; });
        Range<int> subreg2 = domain.positions.filter([](Vec2d p) {return p.norm() > 0.8; });
        Range<int> upleft = domain.positions.filter([](Vec2d p) {return p[0] <= 0 && p[1] > 0;});

        //RefineCircleRegion(domain.types != 0, 5, 3, domain, radius_size, radius_fraction);
        //if (n != 0) RefineCircleSubRegion(all, 6, 4, domain, radius_size, radius_fraction);
        //if (n != 0) RefineWithHalfLinks(all,radius_size,radius_size,radius_fraction,domain);
        //if (n != 0) RefineHybrid(all,radius_size,7,radius_fraction,domain);
    
    Range<int> border = domain.types == BOUNDARY;
    //if (n != 0) domain.refine(all,radius_size,radius_fraction);
    mytimer.addCheckPoint("start");
    
    Range<int> new_p;
    
    if (n == 1) domain.refine(all,radius_size,radius_fraction,new_p,true);
    if (n == 2) domain.refine(all,radius_size,radius_fraction,new_p,true);

    mytimer.addCheckPoint("refine");

    Range<int> new_internal;
    for (auto& p : new_p) {
        if (domain.types[p] > 0) {
            new_internal.push_back(p);
        }
    }
    12
    domain.relax(10, 1e-2, 1.0, 3, 10);
    //domain.relax(10, 1e-2, [] (const Vec2d& ) { return 1.0; }, 3, 100, new_internal);

    mytimer.addCheckPoint("relax"); 

    domain.findSupport(support_size);
    mytimer.addCheckPoint("find_support");

    double thiele = 1.0;
    Monomials<Vec2d> basis({{0,0},{1,0},{0,1},{2,0},{0,2}});

    // set interior and boundary flags
    Range<int> interior = domain.types == INTERNAL;
    Range<int> boundary = domain.types == BOUNDARY;

    // unknown concentration field and right-hand side
    VecXd C(domain.size(), 0);
    VecXd RHS(domain.size(),0);

    // set dirichlet boundary conditions
    RHS[boundary] = 1.0;

    // prepare shape functions and laplacian
    std::vector<Triplet<double>> coeff;
    for (auto& c : interior) {
        Range<Vec2d> supp_domain = domain.positions[domain.support[c]];
        EngineMLS<Vec2d, Monomials, Gaussians> MLS(basis,supp_domain,std::pow(domain.characteristicDistance(),2));
        // laplacian
        VecXd shape = MLS.getShapeAt(supp_domain[0], {{2, 0}}) +
                      MLS.getShapeAt(supp_domain[0], {{0, 2}});
        for (int i = 0; i < supp_domain.size(); ++i) {
            coeff.emplace_back(c, domain.support[c][i], shape[i]);
        }
    }

    // prepare reaction term
    std::vector<Triplet<double>> coeff2;
    for (auto& c : interior) {
        coeff2.emplace_back(c, c, -thiele*thiele);
    }

    // prepare dirichlet boundaries
    std::vector<Triplet<double>> coeff3;
    for (auto& c : boundary) {
        coeff3.emplace_back(c, c, 1.0);
    }

    // prepare matrices
    SparseMatrix<double> shape_laplace(domain.size(), domain.size());
    shape_laplace.setFromTriplets(coeff.begin(), coeff.end());
    
    SparseMatrix<double> reaction(domain.size(), domain.size());
    reaction.setFromTriplets(coeff2.begin(), coeff2.end());
    
    SparseMatrix<double> bound(domain.size(), domain.size());
    bound.setFromTriplets(coeff3.begin(), coeff3.end());

    mytimer.addCheckPoint("build_matrix");
    // draw
    std::thread th([&] { draw2D(domain.positions, C); });

    // initialize solver
    Eigen::BiCGSTAB<SparseMatrix<double>> BCGST;

    // join matrices together
    SparseMatrix<double> tmp = bound + shape_laplace + reaction;
    BCGST.compute(tmp);

    // solve system of equations
    C = BCGST.solve(RHS);
    //std::cout << "#iterations:     " << BCGST.iterations() << std::endl;
    //std::cout << "estimated error: " << BCGST.error()      << std::endl;

    mytimer.addCheckPoint("solve");
    mytimer.showTimings();

    std::ofstream results;
    std::string fname = "results_";
    fname += std::to_string(n);
    fname += ".txt";
    results.open(fname.c_str());
    for (int i = 0; i < domain.size(); i++) {
        results << domain.positions[i][0] << " " << domain.positions[i][1] << " " << domain.types[i] << " " << C[i] << "\n";
    }
    results.close();

    // end drawing
    th.join(); 
    
    std::cout << n << " " << domain.size() << " " << domain.characteristicDistance() << " " <<
        BCGST.iterations() << " " << BCGST.error() << std::endl;

    }
    return 0;
}

bool PointFarEnough(const Vec2d my_point, double radius, const Range<Vec2d> all_points) {

    bool too_close = false;
    for (auto& point : all_points) {
        Vec2d distance = my_point - point;
        if (distance.norm() < radius) {
            too_close = true;
            break;
        }
    }

    return !too_close;
}

// Returns the average radius multiplied by a given fraction
double GetRadius(const double fraction, const Range<double> distances) {
    
    assert(distances.size() > 1 && "Atleast 2 nodes should be in the support!");
    
    // calculate average radius
    double radius = 0;
    for (int i = 1; i < distances.size(); i++) {
        radius += std::sqrt(distances[i]);
    }
    radius /= (double)(distances.size()-1);

    // apply fraction of the calculated radius (usually 0.5)
    radius *= fraction;

    return radius;

}


void RefineCircleRegion(Range<int> subregion, int nmax, int nbmax, CircleDomain<Vec2d> &domain,
                        int radius_size, double fraction) {

    assert(subregion.size() > 0 && "No nodes to refine!");
    assert(nbmax <=3 && "Only 3 nodes supported for boundary refinement");
    assert((fraction < 1 && fraction > 0) && "Fraction should be between 0 and 1");

    // save initial support size
    int initial_support_size = domain.support[0].size();

    // find supports for purposes of boundary check
    domain.findSupport(7);

    // split subregion into boundary and internal
    Range<int> boundary;
    Range<int> interior;
    for (auto& c : subregion) {
        if (domain.types[c] < 0) {
            boundary.push_back(c);
        } else {
            interior.push_back(c);
        }
    }

    // iterate through interior nodes of subregion
    for (auto& c : interior) {
        Range<int> support = domain.support[c];
        // then through the supports
        for (auto& s : support) {
            //and check if their are any boundary nodes
            if (domain.types[s] < 0) {
                bool contains = false;
                // check if the boundary perhaps already contains that node
                for (auto& b : boundary) {
                    if (b == s) {
                        contains = true;
                        break;
                    }
                }
                // if not then add it to the subregion to be refined
                if (!contains) {
                    boundary.push_back(s);
                }
            }
        }
    }

    // prepare a random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 2*M_PI);

    // initialize containers for new points
    Range<Vec2d> new_points;
    Range<int> new_types;
    new_points.reserve(4*subregion.size());
    new_types.reserve(4*subregion.size());

    // find support for purposes of refinement
    domain.findSupport(radius_size);

    // print initial point before refinement
    {
        std::ofstream myfile;
        myfile.open("points.txt");
        Range<Vec2d> nodes = domain.positions;
        for (int i = 0; i < domain.size(); i++) {
            myfile << nodes[i][0] << " " << nodes[i][1] << " "
            << GetRadius(fraction,domain.distances[i]) << "\n";
        }
        myfile.close();
    }


    // iterate through all points in boundary
    for (int i = 0; i < boundary.size(); i++) {

        int c = boundary[i];

        // store parent point and support
        Vec2d parent = domain.positions[c];
        Range<int> support = domain.support[c];

        // calculate the refinement radius
        double radius = GetRadius(fraction,domain.distances[c]);


        
        for (int n = 0; n < nbmax; n++) {
            double angle = 2.0*std::acos(0.5*radius/domain.radius);
            double offset = std::atan2(parent[1],parent[0]);
            
            Vec2d child;
            if (n == 0) {
                child = domain.center + Vec2d{domain.radius*std::cos(offset+angle),domain.radius*std::sin(offset+angle)};
                if (new_points.empty() || PointFarEnough(child,radius,new_points)) {
                    new_points.push_back(child);
                    new_types.push_back(BOUNDARY);
                }
            } else if (n == 1) {
                child = domain.center + Vec2d{domain.radius*std::cos(offset-angle),domain.radius*std::sin(offset-angle)};
                if (PointFarEnough(child,radius,new_points)) {
                    new_points.push_back(child);
                    new_types.push_back(BOUNDARY);

                }
            } else if (n == 2) {
                double crad = domain.radius - radius;
                child = domain.center + Vec2d{crad*std::cos(offset),crad*std::sin(offset)};
                if (PointFarEnough(child,radius,new_points)) {
                    new_points.push_back(child);
                    new_types.push_back(INTERNAL);
                }
            }
        }
    }

    std::ofstream file;
    file.open("b_points.txt");
    for (int i = 0; i < new_points.size(); i++) {
        file << new_points[i][0] << " " << new_points[i][1] << "\n";
    }
    file.close();

    for (auto& c : interior) {

        // store parent point and support
        Vec2d parent = domain.positions[c];
        Range<int> support = domain.support[c];

        double radius = GetRadius(fraction,domain.distances[c]);

        // try to find nmax points around a given parent point
        int max_number_of_checks = 50;
        for (int n = 0; n < nmax; n++) {
            int niter = 0;
            bool point_found = false;
            while (!point_found && niter < max_number_of_checks) {

                // randomly select point on circle around main node
                double random_value = dis(gen);
                
                // calculate potential child 
                Vec2d child = parent
                    + Vec2d{radius*std::cos(random_value),radius*std::sin(random_value)};
                niter++;
                
                // plug in first new point
                if (new_points.size() == 0 || PointFarEnough(child,radius,new_points)) {
                    new_points.push_back(child);
                    new_types.push_back(INTERNAL);
                    point_found = true;


                } 
            }
        }
    }

    // push new points into domain
    domain.positions.append(new_points);
    domain.types.append(new_types);

    domain.findSupport(initial_support_size);

}


void RefineCircleSubRegion(const Range<int> subregion, int nmax, int nbmax, CircleDomain<Vec2d> &domain,
                           int radius_size, double fraction, bool verbose) {

    assert(subregion.size() > 0 && "No nodes to refine!");
    assert(nbmax <=4 && "Only 3 nodes supported for boundary refinement");
    assert((fraction < 1 && fraction > 0) && "Fraction should be between 0 and 1");

    // save initial support size
    int initial_support_size = domain.support[0].size();
    assert(radius_size < initial_support_size && "# of radius nodes is too large");

    // split subregion into boundary and internal
    Range<int> boundary;
    Range<int> interior;
    for (auto& c : subregion) {
        if (domain.types[c] < 0) {
            boundary.push_back(c);
        } else {
            interior.push_back(c);
        }
    }

    // This part checks if there are any extra boundary nodes that should also be refined
    // iterate through interior nodes of subregion
    for (auto& c : interior) {
        Range<int> support = domain.support[c];
        // then through the supports
        for (auto& s : support) {
            //and check if their are any boundary nodes
            if (domain.types[s] < 0) {
                bool contains = false;
                // check if the boundary perhaps already contains that node
                for (auto& b : boundary) {
                    if (b == s) {
                        contains = true;
                        break;
                    }
                }
                // if not then add it to the subregion to be refined
                if (!contains) {
                    boundary.push_back(s);
                }
            }
        }
    }

    // prepare a random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 2*M_PI);

    // initialize containers for new points
    
    // the range in range is for purposes of easier searching later on
    Range<Range<Vec2d>> new_points;
    Range<Range<int>> new_types;

    // reserve space
    new_points.reserve(domain.size());
    new_types.reserve(domain.size());

    // fill with empty ranges
    for (int i = 0; i < domain.size(); i++) {
        new_points.push_back(Range<Vec2d>());
        new_types.push_back(Range<int>());
    }

    // radius distance indexes
    Range<int> dist_id(radius_size + 1);
    for (int i = 0; i < radius_size + 1; i++) dist_id[i] = i;

    // print initial point before refinement
    if (verbose) {
        std::ofstream myfile;
        myfile.open("points.txt");
        Range<Vec2d> nodes = domain.positions;
        for (int i = 0; i < domain.size(); i++) {
            double radius = GetRadius(fraction,domain.distances[i][dist_id]);
            myfile << nodes[i][0] << " " << nodes[i][1] << " " << radius << "\n";
        }
        myfile.close();
    }


    // iterate through all points in the boundary subregion
    for (auto& c : boundary) {

        // store parent point and support
        Vec2d parent = domain.positions[c];
        Range<int> support = domain.support[c];

        // calculate the refinement radius
        double radius = GetRadius(fraction,domain.distances[c][dist_id]);

        // find the neighbour points for comparing
        Range<Vec2d> points_to_check;
        //points_to_check.reserve(support.size()*nbmax);
        for (auto& idx : support) {
            points_to_check.append(new_points[idx]);
            //points_to_check.push_back(domain.positions[idx]);
        }

        // reserve space for the new points

        // try to place nbmax points on the circle
        for (int n = 0; n < nbmax; n++) {

            // angle from isosceles triangle
            double angle = 2.0*std::acos(0.5*radius/domain.radius);

            // this is just the angle from the center of the circle to the boundary point
            double offset = std::atan2((parent[1]-domain.center[1]),(parent[0]-domain.center[0]));
            
            // create a child
            Vec2d child;

            if (n == 0) {
                child = domain.center - Vec2d{domain.radius*std::cos(offset+angle),domain.radius*std::sin(offset+angle)};
                if (PointFarEnough(child,radius,points_to_check)) {
                    new_points[c].push_back(child);
                    new_types[c].push_back(BOUNDARY);
                    points_to_check.push_back(child);
                }
            } else if (n == 1) {
                child = domain.center - Vec2d{domain.radius*std::cos(offset-angle),domain.radius*std::sin(offset-angle)};
                if (PointFarEnough(child,radius,points_to_check)) {
                    new_points[c].push_back(child);
                    new_types[c].push_back(BOUNDARY);
                    points_to_check.push_back(child);

                }
            } else if (n >= 2) {
                //double crad = domain.radius - radius;
                //child = domain.center + Vec2d{crad*std::cos(offset),crad*std::sin(offset)};
                //if (PointFarEnough(child,radius,points_to_check)) {
                //    new_points[c].push_back(child);
                //    new_types[c].push_back(INTERNAL);
                //    points_to_check.push_back(child);
                //}
                int max_number_of_checks = 70;
                int niter = 0;
                bool point_found = false;
                while (!point_found && niter < max_number_of_checks) {

                    // randomly select point on circle around main node
                    double random_value = dis(gen);
                    
                    // calculate potential child 
                    Vec2d child = parent
                        + Vec2d{radius*std::cos(random_value),radius*std::sin(random_value)};
                    niter++;
                    
                    if ((child-domain.center).norm() >= domain.radius) {
                        random_value += M_PI;
                        child = parent
                        + Vec2d{radius*std::cos(random_value),radius*std::sin(random_value)};
                    }


                    // plug in first new point
                    if (PointFarEnough(child,radius,points_to_check)) {
                        new_points[c].push_back(child);
                        new_types[c].push_back(INTERNAL);
                        points_to_check.push_back(child);
                        point_found = true;
                    }
                } 
            }

        }

    }

    if (verbose) {
        std::ofstream file;
        file.open("b_points.txt");
        for (int i = 0; i < domain.size(); i++) {
            for (auto& np : new_points[i]) {
                file << np[0] << " " << np[1] << "\n";
            }
        }
        file.close();
    }

    for (auto& c : interior) {

        // store parent point and support
        Vec2d parent = domain.positions[c];
        Range<int> support = domain.support[c];

        double radius = GetRadius(fraction,domain.distances[c][dist_id]);

        // find the neighbour points for comparing
        Range<Vec2d> points_to_check;
        points_to_check.reserve(support.size()*nmax+nmax);
        for (auto& idx : support) {
            points_to_check.append(new_points[idx]);
            //points_to_check.push_back(domain.positions[idx]);
        }

        // try to find nmax points around a given parent point
        int max_number_of_checks = 70;
        for (int n = 0; n < nmax; n++) {
            int niter = 0;
            bool point_found = false;
            while (!point_found && niter < max_number_of_checks) {

                // randomly select point on circle around main node
                double random_value = dis(gen);
                
                // calculate potential child 
                Vec2d child = parent
                    + Vec2d{radius*std::cos(random_value),radius*std::sin(random_value)};
                niter++;
                
                // plug in first new point
                if (PointFarEnough(child,radius,points_to_check)) {
                    new_points[c].push_back(child);
                    new_types[c].push_back(INTERNAL);
                    points_to_check.push_back(child);
                    point_found = true;
                } 
            }
        }
    }

    if (verbose) {
        std::ofstream file2;
        file2.open("refined_points.txt");
        for (auto& c : interior) {
            for (auto& np : new_points[c]) {
                file2 << np[0] << " " << np[1] << "\n";
            }
        }
        file2.close();
    }

    int size = domain.size();
    // push new points into domain
    for (int i = 0; i < size; i++) {
        if (!(new_points[i].empty())) {
            domain.positions.append(new_points[i]);
            domain.types.append(new_types[i]);
        }
    }

    domain.findSupport(initial_support_size);
}

void RefineWithHalfLinks(const Range<int> subregion, int nmax, int nbmax, double fraction,
                            CircleDomain<Vec2d> &domain, bool verbose) {

    assert(subregion.size() > 0 && "No nodes to refine!");
    //assert(nbmax <=6 && "Only 3 nodes supported for boundary refinement");

    // save initial support size
    int initial_support_size = domain.support[0].size();
    assert(nmax < initial_support_size && "number of links is larger than support size");

    // split subregion into boundary and internal
    Range<int> boundary;
    Range<int> interior;
    for (auto& c : subregion) {
        if (domain.types[c] < 0) {
            boundary.push_back(c);
        } else {
            interior.push_back(c);
        }
    }

    // This part checks if there are any extra boundary nodes that should also be refined
    // iterate through interior nodes of subregion
    for (auto& c : interior) {
        Range<int> support = domain.support[c];
        // then through the supports
        for (auto& s : support) {
            //and check if their are any boundary nodes
            if (domain.types[s] < 0) {
                bool contains = false;
                // check if the boundary perhaps already contains that node
                for (auto& b : boundary) {
                    if (b == s) {
                        contains = true;
                        break;
                    }
                }
                // if not then add it to the subregion to be refined
                if (!contains) {
                    boundary.push_back(s);
                }
            }
        }
    }

    // prepare a random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 2*M_PI);

    // initialize containers for new points
    
    // the range in range is for purposes of easier searching later on
    Range<Range<Vec2d>> new_points;
    Range<Range<int>> new_types;

    // reserve space
    new_points.reserve(domain.size());
    new_types.reserve(domain.size());

    // fill with empty ranges
    for (int i = 0; i < domain.size(); i++) {
        new_points.push_back(Range<Vec2d>());
        new_types.push_back(Range<int>());
    }

    // radius distance indexes
    Range<int> dist_id(2);
    for (int i = 0; i < 2; i++) dist_id[i] = i;

    // print initial point before refinement
    if (verbose) {
        std::ofstream myfile;
        myfile.open("points.txt");
        Range<Vec2d> nodes = domain.positions;
        for (int i = 0; i < domain.size(); i++) {
            double radius = GetRadius(0.5,domain.distances[i][dist_id]);
            myfile << nodes[i][0] << " " << nodes[i][1] << " " << radius << "\n";
        }
        myfile.close();
    }


    // iterate through all points in the boundary subregion
    for (auto& c : boundary) {

        // store parent point and support
        Vec2d parent = domain.positions[c];
        Range<int> support = domain.support[c];

        // calculate the refinement radius
        //double radius = GetRadius(fraction,domain.distances[c][dist_id]);
        double radius = fraction*domain.characteristicDistance();
        
        // find the neighbour points for comparing
        Range<Vec2d> points_to_check;
        //points_to_check.reserve(support.size()*nbmax);
        for (auto& idx : support) {
            points_to_check.append(new_points[idx]);
            points_to_check.push_back(domain.positions[idx]);
        }

        // reserve space for the new points

        // try to place nbmax points on the circle
        for (int n = 1; n < nbmax+1; n++) {

            // calculate potential child 
            Vec2d child = parent + domain.positions[support[n]];
            prn(child)
            child *= 0.5;
            prn(child)

            // in case the link is between 2 boundary nodes
            if (domain.types[support[n]] < 0) {
                double angle = std::atan2(child[1]-domain.center[1],child[0]-domain.center[0]);
                child = domain.center + domain.radius*Vec2d{std::cos(angle),std::sin(angle)};
                // plug in first new point
                if (PointFarEnough(child,radius,points_to_check)) {
                    new_points[c].push_back(child);
                    new_types[c].push_back(BOUNDARY);
                    points_to_check.push_back(child);
                }
            } else {
                // plug in first new point
                if (PointFarEnough(child,radius,points_to_check)) {
                    new_points[c].push_back(child);
                    new_types[c].push_back(INTERNAL);
                    points_to_check.push_back(child);
                }
            }
        }

    }

    if (verbose) {
        std::ofstream file;
        file.open("b_points.txt");
        for (int i = 0; i < domain.size(); i++) {
            for (auto& np : new_points[i]) {
                file << np[0] << " " << np[1] << "\n";
            }
        }
        file.close();
    }


    for (auto& c : interior) {

        // store parent point and support
        Vec2d parent = domain.positions[c];
        Range<int> support = domain.support[c];

        // find the neighbour points for comparing
        Range<Vec2d> points_to_check;
        points_to_check.reserve(support.size()*nmax+nmax);
        for (auto& idx : support) {
            points_to_check.append(new_points[idx]);
            points_to_check.push_back(domain.positions[idx]);
        }

        //double radius = GetRadius(fraction,domain.distances[c][dist_id]);
        double radius = fraction*domain.characteristicDistance();

        for (int i = 1; i < nmax+1; i++) {
                
            // calculate potential child 
            Vec2d child = parent + domain.positions[support[i]];
            prn(child)
            child *= 0.5;
            prn(child)
                
            //radius = (parent-child).norm();    
            // plug in first new point
            if (PointFarEnough(child,radius,points_to_check)) {
                new_points[c].push_back(child);
                new_types[c].push_back(INTERNAL);
                points_to_check.push_back(child);
            } 
        }
    }


    int size = domain.size();
    // push new points into domain
    for (int i = 0; i < size; i++) {
        if (!(new_points[i].empty())) {
            domain.positions.append(new_points[i]);
            domain.types.append(new_types[i]);
        }
    }

    domain.findSupport(initial_support_size);

    if (verbose) {
        std::ofstream myfile;
        myfile.open("refined_points.txt");
        Range<Vec2d> nodes = domain.positions;
        for (int i = 0; i < domain.size(); i++) {
            myfile << nodes[i][0] << " " << nodes[i][1] << "\n";
        }
        myfile.close();
    }
}    


void RefineHybrid(const Range<int> subregion, int nmax, int nbmax, double fraction,
                            CircleDomain<Vec2d> &domain, bool verbose) {

    assert(subregion.size() > 0 && "No nodes to refine!");
    //assert(nbmax <=6 && "Only 3 nodes supported for boundary refinement");

    // save initial support size
    int initial_support_size = domain.support[0].size();
    assert(nmax < initial_support_size && "number of links is larger than support size");

    // split subregion into boundary and internal
    Range<int> boundary;
    Range<int> interior;
    for (auto& c : subregion) {
        if (domain.types[c] < 0) {
            boundary.push_back(c);
        } else {
            interior.push_back(c);
        }
    }

    // This part checks if there are any extra boundary nodes that should also be refined
    // iterate through interior nodes of subregion
    for (auto& c : interior) {
        Range<int> support = domain.support[c];
        // then through the supports
        for (auto& s : support) {
            //and check if their are any boundary nodes
            if (domain.types[s] < 0) {
                bool contains = false;
                // check if the boundary perhaps already contains that node
                for (auto& b : boundary) {
                    if (b == s) {
                        contains = true;
                        break;
                    }
                }
                // if not then add it to the subregion to be refined
                if (!contains) {
                    boundary.push_back(s);
                }
            }
        }
    }

    // prepare a random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 2*M_PI);

    // initialize containers for new points
    
    // the range in range is for purposes of easier searching later on
    Range<Range<Vec2d>> new_points;
    Range<Range<int>> new_types;

    // reserve space
    new_points.reserve(domain.size());
    new_types.reserve(domain.size());

    // fill with empty ranges
    for (int i = 0; i < domain.size(); i++) {
        new_points.push_back(Range<Vec2d>());
        new_types.push_back(Range<int>());
    }

    // radius distance indexes
    Range<int> dist_id(2);
    for (int i = 0; i < 2; i++) dist_id[i] = i;

    // print initial point before refinement
    if (verbose) {
        std::ofstream myfile;
        myfile.open("points.txt");
        Range<Vec2d> nodes = domain.positions;
        for (int i = 0; i < domain.size(); i++) {
            double radius = GetRadius(0.5,domain.distances[i][dist_id]);
            myfile << nodes[i][0] << " " << nodes[i][1] << " " << radius << "\n";
        }
        myfile.close();
    }


    // iterate through all points in the boundary subregion
    for (auto& c : boundary) {

        // store parent point and support
        Vec2d parent = domain.positions[c];
        Range<int> support = domain.support[c];

        // calculate the refinement radius
        double radius = GetRadius(fraction,domain.distances[c][dist_id]);
        //double radius = fraction*domain.characteristicDistance();
        
        // find the neighbour points for comparing
        Range<Vec2d> points_to_check;
        //points_to_check.reserve(support.size()*nbmax);
        for (auto& idx : support) {
            points_to_check.append(new_points[idx]);
            points_to_check.push_back(domain.positions[idx]);
        }

        // reserve space for the new points

        // try to place nbmax points on the circle
        for (int n = 1; n < nbmax+1; n++) {

            // calculate potential child 
            Vec2d child = parent + domain.positions[support[n]];
            prn(child)
            child *= 0.5;
            prn(child)

            // in case the link is between 2 boundary nodes
            if (domain.types[support[n]] < 0) {
                double angle = std::atan2(child[1]-domain.center[1],child[0]-domain.center[0]);
                child = domain.center + domain.radius*Vec2d{std::cos(angle),std::sin(angle)};
                // plug in first new point
                if (PointFarEnough(child,radius,points_to_check)) {
                    new_points[c].push_back(child);
                    new_types[c].push_back(BOUNDARY);
                    points_to_check.push_back(child);
                }
            } else {
                // plug in first new point
                if (PointFarEnough(child,radius,points_to_check)) {
                    new_points[c].push_back(child);
                    new_types[c].push_back(INTERNAL);
                    points_to_check.push_back(child);
                }
            }
        }

    }

    if (verbose) {
        std::ofstream file;
        file.open("b_points.txt");
        for (int i = 0; i < domain.size(); i++) {
            for (auto& np : new_points[i]) {
                file << np[0] << " " << np[1] << "\n";
            }
        }
        file.close();
    }


    for (auto& c : interior) {

        // store parent point and support
        Vec2d parent = domain.positions[c];
        Range<int> support = domain.support[c];

        double radius = GetRadius(fraction,domain.distances[c][dist_id]);

        // find the neighbour points for comparing
        Range<Vec2d> points_to_check;
        points_to_check.reserve(support.size()*nmax+nmax);
        for (auto& idx : support) {
            points_to_check.append(new_points[idx]);
        }

        // try to find nmax points around a given parent point
        int max_number_of_checks = 70;
        for (int n = 0; n < nmax; n++) {
            int niter = 0;
            bool point_found = false;
            while (!point_found && niter < max_number_of_checks) {

                // randomly select point on circle around main node
                double random_value = dis(gen);
                
                // calculate potential child 
                Vec2d child = parent
                    + Vec2d{radius*std::cos(random_value),radius*std::sin(random_value)};
                niter++;
                
                // plug in first new point
                if (PointFarEnough(child,radius,points_to_check)) {
                    new_points[c].push_back(child);
                    new_types[c].push_back(INTERNAL);
                    points_to_check.push_back(child);
                    point_found = true;
                } 
            }
        }
    }


    int size = domain.size();
    // push new points into domain
    for (int i = 0; i < size; i++) {
        if (!(new_points[i].empty())) {
            domain.positions.append(new_points[i]);
            domain.types.append(new_types[i]);
        }
    }

    domain.findSupport(initial_support_size);

    if (verbose) {
        std::ofstream myfile;
        myfile.open("refined_points.txt");
        Range<Vec2d> nodes = domain.positions;
        for (int i = 0; i < domain.size(); i++) {
            myfile << nodes[i][0] << " " << nodes[i][1] << "\n";
        }
        myfile.close();
    }
}    