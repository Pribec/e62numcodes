#ifndef SRC_IO_HPP_
#define SRC_IO_HPP_

#include "includes.hpp"
#include "common.hpp"
#include <tinyxml2/tinyxml2.hpp>
#ifndef __MIC__
#include <hdf5.h>
#include <H5Cpp.h>
#endif

namespace mm {

/**
 * @file io.hpp
 * @brief Basic IO functionality
 * @example io_test.cpp
 */

/**
 * @brief Primitives for loading XML files.
 */
class XMLloader {
  public:
    /**
     * Proxy to allow typecasts and calling simply
     * double a = getAttribute(path, att_name);
     **/
    class Proxy {
        const XMLloader* parent;  ///< pointer to parent
        std::vector<std::string> path;  ///< path to the att. tag
        std::string att;  ///< attribute name

      public:
        /// Constructor
        Proxy(const XMLloader* parent_, const std::vector<std::string>& path_,
              const std::string& att_) : parent(parent_), path(path_), att(att_) {}
        /// Cast operator to double
        operator double() const;
        /// Cast operator to float
        operator float() const;
        /// Cast operator to int
        operator int() const;
        /// Cast operator to int
        operator size_t() const;
        /// Cast operator to bool
        operator bool() const;
        /// Cast operator to string
        operator std::string() const;

        friend std::ostream& operator<<(std::ostream& xx, const Proxy& arr);
    };

    tinyxml2::XMLDocument doc;  ///< document to parse
    /// Function used for document walks.
    const tinyxml2::XMLElement* walk(const std::vector<std::string>& path) const;

    XMLloader();  ///< allow default constructor
    /**
     * Loads xml from file.
     * @param file Filename of a file to load from.
     */
    XMLloader(const std::string& file);
    /**
     * Loads xml from file.
     * @param file Filename of a file to load from.
     */
    void operator()(const std::string& file);
    /**
     * @brief reads text from xml element -
     * @param path `textRead([{"level1","level2",...}])`
     */
    std::string textRead(const std::vector<std::string>& path) const;
    /**
     * @brief Reads element's attribute based on return type
     * @param path Path to read from.
     * @param att Attribute to read.
     */
    Proxy getAttribute(const std::vector<std::string>& path, const std::string& att) const;

    /// Sets an attribute at a given path
    template <typename T>
    void setAttribute(const std::vector<std::string>& path,
                      const std::string& att, const T& value) {
        assert_msg(!path.empty(), "Path must not be empty.");
        tinyxml2::XMLElement* e = doc.FirstChildElement(path[0].c_str());
        assert_msg(e != nullptr, "First element in path must exist.");
        for (size_t i = 1; i < path.size(); ++i) {
            tinyxml2::XMLElement* child = e->FirstChildElement(path[i].c_str());
            if (child == nullptr) {
                child = doc.NewElement(path[i].c_str());
                e->InsertEndChild(child);
            }
            e = child;
        }
        e->SetAttribute(att.c_str(), value);
    }

    /// Specialized getAttribute for double
    double getDoubleAttribute(const std::vector<std::string>& path, const std::string& att) const;
    /// Specialized getAttribute for float
    float getFloatAttribute(const std::vector<std::string>& path, const std::string& att) const;
    /// Specialized getAttribute for int
    int getIntAttribute(const std::vector<std::string>& path, const std::string& att) const;
    /// Specialized getAttribute for bool
    bool getBoolAttribute(const std::vector<std::string>& path, const std::string& att) const;
    /// Specialized getAttribute for string
    std::string getStringAttribute(const std::vector<std::string>& path,
                                   const std::string& att) const;

    /// Returns all conf variables as key value pairs.
    std::vector<std::pair<std::string, std::string>> getKeyValuePairs() const;

    /**
     * Get value from xml file.
     * @tparam T type of the parameter.
     * @param path Dot separated path to the element, including the root xml-tag.
     * For example `xml.get<double>("params.case.P");`.
     * @return Value of the element.
     * @throws Assertion fails if an element does not exist.
     */
    template <typename T>
    T get(const std::string& path) const {
        std::vector<std::string> parts = split(path, '.');
        assert_msg(!parts.empty(), "Path is empty.");
        std::vector<std::string> root_path(parts.begin(), parts.end()-1);
        std::string attribute = parts.back();
        T result = getAttribute(root_path, attribute);
        return result;
    }

    /**
     * Save a value in the xml file.
     * @param path Dot separated path to the value. It the path does not exist, it is created.
     * @param value Value to save.
     */
    template <typename T>
    void set(const std::string& path, const T& value) {
        std::vector<std::string> parts = split(path, '.');
        assert_msg(!parts.empty(), "Path is empty.");
        std::vector<std::string> root_path(parts.begin(), parts.end()-1);
        std::string attribute = parts.back();
        setAttribute(root_path, attribute, value);
    }
};

#ifndef __MIC__  // Disable HDF5 support on MIC
/// HDF5IO string error value.
#define HDF5IO_STRING_ERR ""
/// HDF5IO int error value.
#define HDF5IO_INT_ERR -999999
/// HDF5IO float error value.
#define HDF5IO_FLOAT_ERR -999999.9f
/// HDF5IO double error value.
#define HDF5IO_DOUBLE_ERR -999999.9

/**
 * @class HDF5IO
 * @brief Class that handles low level hdf5 IO.
 */
class HDF5IO {
    /// Name of the open file
    H5std_string file_name;
    /// Name of the open group
    H5std_string folder_name;
    /// Open file object
    H5::H5File file;
    /// Group (folder) object
    H5::Group group;
    /**
     * Exception handling:
     *   0: ignore,
     *   1: print warning to stderr (default),
     *   2: throw exception,
     *   3: print warning and assert,
     */
    int exception_handling;

    /**
     * @brief Print and handle HDF5 Exceptions and info.
     * @param error `const H5::Exception&` thrown by some HDF5Cpp function.
     * @param function Name of the function that caught the exception
     */
    void printError(const H5::Exception& error, const std::string& function) const;

    /// Open file in a smart way.
    static H5::H5File openFileHelper(const std::string& file_name, unsigned mode);
  public:
    /// Default chunk size
    static constexpr hsize_t default_chunk_size = 10000;
    /// Mode to open the file
    enum Mode : unsigned {
        APPEND = 256,  ///< Appends to the existing contents = 0b100000000.
        DESTROY = 128,  ///< Removes old contents = 0b010000000.
        READONLY = 64   ///< Read only open, file must exist = 0b001000000.
    };

    /**
     * @brief Constructor that opens the given file.
     * @details The constructor will fail if you give it a non-existent file
     * with new_file = false or an existing file with new_file = true.
     * @param _file_name Path and name of the file to open.
     * @param mode How to open the file, either `APPEND`, `DESTROY`, `READONLY`, `H5F_ACC_EXCL`,
     * `H5F_ACC_TRUNC`, `H5F_ACC_RDONLY` or `H5F_ACC_RDWR`.
     * @param _exception_handling
     * Exception handling:
     *     0: ignore,
     *     1: print warning to stderr (default),
     *     2: throw exception,
     *     4: print warning and assert,
     */
    explicit HDF5IO(const std::string& _file_name, unsigned mode = APPEND,
                    int _exception_handling = 1);
    /// Initialize empty object to be used later.
    explicit HDF5IO(int _exception_handling = 1);

    /// Remove move assignment
    HDF5IO& operator=(HDF5IO&&) = delete;
    /// Remove copy assignment
    HDF5IO& operator=(const HDF5IO&) = delete;

    /**
     * @brief Open a new file with given filename `_file_name` and mode `mode`.
     * @sa HDF5IO::HDF5IO
     */
    void openFile(const std::string& _file_name, int mode = APPEND);

    /// Reopens a possibly closed file.
    void reopenFile(int mode = APPEND);

    /// Closes current file.
    void closeFile();
    /**
     * @brief Enters a folder inside a HDF5 file
     *
     * @param folder_name Folder name to open. You have to use absolute path
     * inside the file every time. Folder must begin with a '/' and not end in one,
     * unless you mean the root folder.
     * Examples:  `openfolder('/test/basic/n100')`, `openfolder('/')`, `openfolder('/test')`
     */
    void openFolder(const std::string& folder_name);

    /// Reopens a possibly closed file.
    void reopenFolder();

    /**
     * @brief Creates a new folder in the file
     *
     * @param folder_name Folder name to create. You have to use absolute path
     * inside the file every time.
     */
    void createFolder(const std::string& folder_name);

    /**
     * @brief Closes the currently open folder in hdf5 file
     */
    void closeFolder();

    /// Lists all folders in the currently open folder.
    std::vector<std::string> ls();

    /// Returns currently open filename
    std::string getFileName() const;

    /// Returns currently open folder name
    std::string getFolderName() const;

    /**
     * @brief Templated function to get all attributes
     *
     * @param name Name of the attribute.
     * @tparam T Type of the attribute.
     * @return Value of the attribute.
     */
    template <class T>
    T getAttribute(const std::string& name) const {
        assert_msg(group.getId() > 0, "Open a folder before reading attribute '%s'.", name);
        T result;
        auto attribute = group.openAttribute(name);
        attribute.read(attribute.getDataType(), &result);
        attribute.close();
        return result;
    }

    /**
     * @brief Templated function to set all attributes
     *
     * @param name Name of the attribute.
     * @param value Value of the attribute.
     * @param type HDF5 style type of T.
     * @tparam T Type of the attribute.
     * @return True if successful and false otherwise..
     */
    template <class T>
    bool setAttribute(const std::string& name, const T& value, const H5::DataType& type) {
        assert_msg(group.getId() > 0, "Open a folder before writing attribute '%s'.", name);
        H5::Attribute attribute;
        H5E_auto2_t func;
        void* client_data;
        H5::Exception::getAutoPrint(func, &client_data);
        H5::Exception::dontPrint();
        try {
            attribute = group.openAttribute(name);
        } catch (H5::AttributeIException error) {
            attribute = group.createAttribute(name, type, H5::DataSpace());
        }
        H5::Exception::setAutoPrint(func, client_data);
        attribute.write(type, &value);
        attribute.close();
        return true;
    }

    /**
     * @brief Reads a vector of type `T` from attribute named `name` from the currently open folder.
     * @param name Attribute name. Valid names are all C++ strings, but we generally stick to
     * `[A-Za-z0-9_.-]+`.
     * @return Vector of values or empty vector if an error occurred.
     * @snippet io_test.cpp Writing nonstandard type
     * @sa setArray
     */
    template <typename T>
    std::vector<T> getArray(const std::string& name) const {
        try {
            auto dataset = group.openDataSet(name);
            std::vector<hsize_t> dims(1);
            dataset.getSpace().getSimpleExtentDims(&dims[0]);
            std::vector<T> result(dims[0]);
            dataset.read(&result[0], dataset.getDataType());
            dataset.close();
            return result;
        } catch (const H5::Exception& error) {
            printError(error, "getArray");
        }
        return {};
    }

    /**
      * @brief Writes a array of type `T` to currently open hdf5 folder (group). The function
      * creates the attribute if it does not exists. Type `T` must be compatible with given HDF5
      * type.
      * @param name Attribute name. Valid names are all C++ strings, but we generally stick to
      * `[A-Za-z0-9_.-]+`.
      * @param value Value to write. The values in the container are automatically cast to the type
      * `T`. The container `value` must have a `.size()` method and support `operator[]`.
      * @param type HDF5 type representing `T`. A list of types is available here:
      * https://support.hdfgroup.org/HDF5/doc1.8/RM/PredefDTypes.html
      * @param chunk_dims_ Chunk size. The default value -1 means that a chunk size of
      * `min(default_chunk_size, array_size)` is chosen in that dimension.
      * @warning The IO performance is gravely affected by the chunk size.
      * Read more on https://support.hdfgroup.org/HDF5/doc/Advanced/Chunking/.
      * In general the chunk size should not exceed the size of data or 4GB, however
      * a common pitfall is the chunk size is too small.
      * @details
      * Example of usage saving an int array in 16 bit precision:
      * @snippet io_test.cpp Writing nonstandard type
      * @return True if successful and false otherwise.
      * @sa getArray
      */
    template<typename T, class vec_t>
    bool setArray(const std::string& name, const vec_t& value, H5::DataType type,
                  hsize_t chunk_dims_ = -1ull) {
        assert_msg(group.getId() > 0, "Open a folder before writing attribute '%s'.", name);
        try {
            hsize_t size = value.size();
            hsize_t dim[1] = {size};
            H5::DataSet dataset;
            if (chunk_dims_ == -1ull) {
                chunk_dims_ = std::min(default_chunk_size, size);
            }
            H5E_auto2_t func;
            void* client_data;
            H5::Exception::getAutoPrint(func, &client_data);
            H5::Exception::dontPrint();
            try {
                dataset = group.openDataSet(name);
                dataset.extend(dim);
            } catch(H5::GroupIException not_found_error) {
                hsize_t chunk_dims[1];
                chunk_dims[0] = chunk_dims_;

                H5::DSetCreatPropList prop;
                prop.setChunk(1, chunk_dims);

                hsize_t maxdim[1] = {H5S_UNLIMITED};
                dataset = group.createDataSet(name, type, H5::DataSpace(1, dim, maxdim), prop);
            }
            H5::Exception::setAutoPrint(func, client_data);

            std::vector<T> cast_value(size);
            for (hsize_t j = 0; j < size; j++) cast_value[j] = static_cast<T>(value[j]);
            dataset.write(&cast_value[0], type);
            dataset.close();
            return true;
        } catch (const H5::Exception& error) {
            printError(error, "setArray");
        }
        return false;
    }

    /**
     * @brief Reads a 2D array of type `T` from attribute named `name` from the currently open
     * folder.
     * @param name Attribute name. Valid names are all C++ strings, but we generally stick to
     * `[A-Za-z0-9_.-]+`.
     * @return Vector of values or empty vector if an error occurred.
     * @sa set2DArray
     * @sa getArray
     */
    template <typename T>
    std::vector<std::vector<T>> get2DArray(const std::string& name) const {
        try {
            auto dataset = group.openDataSet(name);
            std::vector<hsize_t> dims(2);
            dataset.getSpace().getSimpleExtentDims(&dims[0]);
            std::vector<T> linear_result(dims[0] * dims[1]);
            dataset.read(&linear_result[0], dataset.getDataType());
            dataset.close();

            std::vector<std::vector<T>> result(dims[0], std::vector<T>(dims[1]));
            for (size_t i = 0; i < dims[0]; i++)
                for (size_t j = 0; j < dims[1]; j++)
                    result[i][j] = linear_result[i * dims[1] + j];
            return result;
        } catch (const H5::Exception& error) {
            printError(error, "get2DArray");
        }
        return {};
    }

    /**
     * @brief Writes a 2D array of `T` to currently open hdf5 folder (group). The function
     * creates the attribute if it does not exists. Type `T` must be compatible with given HDF5
     * type.
     * @param name Attribute name. Valid names are all C++ strings, but we generally stick to
     * `[A-Za-z0-9_.-]+`.
     * @param value Value to write. The value is automatically cast to the required type.
     * @param type HDF5 type representing `T`. A list of types is available here:
     * https://support.hdfgroup.org/HDF5/doc1.8/RM/PredefDTypes.html
     * @param chunk_dims 2-array for chunking data. Describes the size of
     * the chunks inside the file. The default value -1 means that a chunk size of
     * `min(default_chunk_size, array_size)` is chosen in that dimension.
     * @param unlimited If the written dataset should be expandable e.g.
     * If you can later store a bigger array in the same spot without manually
     * deleting it.
     * @warning The IO performance is gravely affected by the chunk size.
     * Read more on https://support.hdfgroup.org/HDF5/doc/Advanced/Chunking/.
     * In general the chunk size should not exceed the size of data or 4GB, however
     * a common pitfall is the chunk size is too small.
     * @return True if successful and false otherwise.
     * @sa setArray
     * @sa get2DArray
     */
    template<typename T, class vec_t>
    bool set2DArray(const std::string& name, const vec_t& value, H5::DataType type,
            std::vector<hsize_t> chunk_dims = {-1ull, -1ull}, bool unlimited = false) {
        assert_msg(group.getId() > 0, "Open a folder before writing attribute '%s'.", name);
        try {
            assert_msg(value.size() > 0, "Cannot write an empty 2d array.");
            hsize_t height = value.size(), width = value[0].size();
            hsize_t dim[2] = {height, width};
            H5::DataSet dataset;
            if (chunk_dims[0] == -1ull) {
                chunk_dims = {std::min(default_chunk_size, height),
                              std::min(default_chunk_size, width)};
            }

            H5E_auto2_t func;
            void* client_data;
            H5::Exception::getAutoPrint(func, &client_data);
            H5::Exception::dontPrint();
            try {
                dataset = group.openDataSet(name);
                dataset.extend(dim);
            } catch(H5::GroupIException not_found_error) {
                H5::DSetCreatPropList prop;
                prop.setChunk(2, &chunk_dims[0]);
                hsize_t maxdim[2] = {H5S_UNLIMITED, H5S_UNLIMITED};
                if (!unlimited) {maxdim[0] = dim[0]; maxdim[1] = dim[1];}
                dataset = group.createDataSet(name, type, H5::DataSpace(2, dim, maxdim), prop);
            }
            H5::Exception::setAutoPrint(func, client_data);
            std::vector<T> linear_value(width * height);
            for (hsize_t i = 0; i < height; i++) {
                for (hsize_t j = 0; j < width; j++) {
                    linear_value[i * width + j] = static_cast<T>(value[i][j]);
                }
            }
            dataset.write(&linear_value[0], type);
            dataset.close();
            return true;
        } catch (const H5::Exception& error) {
            printError(error, "set2DArray");
        }
        return false;
    }

    /**
     * @brief Writes a sparse matrix to a hdf5 folder (group). The function creates the
     * attribute if it does not exists. Matrix `M` is written as a `N x 3` matrix, where the columns
     * represent vectors `I`, `J`, `V`, such that `M(I(i), J(i)) = V(i)`. Matrix values are stored
     * as doubles of native size.
     * @warning By default, the indexes of the matrix elements are saved in 1-based format.
     * @param name Attribute name. Valid names are all C++ strings, but we generally stick to
     * `[A-Za-z0-9_.-]+`.
     * @param matrix A sparse matrix.
     * @param one_based Whether to store the indices as one or zero based. One based indexes are
     * ready to be read by Matlab's `spconvert`.
     * @return True if successful and false otherwise.
     */
    template <typename SparseMatrixType>
    bool setSparseMatrix(const std::string& name, SparseMatrixType& matrix, bool one_based = true) {
        std::vector<std::array<double, 3>> triplets(matrix.nonZeros());
        int c = 0;
        for (int k = 0; k < matrix.outerSize(); ++k) {
            for (typename SparseMatrixType::InnerIterator it(matrix, k); it; ++it) {
                triplets[c][0] = one_based+it.row();
                triplets[c][1] = one_based+it.col();
                triplets[c][2] = it.value();
                ++c;
            }
        }
        return setDouble2DArray(name, triplets);
    }

    /**
     * Saves a double array `value` under name `name` in currently open group
     * using type `H5::PredType::NATIVE_DOUBLE` and chunk size `chunk_dims_`.
     * @sa setArray
     */
    template<class vec_t>
    bool setDoubleArray(const std::string& name, const vec_t& value, hsize_t chunk_dims_ = -1ull) {
        return setArray<double>(name, value, H5::PredType::NATIVE_DOUBLE, chunk_dims_);
    }

    /**
     * Saves a float array `value` under name `name` in currently open group
     * using type `H5::PredType::NATIVE_FLOAT` and chunk size `chunk_dims_`.
     * @sa setArray
     */
    template<class vec_t>
    bool setFloatArray(const std::string& name, const vec_t& value, hsize_t chunk_dims_ = -1ull) {
        return setArray<float>(name, value, H5::PredType::NATIVE_FLOAT, chunk_dims_);
    }

    /**
     * Saves an int array `value` under name `name` in currently open group
     * using type `H5::PredType::NATIVE_INT` and chunk size `chunk_dims_`.
     * @sa setArray
     */
    template<class vec_t>
    bool setIntArray(const std::string& name, const vec_t& value, hsize_t chunk_dims_ = -1ull) {
        return setArray<int>(name, value, H5::PredType::NATIVE_INT, chunk_dims_);
    }

    /**
     * Saves a 2D double array `value` under name `name` in currently open group
     * using type `H5::PredType::NATIVE_DOUBLE` and chunk size `chunk_dims_`.
     * @sa set2DArray
     */
    template<class vec_t>
    bool setDouble2DArray(const std::string& name, const vec_t& value,
                          std::vector<hsize_t> chunk_dims = {-1ull, -1ull},
                          bool unlimited = false) {
        return set2DArray<double>(name, value, H5::PredType::NATIVE_DOUBLE, chunk_dims, unlimited);
    }

    /**
     * Saves a 2D float array `value` under name `name` in currently open group
     * using type `H5::PredType::NATIVE_FLOAT` and chunk size `chunk_dims_`.
     * @sa set2DArray
     */
    template<class vec_t>
    bool setFloat2DArray(const std::string& name, const vec_t& value,
                         std::vector<hsize_t> chunk_dims = {-1ull, -1ull}, bool unlimited = false) {
        return set2DArray<float>(name, value, H5::PredType::NATIVE_FLOAT, chunk_dims, unlimited);
    }

    /**
     * Saves a 2D int array `value` under name `name` in currently open group
     * using type `H5::PredType::NATIVE_INT` and chunk size `chunk_dims_`.
     * @sa set2DArray
     */
    template<class vec_t>
    bool setInt2DArray(const std::string& name, const vec_t& value,
                       std::vector<hsize_t> chunk_dims = {-1ull, -1ull}, bool unlimited = false) {
        return set2DArray<int>(name, value, H5::PredType::NATIVE_INT, chunk_dims, unlimited);
    }

    /**
     * Saves string attribute `value` under name `name` in the currently open folder.
     */
    bool setStringAttribute(const std::string& name, const std::string& value);

    /**
     * Saves double attribute `value` under name `name` in the currently open folder.
     * @sa setAttribute
     */
    bool setDoubleAttribute(const std::string& name, double value);

    /**
     * Saves float attribute `value` under name `name` in the currently open folder.
     * @sa setAttribute
     */
    bool setFloatAttribute(const std::string& name, float value);

    /**
     * Saves int attribute `value` under name `name` in the currently open folder.
     * @sa setAttribute
     */
    bool setIntAttribute(const std::string& name, int value);

    /**
     * Saves domain in a hdf5 file by creating a folder `name` in the currently open folder
     * and writing properties `pos`, `N`, `types`, `supp`, `bmap` and `normals` into it.
     * Previously opened folder is reopened in the end, leaving the file in the state it was
     * received.
     * @param name Name of the domain.
     * @param domain Domain object.
     * @return `true` if all writes were successful, and `false` otherwise.
     * @todo(jureslak): write tests
     */
    template <typename domain_t>
    bool setDomain(const std::string& name, const domain_t& domain) {
        assert_msg(group.getId() > 0, "Open a folder before writing attribute '%s'.", name);
        std::string folder_name = getFolderName();
        if (folder_name.back() == '/') openFolder(folder_name+name);
        else openFolder(folder_name+'/'+name);
        bool b = true;
        b &= setDouble2DArray("pos", domain.positions);
        b &= setIntAttribute("N", domain.size());
        b &= setIntArray("types", domain.types);
        b &= setInt2DArray("supp", pad(domain.support, -1));
        b &= setIntArray("bmap", domain.boundary_map());
        b &= setDouble2DArray("normals", domain.normals());
        openFolder(folder_name);
        return b;
    }

    /**
     * Saves all timings in timer in a hdf5 file by creating a folder `name` in the current
     * folder and writing properties attributes named `from-to` to it.
     * Previously opened folder is reopened in the end, leaving the file in the state it was
     * received.
     * @param name Name of the timer
     * @param timer Timer object.
     * @return `true` if all writes were successful, and `false` otherwise.
     */
    template <typename timer_t>
    bool setTimer(const std::string& name, const timer_t& timer) {
        assert_msg(group.getId() > 0, "Open a folder before writing attribute '%s'.", name);
        std::string folder_name = getFolderName();
        if (folder_name.back() == '/') openFolder(folder_name+name);
        else openFolder(folder_name+'/'+name);
        bool b = true;
        std::vector<std::string> labels = timer.getLabels();
        int size = labels.size();
        if (size <= 1) return true;
        for (int i = 1; i < size; ++i) {
            b &= setDoubleAttribute(labels[i-1]+"-"+labels[i],
                                    timer.getTime(labels[i-1], labels[i]));
        }
        b &= setDoubleAttribute("total", timer.getTime(labels.front(), labels.back()));
        openFolder(folder_name);
        return b;
    }

    /**
     * Saves a configuration in a specified folder.
     * @param name Name of the conf.
     * @param conf The configuration object.
     * @return `tru` is all writes were seuccessful and `false` otherwise.
     */
    template <typename conf_t>
    bool setConf(const std::string& name, const conf_t& conf) {
        assert_msg(group.getId() > 0, "Open a folder before writing attribute '%s'.", name);
        std::string folder_name = getFolderName();
        if (folder_name.back() == '/') openFolder(folder_name+name);
        else openFolder(folder_name+'/'+name);
        bool b = true;
        std::vector<std::pair<std::string, std::string>> data = conf.getKeyValuePairs();
        for (const auto& kv : data) {
            try {
                std::string::size_type num_read;
                double x = std::stod(kv.second, &num_read);
                if (num_read < kv.second.size()) {  // some characters were not read, it's a string
                    throw std::invalid_argument(kv.second);
                }
                b &= setDoubleAttribute(kv.first, x);
            } catch (const std::invalid_argument&) {
                b &= setStringAttribute(kv.first, kv.second);
            }
        }
        openFolder(folder_name);
        return b;
    }

    /**
     * Reads an int array attribute saved under name `name` in the currently open folder.
     * @sa getArray
     * @sa setArray
     */
    std::vector<int> getIntArray(const std::string& name) const;
    /**
      * Reads a double array attribute saved under name `name` in the currently open folder.
      * @sa getArray
      * @sa setArray
      */
    std::vector<double> getDoubleArray(const std::string& name) const;

    /**
     * Reads a float array attribute saved under name `name` in the currently open folder.
     * @sa getArray
     * @sa setArray
     */
    std::vector<float> getFloatArray(const std::string& name) const;

    /**
     * Reads an 2D int array attribute saved under name `name` in the currently open folder.
     * @sa get2DArray
     * @sa set2DArray
     */
    std::vector<std::vector<int>> getInt2DArray(const std::string& name) const;
    /**
     * Reads a 2D double array attribute saved under name `name` in the currently open folder.
     * @sa get2DArray
     * @sa set2DArray
     */
    std::vector<std::vector<double>> getDouble2DArray(const std::string& name) const;

    /**
     * Reads a 2D float array attribute saved under name `name` in the currently open folder.
     * @sa get2DArray
     * @sa set2DArray
     */
    std::vector<std::vector<float>> getFloat2DArray(const std::string& name) const;

    /**
     * @brief Reads a string attribute from a hdf5 file or folder.
     * @param name Attribute name.
     * @return Read string attribute or `HDF5IO_STRING_ERR` if error occurred.
     */
    std::string getStringAttribute(const std::string& name) const;

    /**
     * @brief Reads a float attribute from a hdf5 file or folder.
     * @param name Attribute name.
     * @return Read float attribute or `HDF5IO_FLOAT_ERR` if error occurred.
     */
    float getFloatAttribute(const std::string& name) const;

    /**
     * @brief Reads a double attribute from a hdf5 file or folder.
     * @param name Attribute name.
     * @return Read double attribute or `HDF5IO_DOUBLE_ERR` if error occurred.
     */
    double getDoubleAttribute(const std::string& name) const;

    /**
     * @brief Reads an int attribute from a hdf5 file or folder.
     * @param name Attribute name.
     * @return Read int attribute or `HDF5IO_INT_ERR` if error occurred.
     */
    int getIntAttribute(const std::string& name) const;

    /**
     * @brief Reads a `b-scope` (a 2D unsigned char array) from a hdf5 file or folder.
     * @param name `b-scope` name. Valid names are all C++ strings, but we generally stick to
     * `[A-Za-z0-9_.-]+`.
     * @return Read `b-scope` or an empty array if error occurred.
     */
    std::vector<std::vector<unsigned char>> getBScope(const std::string& name) const;
};
#endif  // ifndef __MIC__

}  // namespace mm

#endif  // SRC_IO_HPP_
