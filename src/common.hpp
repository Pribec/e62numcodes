#ifndef SRC_COMMON_HPP_
#define SRC_COMMON_HPP_

/**
 * @file
 * @brief provides some type independent funcionalities, e.g., << overload for different
 * classes, print macros...
 * @example common_test.cpp
 */

#include "includes.hpp"
#include <tinyformat/tinyformat.h>

// additional ostream operators
namespace std {
/// cout << overload for PAIR
template<class T, class U>
std::ostream& operator<<(std::ostream& xx, const std::pair<T, U>& par) {
    xx << "(" << par.first << "," << par.second << ")";
    return xx;
}

/// cout << overload for ARRAY with MATLAB like output.
template<class T, int N>
std::ostream& operator<<(std::ostream& xx, const std::array<T, N>& arr) {
    xx << "[";
    for (int i = 0; i < N; ++i) {
        xx << arr[i];
        if (i < N - 1) xx << ",";
    }
    xx << "]";
    return xx;
}

/// cout << overload for VALARRAY with MATLAB like output.
template<class T>
std::ostream& operator<<(std::ostream& xx, const std::valarray<T>& arr) {
    xx << "[";
    for (size_t i = 0; i < arr.size(); ++i) {
        xx << arr[i];
        if (i < arr.size() - 1) xx << ",";
    }
    xx << "]";
    return xx;
}

/// cout << overload for VECTOR with MATLAB like output.
template<class T>
std::ostream& operator<<(std::ostream& xx, const std::vector<T>& arr) {
    // do it like the matlab does it.
    xx << "[";
    for (size_t i = 0; i < arr.size(); ++i) {
        xx << arr[i];
        if (i < arr.size() - 1) xx << ";";
    }
    xx << "]";  //<< std::endl;
    return xx;
}

/// cout << overload for VECTOR < VECTOR> with MATLAB like output.
template<class T>
std::ostream& operator<<(std::ostream& xx, const std::vector<std::vector<T>>& arr) {
    xx << "[";
    for (size_t i = 0; i < arr.size(); ++i) {
        for (size_t j = 0; j < arr[i].size(); ++j) {
            xx << arr[i][j];
            if (j < arr[i].size() - 1) xx << ", ";
        }
        if (i < arr.size() - 1) xx << ";";
    }

    xx << "]";  // << std::endl;
    return xx;
}

}  // namespace std

namespace mm {

// print macro
//! @{
#define VA_NUM_ARGS(...) VA_NUM_ARGS_IMPL(__VA_ARGS__, 5, 4, 3, 2, 1, 0)
#define VA_NUM_ARGS_IMPL(_1, _2, _3, _4, _5, N, ...) N
#define macro_dispatcher(func, ...)     macro_dispatcher_(func, VA_NUM_ARGS(__VA_ARGS__))
#define macro_dispatcher_(func, nargs)  macro_dispatcher__(func, nargs)
#define macro_dispatcher__(func, nargs) func ## nargs
/// Quick and neat print function printing in matlab format.
#define addflag(a) {std::cout << "flags=[flags, " << (a) << "];" << std::endl;}
#define prnv2(a, b) {std::cout << a << " = " << (b) << ";" << std::endl;}
#define prnv1(a)   {std::cout << #a << " = " << (a) << ";" << std::endl;}
//! @}
/**
 * Prints a variable to standard output. Can take one or two parameters.
 * Example:
 * @code
 * int a = 6;
 * prn(a) // prints 'a=6'
 * prn("value", a) // prints 'value=6'
 * @endcode
 */
#define prn(...) macro_dispatcher(prnv, __VA_ARGS__)(__VA_ARGS__)
#ifdef NDEBUG
#define assert_msg(cond, ...) ((void)sizeof(cond))
#else
/**
 * @brief Assert with better error reporting.
 * @param cond Conditions to test.
 * @param ... The second parameter is also required and represents the message to print on failure.
 * For every %* field in message one additional parameter must be present.
 * Example:
 * @code
 * assert_msg(n > 0, "n must be positive, got %d.", n);
 * @endcode
 */
#define assert_msg(cond, ...) ((void)(!(cond) && \
    assert_internal::assert_handler(#cond, __FILE__, __LINE__, __VA_ARGS__) && (assert(false), 1)))
#endif

using tinyformat::printf;
using tinyformat::format;

/// Namespace holding custom assert implementation.
namespace assert_internal {
/**
 * Actual assert implementation.
 * @param condition Condition to test, e.g. `n > 0`.
 * @param file File where the assertion failed.
 * @param line Line on which the assertion failed.
 * @param message Message as specified in the assert_msg macro.
 * @param format_list List of format field values to pass to tinyformat::format function.
 */
bool assert_handler_implementation(const char* condition, const char* file, int line,
                                   const char* message, tfm::FormatListRef format_list);
/// Assert handler that unpacks varargs.
template<typename... Args>
bool assert_handler(const char* condition, const char* file, int line,
                    const char* message, const Args&... args) {  // unpacks first argument
    tfm::FormatListRef arg_list = tfm::makeFormatList(args...);
    return assert_handler_implementation(condition, file, line, message, arg_list);
}
}  // namespace assert_internal

/// Standard relative error tolerance for testing various geometric inclusions.
static const double EPS = 1e-6;
/// Threshold for what is considers a high conditional number. For conditional numbers
/// that are greater than this, MLS prints a warning
static const double HIGH_COND_NUMBER = 1e15;

/// print function for 2D C Pointer Array
template <class scalar_t>
void printArray(scalar_t arr[], int n, int m = 1) {
    for (int i = 0; i < m; i++) {
        std::cout << std::vector<scalar_t>(arr + i * n, arr + (i + 1) * n) << std::endl;
    }
}

/// print formatted for integers -- noop
std::ostream& print_formatted(int x, const std::string&, const std::string&,
                              const std::string&, const std::string&,
                              std::ostream& xx = std::cout);
/// print formatted for doubles -- always fixed
std::ostream& print_formatted(double x, const std::string&, const std::string&,
                              const std::string&, const std::string&,
                              std::ostream& xx = std::cout);
/// print formatted for vectors -- recursive
template <typename T>
std::ostream& print_formatted(const std::vector<T>& v, const std::string& before = "{",
                              const std::string& delimiter = ", ",
                              const std::string& after = "}",
                              const std::string finish = ";", std::ostream& xx = std::cout) {
    bool first = true;
    xx << before;
    for (const T& x : v) {
        if (!first) xx << delimiter;
        first = false;
        print_formatted(x, before, delimiter, after, "", xx);
    }
    return xx << after << finish;
}
/// print formatted for vectors -- recursive
template <typename matrix_t>
std::ostream& print_formatted_matrix(const matrix_t& v, const std::string& before = "{",
                                     const std::string& delimiter = ", ",
                                     const std::string& after = "}",
                                     const std::string finish = ";", std::ostream& xx = std::cout) {
    xx << before;
    int n = v.rows();
    int m = v.cols();
    for (int i = 0; i < n; ++i) {
        if (i > 0) xx << delimiter;
        xx << before;
        for (int j = 0; j < m; ++j) {
            if (j > 0) xx << delimiter;
            xx << v.coeff(i, j);
        }
        xx << after;
    }
    return xx << after << finish;
}

/**
 * Prints given text in bold red.
 * @param s text to print.
 */
void print_red(const std::string& s);
/**
 * Prints given text in bold white.
 * @param s text to print.
 */
void print_white(const std::string& s);
/**
 * Prints given text in bold green.
 * @param s text to print.
 */
void print_green(const std::string& s);

/// Signum overload for unsigned types
template <typename T>
inline constexpr int signum(T x, std::false_type) {
    return T(0) < x;
}
/// Signum overload for unsigned types
template <typename T>
inline constexpr int signum(T x, std::true_type) {
    return (T(0) < x) - (x < T(0));
}
/**
 * Signum function -- determines a sign of a number x.
 * @param x A number under inspection.
 * @return 0 if x == 0, -1 if x is negative and +1 if x is positive
 */
template <typename T>
inline constexpr int signum(T x) {
    return signum(x, std::is_signed<T>());
}

/**
 * Return a random seed. The seed is truly random if available, otherwise it is
 * current system time.
 */
unsigned int get_seed();  // NOLINT(*)

/**
 * @brief Our implementation of make_unique for smart pointers
 * @details Intel C Compiler has not yet implemented all c++14 features. One of
 * them includes std::make_unique. Therefore we have created our own
 * (actually we stole it from someone on StackOverflow)
 *
 * @tparam T Type of object that we want to create a pointer for
 * @param args All parameters (including the object we want to point at)
 * @return A unique pointer pointing to the given object
 */
template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args) {
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

/**
 * Simple function to help format memory amounts for printing. Takes in number of bytes
 * and returns a human readable representation.
 */
std::string mem2str(size_t bytes);

/**
 * Returns number of bytes the container uses in memory. The container must support `size()`.
 * This does not count the memory that may be allocated by objects stored in the container.
 * Also STL containers like vector may actually have more memory allocated than their size.
 */
template<typename container_t>
size_t mem_used(const container_t& v) {
    return sizeof(v[0]) * v.size();
}

/**
 * Splits string by `delim`, returning a vector of tokens (including empty).
 */
std::vector<std::string> split(const std::string& s, const std::string& delim);
/// Overload for char
std::vector<std::string> split(const std::string& s, char delim);

/**
 * Joins a vector of strings back together.
 * @param parts vector of strings
 * @param joiner string to glue the parts with
 */
std::string join(const std::vector<std::string>& parts, const std::string& joiner);
/// Overload for char
std::string join(const std::vector<std::string>& parts, char joiner);

/// Ceils a floating point to an integer.
template<typename T>
int iceil(T x) { return static_cast<int>(std::ceil(x)); }

/// Floors a floating point to an integer.
template<typename T>
int ifloor(T x) { return static_cast<int>(std::floor(x)); }

/**
 * @brief Simple timer class - add checkPoints throughout the code and measure
 * execution time between them.
 * @details
 * @code
 * timer.addCheckPoint("beginning");
 * //... tons of code ...
 * timer.addCheckPoint("intermediate");
 * // ...more code ...
 * timer.addCheckPoint("end");
 * timer.showTimings(); //shows all
 * timer.showTimings("beginning", "intermediate"); // shows time between two checkpoints
 * @endcode
 * When timing parts of code for example inside loops stopwatch should be used instead.
 * Stopwatch keeps track of cumulative time spent between all calls to
 * startWatch("label"), stopWatch("label") pairs for the same "label".
 * Number of laps (calls to said functions) and average time per lap is also available.
 * @code
 * for (...) {
 *     // ... code: NOT timed  ...
 *     timer.startWatch("loop1");
 *     // ... code: timed  ...
 *     timer.stopWatch("loop1");
 *     // ... code: NOT timed  ...
 * }
 * timer.getCumulativeTime("loop1"); // Returns cumulative time.
 * timer.getNumbLaps("loop1"); // Returns number of laps counted.
 * timer.getTimePerLap("loop1"); // Returns average time per lap.
 * @endcode
 */
class Timer {
  protected:
    /// time type
    typedef std::chrono::high_resolution_clock::time_point time_type;
    /** list of checkpoint labels */
    std::vector<std::string> labels;
    /** list of checkpoint times */
    std::vector<time_type> times;
    /** list of stopWatch labels */
    std::vector<std::string> stopwatch_labels;
    /** list of cumulative time */
    std::vector<double> cumulative_time;
    /** list of stopwatch counts */
    std::vector<int> stopwatch_counts;
    /** list of stopwatch times */
    std::vector<time_type> stopwatch_times;
    /** For tracking when stopwatch is running. */
    std::vector<bool> currently_running;
    /// Returns the ID of checkpoints with a given label.
    int getID(const std::string& label) const;
    /// Returns the ID of stopwatch with a given label.
    int stopwatchGetID(const std::string& label) const;
  public:
    /**
     * Adds a checkpoint with given label and remembers the current time.
     * @returns The unique id of the checkpoint.
     */
    int addCheckPoint(const std::string& label);
    /// Pretty print all timings.
    void showTimings(std::ostream& os = std::cout) const;
    /**
     * @brief Output timings between the checkpoints with given labels to `os`.
     * Example: `showTimings("begin", "end")`;
     */
    void showTimings(const std::string& from, const std::string& to,
                     std::ostream& os = std::cout) const;
    /**
     * @brief Output timings between the checkpoints with given id's to `os`.
     * Example: `showTimings("begin", "end")`;
     */
    void showTimings(int from, int to, std::ostream& os = std::cout) const;
    /// Return absolute time for a given id.
    time_type getTime(int id) const;
    /// Return absolute time for a given label.
    time_type getTime(const std::string& label) const;
    /// Return time difference in seconds between two checkpoints.
    double getTime(const std::string& from, const std::string& to) const;
    /// Return time difference in seconds between `from` and now.
    double getTimeToNow(const std::string& from) const;
    /// Return the number of measurements taken.
    int size() const;
    /// Return all labels.
    const std::vector<std::string>& getLabels() const;

    /// Clear all of time points.
    void clear();
    /**
     * @brief Starts the stopwatch with a given label.
     */
    void startWatch(const std::string& label);
    /**
     * @brief stops the stopwatch with a given label.
     * Updates cumulative time and number of laps information for the given label.
     * Individual lap durations are not saved.
     */
    void stopWatch(const std::string& label);
    /// Returns total time of all laps for a given label.
    double getCumulativeTime(const std::string& label);
    /// Returns number of laps for a given label.
    int getNumLaps(const std::string& label);
    /// Returns average time spent per lap.
    double getTimePerLap(const std::string& label);
    /// Clear all stopwatch related data.
    void stopwatchClear();
};

/**
 * Solves `f(x) = target` using bisection.
 * @tparam function_t Function type, like std::function or lambda, that returns s_type.
 * @tparam input_t Floating point data type, that support arithmetic operations.
 * @tparam output_t Floating point data type, such that `mm::signum(output_t)` can be called.
 * @tparam verbose Reports approximation every iteration.
 * @param f Function mapping `input_t -> output_t`, for which to solve `f(x) = target`.
 * @param lo Lower bound of the interval, containing the solution.
 * @param hi Upper bound of the interval, containing the solution.
 * @param target Target value, default 0.
 * @param tolerance Accuracy of the provided solution, default 1e-4.
 * @param max_iter Maximal number of iterations.
 * @return x
 */
template<typename function_t, typename input_t, typename output_t, bool verbose = false>
input_t bisection(const function_t& f, input_t lo, input_t hi, output_t target = 0.0,
                  input_t tolerance = 1e-4, int max_iter = 40) {
    input_t a = lo, b = hi, c;
    output_t fa = f(a) - target;
    output_t fb = f(b) - target;

    assert_msg(signum(fa) != signum(fb),
               "Function values have the same sign, f(a) = %s, f(b) = %s.", fa, fb);

    while (std::abs(b - a) > tolerance) {
        c = a + (b - a) / static_cast<input_t>(2);
        input_t fc = f(c) - target;

        if (verbose) std::cout << "f(" << c << ")=" << fc + target << "\n";
        if (signum(fa) == signum(fc)) {
            a = c;
            fa = fc;
        } else {
            b = c;
        }
        --max_iter;

        if (max_iter < 0) {
            std::cerr << "\nBisection ended without finding the solution after max_num"
                                        " iterations." << std::endl;
            break;
        }
    }
    return c;
}

/// Sorts a container inplace.
template<typename container_t>
container_t& sort(container_t& v) {
    std::sort(std::begin(v), std::end(v));
    return v;
}

/// Sorts a container inplace according to ordering defined by `pred`.
template<typename container_t, typename predicate_t>
container_t& sort(container_t& v, const predicate_t& pred) {
    std::sort(std::begin(v), std::end(v), pred);
    return v;
}

/// Returns a sorted copy of container.
template<typename container_t>
container_t sorted(container_t v) {
    std::sort(std::begin(v), std::end(v));
    return v;
}

/// Returns a sorted copy of container ordered according to `pred`.
template<typename container_t, typename predicate_t>
container_t sorted(container_t v, const predicate_t& pred) {
    std::sort(std::begin(v), std::end(v), pred);
    return v;
}

/**
 * Pads a ragged array with given value.
 * Example:
 * @code
 * pad({{1, 2}, {}, {9, 4, 2}}, -1);  // return {{1, 2, -1}, {-1, -1, -1}, {9, 4, 2}}
 * @endcode
 */
template <typename container_t, typename T>
container_t pad(container_t container, T value) {
    size_t maxsize = 0;
    for (const auto& x : container) {
        if (x.size() > maxsize) {
            maxsize = x.size();
        }
    }
    for (auto& x : container) {
        for (size_t i = x.size(); i < maxsize; ++i) x.push_back(value);
    }
    return container;
}

}  // namespace mm

#endif  // SRC_COMMON_HPP_
