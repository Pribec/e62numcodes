#include "domain_refine_engines.hpp"
namespace mm {

HalfLinksRefine& HalfLinksRefine::min_dist(double fraction) {
    fraction_ = fraction; return *this;
}

HalfLinksRefine& HalfLinksRefine::region(Range<int> region) {
    region_ = std::move(region); return *this;
}

HalfLinksRefine::HalfLinksRefine(double fraction) : region_(), fraction_(fraction) {}


HalfLinksSmoothRefine::HalfLinksSmoothRefine(double fraction, double max_imbalance) :
        region_(), fraction_(fraction), max_imbalance_(max_imbalance) {}

HalfLinksSmoothRefine& HalfLinksSmoothRefine::region(Range<int> region) {
    region_ = std::move(region); return *this;
}

HalfLinksSmoothRefine& HalfLinksSmoothRefine::min_dist(double fraction) {
    fraction_ = fraction; return *this;
}

HalfLinksSmoothRefine& HalfLinksSmoothRefine::max_imbalance(double max_imbalance) {
    max_imbalance_ = max_imbalance; return *this;
}

}  // namespace mm
