#ifndef SRC_LBM_HPP_
#define SRC_LBM_HPP_

#include <tuple>
#include <cmath>

#include "types.hpp"
#include "mlsm_operators.hpp"

namespace mm {
namespace lbm {

template< typename MLSM_t, typename latt_t >
class LBMop
{
public:

    // typedefs
    typedef typename MLSM_t::vec_t vec_t;  ///< type of vectors
    typedef typename MLSM_t::scalar_t scalar_t;  ///< type of scalars
    static const int dim = vec_t::dimension;  ///< dimension of the domain
    static const int q = latt_t::dimension; ///< number of lattice vectors
    
    
    // constructor
    LBMop(MLSM_t MLSM) : MLSM_( MLSM ) {
        size_ = MLSM_.getSize();
    }

    Eigen::Matrix< scalar_t, q, dim > fgrad(Eigen::VectorXd f, int node) const
    {
        Eigen::Matrix< scalar_t, q, dim > ret(q,dim);
        ret.setZero();
        for (int k = 0; k < q; ++k)
        {
            Range< scalar_t > fk = reshape( f.segment(k*size_,size_) );
            ret.row( k ) = MLSM_.grad( fk, node );
        }
        return ret;
    }

private:
    MLSM_t MLSM_;
    int size_;
};

template< typename scalar_t, typename latt_t >
class Pdf : public Vec< scalar_t, latt_t::dimension >
{
public:
    
    typedef typename latt_t::vec_t vec_t;
    static const int dim = latt_t::dimension;  ///< number of lattice vectors

    scalar_t getDensity()
    {
        return this->sum();
    }

    scalar_t getDensityAndVelocity(vec_t & u)
    {
        scalar_t rho = this->sum();
        u = 0.0;
        for (int i = 0; i < dim; ++i)
        {
            u += this->operator[](i)*latt_t::c[i];
        }
        u /= rho;
        return rho;
    }

private:
}; // class Pdf


/*template< typename latt_t >
class BgkEquilibriumDistribution
{
public:

    typedef typename latt_t::pdf_t pdf_t;
    typedef typename latt_t::vec_t vec_t;
    typedef typename vec_t::scalar_t scalar_t;

    BgkEquilibriumDistribution( latt_t lattice ) : lattice_( lattice ) {
        scalar_t cs = lattice.speedOfSound();
        invcsqr = 1.0/(cs*cs);
    };

    pdf_t operator()( scalar_t rho, vec_t u)
    {
        pdf_t feq = 0.0;
        for (int i = 0; i < pdf_t::dim; ++i)
        {
            scalar_t usqr = u.dot(u);
            scalar_t cu = (latt_t::c[i]).dot(u);

            feq[i] = latt_t::w[i]*rho*(1.0 + 0.5*invcsqr*cu + 0.5*invcsqr*invcsqr*cu*cu + 0.5*invcsqr*usqr);
        }
        return feq;
    }
private:
    latt_t lattice_;
    scalar_t invcsqr;
} // class BgkEquilibriumDistribution*/

} // namespace lbm
} // namespace mm

#endif  // SRC_LBM_HPP_
