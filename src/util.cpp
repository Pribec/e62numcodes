#include "util.hpp"

namespace mm {

/**
 * @file util.cpp
 * @brief Some implementations of functions declared in util.hpp.
 */

Range<double> linspace(double beg, double end, int count, bool include_boundary) {
    Range<double> ret;
    if (include_boundary) {
        assert(count >= 2);
        double step = (end - beg) / (count - 1);
        for (int i = 0; i < count; ++i) {
            ret.push_back(beg + step * i);
        }
    } else {
        assert(count >= 0);
        double step = (end - beg) / (count + 1);
        for (int i = 1; i <= count; ++i) {
            ret.push_back(beg + step * i);
        }
    }
    return ret;
}

#ifndef __MIC__
Monitor::Monitor() {
    m = PCM::getInstance();
    m->resetPMU();
    PCM::ErrorCode returnResult = m->program();
    if (returnResult != PCM::Success) {
        print_red("Intel's PCM couldn't start (try running with sudo)\n");
        std::cerr << "Error code: " << returnResult << std::endl;
        std::cerr << "Try running: \n\n"
                  << "\tsudo su\n"
                  << "\techo 0 > /proc/sys/kernel/nmi_watchdog\n"
                  << "\texit\n"
                  << "\tsudo modprobe msr\n"
                  << "\tsudo chown $USER /dev/cpu/*/msr\n"
                  << "\tsudo ./performance_test\n\n";
    }
}
Monitor::~Monitor() { m->cleanup(); }
int Monitor::addCheckPoint(const std::string& label) {
    int id = Timer::addCheckPoint(label);
    ids.push_back(id);
    counter_states.push_back(getSystemCounterState());
    return id;
}
SystemCounterState Monitor::getState(const std::string& label) const {
    return getState(getID(label));
}
SystemCounterState Monitor::getState(int id) const {
    return counter_states[getStateID(id)];
}
int Monitor::getStateID(int id) const {
    auto it = std::find(ids.begin(), ids.end(), id);
    assert(it != ids.end() && "No such ID recorded.");
    return it - ids.begin();
}
double Monitor::getL2CacheHitRatio(const std::string& from, const std::string& to) {
    return ::getL2CacheHitRatio(getState(from), getState(to));
}
double Monitor::getL3CacheHitRatio(const std::string& from, const std::string& to) {
    return ::getL3CacheHitRatio(getState(from), getState(to));
}
void Monitor::showTimingAndCacheRatios(const std::string& from, const std::string& to,
                                       std::ostream& os) const {
    showTimingAndCacheRatios(getID(from), getID(to), os);
}
void Monitor::showTimingAndCacheRatios(int from, int to, std::ostream& os) const {
    int fromID = getStateID(from);
    int toID = getStateID(to);
    os << std::left << std::setw(20) << labels[from] << " -- "
       << std::setw(20) << labels[to] << std::setw(20)
       << std::chrono::duration<double>(times[to] - times[from]).count()
       << std::setw(22) << "[s];  L3CacheHitRatio: " << std::setw(20)
       << ::getL3CacheHitRatio(counter_states[fromID], counter_states[toID])
       << std::setw(18) << "  L2CacheHitRatio: " << std::setw(20)
       << ::getL2CacheHitRatio(counter_states[fromID], counter_states[toID])
       << std::endl;
}
void Monitor::clear() {
    Timer::clear();
    counter_states.clear();
    ids.clear();
}
#endif  // ifndef __MIC__

}  // namespace mm
