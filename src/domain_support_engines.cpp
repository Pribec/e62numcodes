#include "domain_support_engines.hpp"

namespace mm {

FindBalancedSupport::FindBalancedSupport(int min_support, int max_support) :
        min_support(min_support), max_support(max_support), for_which_(), search_among_() {}

FindBalancedSupport& FindBalancedSupport::forNodes(Range<int> for_which) {
    this->for_which_ = std::move(for_which);
    return *this;
}

FindBalancedSupport& FindBalancedSupport::searchAmong(Range<int> search_among) {
    this->search_among_ = std::move(search_among);
    return *this;
}

}  // namespace mm
