#ifndef SRC_MLSM_OPERATORS_HPP_
#define SRC_MLSM_OPERATORS_HPP_

#include "common.hpp"
#include "domain.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "types.hpp"
#include "shape_storage.hpp"

/**
 * @file mlsm_operators.hpp
 * Basic Meshless Local Strong Form operators. This file contains MLSM class and its factories,
 * designed to make PDE solving easier and more expressive.
 * @example mlsm_operators_test.cpp
 */

namespace mm {

/**
 * Performs all asserts for a given node: asserts that node index is valid and that shape functions
 * were initialized for a given node.
 * @param node Node index to consider.
 */
#define NODE_ASSERTS(node) \
    assert_msg(0 <= (node) && (node) < ss.size(), "Node index %d must be in range " \
               "[0, num_nodes = %d).", ss.size());

/**
 * @brief A class for evaluating typical operators needed in spatial discretization
 * @tparam shape_storage_type Storage class for shapes.
 *
 * @sa ShapeStorage
 */
template <class shape_storage_type>
class MLSM {
  public:
    typedef shape_storage_type shape_storage_t;   ///< shape storage style
    typedef typename shape_storage_t::vec_t vec_t;  ///< type of vectors
    typedef typename shape_storage_t::scalar_t scalar_t;  ///< type of scalars
    static const int dim = vec_t::dimension;  ///< dimension of the domain

  private:
    /// Shape storage, but name is shortened for readability.
    const shape_storage_t ss;

  public:
    /// Make operators using given shape functions.
    explicit MLSM(const shape_storage_t& shape_storage) : ss(shape_storage) {}

    /// initialize
    MLSM() = default;

    /// Set new shapes.
    void set_shapes(const shape_storage_t& shape_storage_) {
        ss = shape_storage_;
    }

    /**
     * Sets implicit equation that value of a field is equal to some other value.
     * @param M matrix representing the equations for all nodes.
     * @param node index of a node from 0 to `N` for which to write the equation for.
     * This means only the `node`-th row of the matrix is changed.
     * User must make sure that the matrix is large enough.
     * @param alpha scalar to multiply the operator with.
     * @details Fills the `node`-th row of matrix `M` so that `node`-th row of the equation
     * @f[ M u = v @f]
     * is a good approximation of the equation
     * @f[ \alpha u(node) = v(node) @f]
     * This is handy when solving equations implicitly.
     */
    template<class matrix_t>
    void value(matrix_t& M, int node, scalar_t alpha = 1.0) const {
        assert_msg(0 <= node && node < ss.size(), "Node index %d must be in range "
                   "[0, shape_storage->size() = %d).", ss.size());
        assert_msg(M.rows() >= ss.size(), "Matrix does not have enough rows, "
                   "expected %d, got %d.", ss.size(), M.rows());
        assert_msg(M.cols() >= ss.size(), "Matrix does not have enough columns, "
                   "expected %d, got %d.", ss.size(), M.cols());
        M.coeffRef(node, node) += alpha;
    }

    /**
     * Sets implicit equation that value of a vector field is equal to some other value.
     * @param M matrix representing the equations for all nodes. If `N` is the number of domain
     * nodes, then `M` is a `3N x 3N` matrix. Vector field `u` is considered to be represented as
     * @f$ u_j(node) = u[j*dim + node]@f$ or equivalently
     * @f$ \vec{u} = \begin{bmatrix} u_0 \\ u_1 \\ u_2 \end{bmatrix} @f$.
     * @param node index of a node from 0 to `N` for which to write the equation for.
     * This means only `node`-th, `N+node`-th and `2N+node`-th rows of the matrix are changed.
     * User must make sure that the matrix is large enough.
     * @param alpha scalar to multiply the operator with.
     * @details Fills the rows of matrix `M` so that `node`-th, `N+node`-th and `2N+node`-th rows
     * of the equation
     * @f[ M u = v @f]
     * are a good approximation of the equation
     * @f[ \alpha \vec{u}(node) = \vec{v}(node) @f]
     * This is handy when solving equations implicitly.
     *
     * @note While 3 was used as the dimension of the vector field in the above description,
     * vector fields of arbitrary dimensions are supported. The given matrix must have dimensions
     * `dim*N x dim*N` and `dim` rows at indexes `node`, `N+node`, ..., `N*(dim-1)+node` are
     * modified.
     */
    template<class matrix_t>
    void valuevec(matrix_t& M, int node, scalar_t alpha = 1.0) const {
        assert_msg(0 <= node && node < ss.size(), "Node index %d must be in range "
                  "[0, shape_storage->size() = %d).", ss.size());
        assert_msg(M.rows() >= dim*ss.size(), "Matrix does not have enough rows, "
                   "expected %d, got %d.", dim*ss.size(), M.rows());
        assert_msg(M.cols() >= dim*ss.size(), "Matrix does not have enough columns, "
                   "expected %d, got %d.", dim*ss.size(), M.cols());
        for (int d = 0; d < dim; ++d) {
            M.coeffRef(node + d*ss.size(), node + d*ss.size()) += alpha;
        }
    }

    /**
     * @brief Returns laplace of field u in node-th node.
     * @param u field to be considered
     * @param node index of a node where the operators is evaluated
     * @details @f$\sum(shape_{\nabla^2}[i]u_{x}[i])@f$
     * @return scalar
     */
    template<template<class> class container>
    scalar_t lap(const container<scalar_t>& u, int node) const {
        NODE_ASSERTS(node);
        scalar_t lap = 0;
        for (int i = 0; i < ss.support_size(node); ++i) {
            lap += ss.laplace(node, i) * u[ss.support(node, i)];
        }
        return lap;
    }

    /**
     * @brief Fill the matrix with the equation for the Laplacian of a scalar
     * field @f$\vec{u}@f$ at the point `node` being equal to some value.
     * This is the implicit version of scalar MLSM::lap(u, node) function above.
     * @param M matrix representing the equations for all nodes.
     * @param node index of a node from 0 to `N` for which to write the equation for.
     * This means only the `node`-th row of the matrix is changed.
     * User must make sure that the matrix is large enough.
     * @param alpha scalar to multiply the operator with.
     * @details Fills the `node`-th row of matrix `M` so that `node`-th row of the equation
     * @f[ M u = v @f]
     * is a good approximation of the differential equation
     * @f[ \alpha \triangle u(node) = v(node) @f]
     * This is handy when solving equations implicitly.
     *
     * Example:
     * @snippet mlsm_operators_test.cpp Implicit laplace 1d example
     */
    template<class matrix_t>
    void lap(matrix_t& M, int node, scalar_t alpha) const {
        NODE_ASSERTS(node);
        assert_msg(M.rows() >= ss.size(), "Matrix does not have enough rows, "
                   "expected %d, got %d.", ss.size(), M.rows());
        assert_msg(M.cols() >= ss.size(), "Matrix does not have enough columns, "
                   "expected %d, got %d.", ss.size(), M.cols());
        for (int i = 0; i < ss.support_size(node); ++i) {
            M.coeffRef(node, ss.support(node, i)) += alpha * ss.laplace(node, i);
        }
    }

    /**
     * @brief returns Laplace of vector field u in node-th node.
     * @param u field to be considered
     * @param node index of a node where the operator is evaluated
     * @details @f$\sum(shape_{\nabla^2}[i]u_{x}[i])@f$ for all dimensions
     * @return vector
     */
    template<template<class> class container>
    vec_t lap(const container<vec_t>& u, int node) const {
        NODE_ASSERTS(node);
        vec_t lap = static_cast<scalar_t>(0.);
        for (int i = 0; i < ss.support_size(node); ++i) {
            for (int d = 0; d < dim; ++d) {
                lap[d] += ss.laplace(node, i) * u[ss.support(node, i)][d];
            }
        }
        return lap;
    }

    /**
     * @brief Fill the matrix with the equation for Laplacian of a vector field @f$\vec{u}@f$
     * at the point `node` being equal to some other field.
     * This is the implicit version of `lap(u, node)` function above, but cannot be named the
     * same due to signature clash with the scalar version `lap(M, node, alpha)`.
     * @param M matrix representing the equations for all nodes. If `N` is the number of domain
     * nodes, then `M` is a `3N x 3N` matrix. Vector field `u` is considered to be represented as
     * @f$ u_j(node) = u[j*dim + node]@f$ or equivalently
     * @f$ \vec{u} = \begin{bmatrix} u_0 \\ u_1 \\ u_2 \end{bmatrix} @f$.
     * @param node index of a node from 0 to `N` for which to write the equation for.
     * This means only `node`-th, `N+node`-th and `2N+node`-th rows of the matrix are changed.
     * User must make sure that the matrix is large enough.
     * @param alpha scalar to multiply the operator with.
     * @details Fills the rows of matrix `M` so that `node`-th, `N+node`-th and `2N+node`-th rows
     * of the equation
     * @f[ M u = v @f]
     * are a good approximation of the differential equation
     * @f[ \alpha \triangle \vec{u}(node) = \vec{v}(node) @f]
     * This is handy when solving equations implicitly.
     *
     * @note While 3 was used as the dimension of the vector field in the above description,
     * vector fields of arbitrary dimensions are supported. The given matrix must have dimensions
     * `dim*N x dim*N` and `dim` rows at indexes `node`, `N+node`, ..., `N*(dim-1)+node` are
     * modified.
     *
     * Example:
     * @snippet mlsm_operators_test.cpp Implicit vector laplace 2d example
     */
    template<class matrix_t>
    void lapvec(matrix_t& M, int node, scalar_t alpha) const {
        NODE_ASSERTS(node);
        assert_msg(M.rows() >= dim*ss.size(), "Matrix does not have enough rows, "
                   "expected %d, got %d.", dim*ss.size(), M.rows());
        assert_msg(M.cols() >= dim*ss.size(), "Matrix does not have enough columns, "
                   "expected %d, got %d.", dim*ss.size(), M.cols());
        for (int d = 0; d < dim; ++d) {
            for (int i = 0; i < ss.support_size(node); ++i) {
                M.coeffRef(d*ss.size() + node,
                           d*ss.size() + ss.support(node, i)) += alpha * ss.laplace(node, i);
            }
        }
    }


    /**
     * @brief returns gradient of a scalar field u in node-th node.
     * @param u field to be considered
     * @param node index of a node where the operators is evaluated
     * @details return
     * @f$[\sum_i shape_{dx}[i]u[i],  \sum_i shape_{dy}[i]u[i], \sum_i shape_{dz}[i]u[i]] @f$
     * @return vector containing the components of the gradient, eg. @f$[u_x(node), u_y(node),
     * u_z(node)]@f$.
     */
    template<template<class> class container>
    vec_t grad(const container<scalar_t>& u, int node) const {
        NODE_ASSERTS(node);
        vec_t ret = static_cast<scalar_t>(0.);
        for (int var = 0; var < dim; ++var)  // loop over dimensions
            for (int i = 0; i < ss.support_size(node); ++i)
                ret[var] += ss.d1(var, node, i) * u[ss.support(node, i)];
        return ret.transpose();
    }

    /**
     * @brief Fill the matrix with the equation for gradient of a scalar field @f$S@f$
     * at the point `node` along another field @f$\vec{v}@f$ being equal to some other scalar field
     * @f$\psi@f$.
     * @param M matrix representing the equations for all nodes. If `N` is the number of domain
     * nodes, then `M` is a `N x N` matrix.
     * @param node index of a node from 0 to `N` for which to write the equation for.
     * This means only `node`-th row of the matrix is changed.
     * User must make sure that the matrix is large enough.
     * @param v vector field to multiply the operator with.
     * @param row write equation in this specific row
     * @details Fills the rows of matrix `M` so that `node`-th row of the equation
     * @f[ M S = \psi @f]
     * is a good approximation of the differential equation
     * @f[ \vec{v}(node) (\operatorname{grad}S)(node) = \psi(node) @f]
     * This is handy when solving equations implicitly.
     *
     * Example:
     * @snippet mlsm_operators_test.cpp Implicit Grad example
    */
    template<class matrix_t>
    void grad(matrix_t& M, int node, const vec_t& v, int row = -1) const {
        NODE_ASSERTS(node);
        if (row == -1) row = node;
//        assert_msg(M.rows() >= ss.size(), "Matrix does not have enough rows, "
//                   "expected %d, got %d.", ss.size(), M.rows());
//        assert_msg(M.cols() >= ss.size(), "Matrix does not have enough columns, "
//                   "expected %d, got %d.", ss.size(), M.cols());
        for (int i = 0; i < ss.support_size(node); ++i) {
            scalar_t& value = M.coeffRef(row, ss.support(node, i));
            for (int var = 0; var < dim; ++var) {
                value += v[var] * ss.d1(var, node, i);
            }
        }
    }

    /**
      * @brief Fill the matrix with the equation for directional derivative of @f$u@f$
      * at the point `node` along direction @f$\vec{n}@f$ being equal to some other scalar field
      * @f$\psi@f$.
      * @param M matrix representing the equations for all nodes. If `N` is the number of domain
      * nodes, then `M` is a `N x N` matrix.
      * @param node index of a node from 0 to `N` for which to write the equation for.
      * This means only `node`-th row of the matrix is changed.
      * User must make sure that the matrix is large enough.
      * @param unit_normal direction in which to take the derivative. To set Neumann BC, this
      * vector must be of unit length.
      * @param alpha scalar to multiply the Neumann coefficients with
      * @param row write equation in this specific row
      * @details Fills the rows of matrix `M` so that `node`-th row of the equation
      * @f[ M u = \psi @f]
      * is a good approximation of the differential equation
      * @f[ \frac{\partial u}{\partian \vec{n}} = \psi(node) @f]
      * This is handy when solving equations implicitly and for setting Neumann boundary conditions.
      */
    template<class matrix_t>
    void neumann_implicit(matrix_t& M, int node, const vec_t& unit_normal, scalar_t alpha,
                         int row = -1) const {
        if (row == -1) { row = node; }
        assert_msg(unit_normal.norm() > 1e-15, "Normal is almost zero, got %s.", unit_normal);
        grad(M, node, unit_normal*alpha, row);
    }

    /**
     * @brief Returns gradient of a vector field @f$ \vec u @f$ in `node`-th node.
     * @param u Vector field to be considered.
     * @param node Index of a node where the operators is evaluated.
     * @return Matrix of a gradient of a vector field @f$ \nabla \vec u @f$.
     *   The returned structure is a matrix @f$ \mathrm{grad} @f$ such that
     *   @f$ \mathrm{grad}(i, j) = \frac{\partial u_i}{\partial x_j} @f$. e.g.
     *   @f$ \mathrm{grad}(0, 0) = \frac{\partial u_x}{\partial x} @f$,
     *   @f$ \mathrm{grad}(0, 1) = \frac{\partial u_x}{\partial y} @f$,
     *   @f$ \mathrm{grad}(1, 0) = \frac{\partial u_y}{\partial x} @f$.
     *
     * @details For 3d vector field it returns
     *   @f$ \left[\sum\limits_{i}\mathrm{shape}_{\mathrm dx}[i]\vec u[i],
     *        \sum\limits_{i}\mathrm{shape}_{\mathrm dy}[i]\vec u[i],
     *        \sum\limits_{i}\mathrm{shape}_{\mathrm dz}[i]\vec u[i] \right]
     *   @f$
     *   @f[
     *     \mathrm{grad} = \begin{bmatrix}
     *         \frac {\partial u_1}{\partial x_1} & \frac {\partial u_1}{\partial x_2} & \cdots \\
     *         \frac {\partial u_2}{\partial x_1} & \frac {\partial u_2}{\partial x_2} & \cdots \\
     *         \vdots & \vdots & \ddots
     *     \end{bmatrix}
     *   @f]
     */
    template<template<class> class container>
    Eigen::Matrix<scalar_t, dim, dim> grad(const container<vec_t>& u, int node) const {
        NODE_ASSERTS(node);
        Eigen::Matrix<scalar_t, dim, dim> ret(dim, dim);
        ret.setZero();
        for (int d = 0; d < dim; ++d)
            for (int var = 0; var < dim; ++var)  // loop over dimensions
                for (int i = 0; i < ss.support_size(node); ++i)
                    ret(var, d) += ss.d1(var, node, i) * u[ss.support(node, i)][d];
        return ret.transpose();
    }

    /**
     * @brief Fill the matrix with the equation for gradient of a vector field @f$u@f$
     * at the point `node` along another field @f$\vec{v}@f$ being equal to some other vector field
     * @f$f@f$. the name `gradvec` is due to a name clash with the scalar version above.
     * @param M matrix representing the equations for all nodes. If `N` is the number of domain
     * nodes, then `M` is a `3N x 3N` matrix. Vector field `u` is considered to be represented as
     * @f$ u_j(node) = u[j*dim + node]@f$ or equivalently
     * @f$ \vec{u} = \begin{bmatrix} u_0 \\ u_1 \\ u_2 \end{bmatrix} @f$.
     * @param node index of a node from 0 to `N` for which to write the equation for.
     * This means only `node`-th, `N+node`-th and `2N+node`-th rows of the matrix are changed.
     * User must make sure that the matrix is large enough.
     * @param v vector to multiply the operator with.
     * @details Fills the rows of matrix `M` so that `node`-th, `N+node`-th and `2N+node`-th rows
     * of the equation
     * @f[ M u = v @f]
     * are a good approximation of the differential equation
     * @f[ (\vec{v} \nabla) \vec{u}(node) = \vec{f}(node) @f]
     * This is handy when solving equations implicitly.
     *
     * @note While 3 was used as the dimension of the vector field in the above description,
     * vector fields of arbitrary dimensions are supported. The given matrix must have dimensions
     * `dim*N x dim*N` and `dim` rows at indexes `node`, `N+node`, ..., `N*(dim-1)+node` are
     * modified.
     *
     * Example:
     * @snippet mlsm_operators_test.cpp Implicit Gradvec example
    */
    template<class matrix_t>
    void gradvec(matrix_t& M, int node, const vec_t& v) const {
        NODE_ASSERTS(node);
        assert_msg(M.rows() >= dim*ss.size(), "Matrix does not have enough rows, "
                   "expected %d, got %d.", dim*ss.size(), M.rows());
        assert_msg(M.cols() >= dim*ss.size(), "Matrix does not have enough columns, "
                   "expected %d, got %d.", dim*ss.size(), M.cols());
        for (int i = 0; i < ss.support_size(node); ++i) {
            scalar_t shape_value = 0;
            for (int var = 0; var < dim; ++var) {
                shape_value += v[var] * ss.d1(var, node, i);
            }
            for (int d = 0; d < dim; ++d) {
                M.coeffRef(d*ss.size() + node,
                           d*ss.size() + ss.support(node, i)) += shape_value;
            }
        }
    }

    /**
     * @brief Returns divergence of a vector field `u` in `node`-th node.
     * @details return @f$\sum(shape_{dx}[i]u_{x}[i]) + \sum(shape_{dy}[i]u_y[i]_{x_i}]) + ...@f$
     * @param u field to be considered
     * @param node index of a node where the operators is evaluated
     * @return scalar div value of the given field at the point `node`
     */
    template<template<class> class container>
    scalar_t div(const container<vec_t>& u, int node) const {
        NODE_ASSERTS(node);
        scalar_t ret = 0;
        for (int var = 0; var < dim; ++var)
            for (int i = 0; i < ss.support_size(node); ++i)
                ret += ss.d1(var, node, i) * u[ss.support(node, i)][var];
        return ret;
    }

    /**
     * @brief Returns gradient of a div vector field @f$ \vec u @f$
     * in `node`-th node @f$ \nabla(\nabla\cdot\vec u)|_{\mathrm{node}}@f$.
     * @param u Vector field to be considered.
     * @param node Index of a node where the operators is evaluated.
     * @return Vector of gradient of a div of a vector field.
     *
     * @details For 3d vector field it returns
     *   @f$ \sum\limits_{i=0}^{\mathrm{dim}}\sum\limits_{j\in S_\mathrm{node}} \left[
     *        \mathrm{shape}_{\mathrm dx\mathrm di}[j] u_i[j],
     *        \mathrm{shape}_{\mathrm dy\mathrm di}[j] u_i[j],
     *        \mathrm{shape}_{\mathrm dz\mathrm di}[j] u_i[j]
     *       \right] ^ T
     *   @f$
     *
     *   Which mathematically equals
     *
     *   @f[
     *     \mathrm{graddiv} = \left[
     *         \frac {\partial}{\partial x_1} \sum\limits_{i=1}^{\mathrm{dim}}
     *           \frac{\partial u_i}{\partial x_i},
     *         \frac {\partial}{\partial x_2} \sum\limits_{i=1}^{\mathrm{dim}}
     *           \frac{\partial u_i}{\partial x_i},
     *         \cdots,
     *         \frac {\partial}{\partial x_{\mathrm{dim}}} \sum\limits_{i=1}^{\mathrm dim}
     *           \frac{\partial u_i}{\partial x_i}
     *     \right]^T
     *   @f]
     */
    template<template<class> class container>
    vec_t graddiv(const container<vec_t>& u, int node) const {
        NODE_ASSERTS(node);
        vec_t ret;
        ret.setZero();
        for (int d1 = 0; d1 < dim; ++d1) {
            for (int d2 = 0; d2 < dim; ++d2) {  // loop over dimensions
                int dmin = std::min(d1, d2);
                int dmax = std::max(d1, d2);
                for (int i = 0; i < ss.support_size(node); ++i) {
                    ret[d1] += ss.d2(dmin, dmax, node, i) * u[ss.support(node, i)][d2];
                }
            }
        }
        return ret;
    }

    /**
     * @brief Fill the matrix with the equation for gradient of divergence of a vector field
     * @f$\vec{u}@f$ at the point `node` being equal to some other field.
     * This is the implicit version of `graddiv(u, node)` function above.
     * @param M matrix representing the equations for all nodes. If `N` is the number of domain
     * nodes, then `M` is a `3N x 3N` matrix. Vector field `u` is considered to be represented as
     * @f$ u_j(node) = u[j*dim + node]@f$ or equivalently
     * @f$ \vec{u} = \begin{bmatrix} u_0 \\ u_1 \\ u_2 \end{bmatrix} @f$.
     * @param node index of a node from 0 to `N` for which to write the equation for.
     * This means only `node`-th, `N+node`-th and `2N+node`-th rows of the matrix are changed.
     * User must make sure that the matrix is large enough.
     * @param alpha scalar to multiply the operator with.
     * @details Fills the rows of matrix `M` so that `node`-th, `N+node`-th and `2N+node`-th rows
     * of the equation
     * @f[ M u = v @f]
     * are a good approximation of the differential equation
     * @f[ \alpha \nabla(\nabla \cdot \vec{u})(node) = \vec{v}(node) @f]
     * This is handy when solving equations implicitly.
     *
     * @note While 3 was used as the dimension of the vector field in the above description,
     * vector fields of arbitrary dimensions are supported. The given matrix must have dimensions
     * `dim*N x dim*N` and `dim` rows at indexes `node`, `N+node`, ..., `N*(dim-1)+node` are
     * modified.
     *
     * Example:
     * @snippet mlsm_operators_test.cpp Implicit graddiv example
     */
    template<class matrix_t>
    void graddiv(matrix_t& M, int node, scalar_t alpha) const {
        NODE_ASSERTS(node);
        assert_msg(M.rows() >= dim*ss.size(), "Matrix does not have enough rows, "
                   "expected %d, got %d.", dim*ss.size(), M.rows());
        assert_msg(M.cols() >= dim*ss.size(), "Matrix does not have enough columns, "
                   "expected %d, got %d.", dim*ss.size(), M.cols());

        // Implicit equation is for all d: \sum_k \sum_j \chi_djk u_j(s_k) = v_d(s)
        for (int d = 0; d < dim; ++d) {
            for (int k = 0; k < ss.support_size(node); ++k) {
                for (int j = 0; j < dim; ++j) {  // loop over dimensions
                    int dmin = std::min(d, j);
                    int dmax = std::max(d, j);
                    M.coeffRef(d*ss.size() + node,
                               j*ss.size() + ss.support(node, k)) +=
                            alpha * ss.d2(dmin, dmax, node, k);
                }
            }
        }
    }

    /**
     * @brief Calculates a new value @f$ a @f$ of node @f$ node @f$ for a
     * specific boundary condition to be true.
     * @details By setting @f$ \vec u[node] = \vec a @f$ we satisfy the condition
     *     @f$ (\vec n \cdot \nabla) \vec u |_{node} = \vec v @f$. This is done
     *     using the following formula
     *   @f[ \vec a = \frac{ \vec v -
     *       \sum\limits_{i=1}^{\mathrm{support}}
     *           \left(\sum\limits_{d\in\{x, y, ...\}}
     *               n_d \ \ \mathrm{shape}_{\mathrm dd}[i]\right)
     *           \  \vec u[i]
     *    }{
     *        \sum\limits_{d\in \{x, y, ...\}} n_d \ \mathrm{shape}_{\mathrm dd}[0]
     *    } @f]
     *
     *     MLSM SUPPORT DOMAIN NOTICE:
     *     - Each node should be its first support node.
     *     - For symmetrical supports the central node may not affect the
     *       calculated derivation and therefore this function is unable to
     *       give a value and will crash.
     *
     * @tparam container A container type for the field. This will usually
     * be `Range` or `std::vector`.
     * @tparam field_t Type of field that is present in the domain. It's
     * type and dimensionality are generally independent of domain. This will be
     * `double` for scalar field or `Vec3d` for 3D vector field.
     *
     * @param u `field_t` field to be considered
     * @param node index of a node where the operators is evaluated
     * @param normal vector @f$ \vec n @f$ with the same dimensionality as the
     *             support domain giving the direction of the derivation
     * @param val `field_t` @f$ \vec v @f$ giving the value of the derivation
     *
     * @return A new value @f$ \vec a @f$ of type `field_t`
     *
     *  @throws assert Crashes if `node`'s first support node is not `node`
     *  @throws assert Crashes if `node`'s value has negligible effect on the
     *                 derivation, because no value could change the derivation
     */
    template<template<class> class container, class field_t>
    field_t neumann(const container<field_t>& u, int node, const vec_t& normal,
                    const field_t val) const {
        NODE_ASSERTS(node);
//        assert_msg(support_domain[node * ss.support_size(node)] == node,
//                   "First support node should be the node itself, node = $d, first support = %d.",
//                   node, support_domain[node * ss.support_size(node)]);
        field_t result = val;
        scalar_t divisor = 0, numerator = 0;
        for (int d = 0; d < dim; ++d) {
            for (int i = 1; i < ss.support_size(node); ++i) {
                result -= normal[d] * ss.d1(d, node, i) * u[ss.support(node, i)];
                numerator += std::abs(normal[d] * ss.d1(d, node, i));
            }
            // i = 0
            divisor += normal[d] * ss.d1(d, node, 0);
        }
        assert_msg(numerator < 1e10 * std::abs(divisor), "There was a problem getting the value "
                   "because the given node has a negligible effect on the given derivation.");
        return result / divisor;
    }

    /**
     * @brief Fill the matrix with the equation of first derivative of some component of
     * @f$\vec{u}@f$  along some axis at the point `node` being equal to some value.
     * @param M matrix representing the equations for all nodes. If `N` is the number of domain
     * nodes, then `M` is a `3N x 3N` matrix. Vector field `u` is considered to be represented as
     * @f$ u_j(node) = u[j*dim + node]@f$ or equivalently
     * @f$ \vec{u} = \begin{bmatrix} u_0 \\ u_1 \\ u_2 \end{bmatrix} @f$.
     * @param comp Which component to derive (0 based).
     * @param var With respect to which variable to derive (0 based).
     * @param node index of a node from 0 to `N` for which to write the equation for.
     * This means only `N*comp+node`-th row of the matrix is changed.
     * User must make sure that the matrix is large enough.
     * @param alpha scalar to multiply the operator with.
     * @param apply_to_comp the component to place the derivative at
     * This will place the derivative in 'N*apply_to_comp+node'-th row. Default value -1 will
     * set 'apply_to_comp = comp'.
     * @details Fills the rows of matrix `M` so that `N*comp+node` row
     * of the equation
     * @f[ M u = g @f]
     * is a good approximation of the differential equation
     * @f[ \alpha \frac{\partial u_{comp}}{\partial x_{var}}(node) = g(node) @f]
     * This is handy when solving equations implicitly.
     *
     * @note While 3 was used as the dimension of the vector field in the above description,
     * vector fields of arbitrary dimensions are supported. The given matrix must have dimensions
     * `dim*N x dim*N` and component and variable indices must be lower than `dim`.
     *
     * Example:
     * @snippet mlsm_operators_test.cpp Implicit derivative example
     */
    template<class matrix_t>
    void der1(matrix_t& M, int comp, int var, int node, scalar_t alpha,
              int apply_to_comp = -1) const {
        NODE_ASSERTS(node);
        assert_msg(0 <= comp && comp < dim, "Component %d out of bounds [%d, %d).", comp, 0, dim);
        assert_msg(comp*ss.size() + node < M.cols(), "Matrix does not have enough columns, "
                   "expected at least %d, got %d.", comp*ss.size() + node + 1, M.cols());
        assert_msg(comp*ss.size() + node < M.rows(), "Matrix does not have enough rows, "
                   "expected %d, got %d.", comp*ss.size() + node + 1, M.rows());
        if (apply_to_comp == -1) {
            apply_to_comp = comp;
        }
        assert_msg(0 <= apply_to_comp && apply_to_comp < dim,
                   "Component to apply %d out of bounds [%d, %d).", apply_to_comp, 0, dim);

        for (int k = 0; k < ss.support_size(node); ++k) {
            M.coeffRef(apply_to_comp*ss.size() + node,
                       comp*ss.size() + ss.support(node, k)) += alpha * ss.d1(var, node, k);
        }
    }

    /**
     * @brief Fill the matrix with the equation of second derivative of some component of
     * @f$\vec{u}@f$  along some pair of axes at the point `node` being equal to some value.
     * @param M matrix representing the equations for all nodes. If `N` is the number of domain
     * nodes, then `M` is a `3N x 3N` matrix. Vector field `u` is considered to be represented as
     * @f$ u_j(node) = u[j*dim + node]@f$ or equivalently
     * @f$ \vec{u} = \begin{bmatrix} u_0 \\ u_1 \\ u_2 \end{bmatrix} @f$.
     * @param comp Which component to derive (0 based).
     * @param var1 First variable with respect to which to derive (0 based).
     * @param var2 Second variable with respect to which to derive (0 based).
     * @param node index of a node from 0 to `N` for which to write the equation for.
     * This means only `N*comp+node`-th row of the matrix is changed.
     * User must make sure that the matrix is large enough.
     * @param alpha scalar to multiply the operator with.
     * @param apply_to_comp the component to place the derivative at
     * This will place the derivative in 'N*apply_to_comp+node'-th row. Default value -1 will
     * set 'apply_to_comp = comp'.
     * @details Fills the rows of matrix `M` so that `N*comp+node` row
     * of the equation
     * @f[ M u = g @f]
     * is a good approximation of the differential equation
     * @f[ \alpha \frac{\partial^2 u_{comp}}{\partial x_{var_1} x_{var_2}}(node) = g(node) @f]
     * This is handy when solving equations implicitly.
     *
     * @note While 3 was used as the dimension of the vector field in the above description,
     * vector fields of arbitrary dimensions are supported. The given matrix must have dimensions
     * `dim*N x dim*N` and component and variable indices must be lower than `dim`.
     */
    template<class matrix_t>
    void der2(matrix_t& M, int comp, int var1, int var2, int node, scalar_t alpha,
              int apply_to_comp = -1) const {
        NODE_ASSERTS(node);
        assert_msg(0 <= comp && comp < dim, "Component %d out of bounds [%d, %d).", comp, 0, dim);
        assert_msg(comp*ss.size() + node < M.cols(), "Matrix does not have enough columns, "
                "expected at least %d, got %d.", comp*ss.size() + node + 1, M.cols());
        assert_msg(comp*ss.size() + node < M.rows(), "Matrix does not have enough rows, "
                "expected %d, got %d.", comp*ss.size() + node + 1, M.rows());
        if (apply_to_comp == -1) {
            apply_to_comp = comp;
        }
        assert_msg(0 <= apply_to_comp && apply_to_comp < dim,
                   "Component to apply %d out of bounds [%d, %d).", apply_to_comp, 0, dim);
        if (var1 > var2) { std::swap(var1, var2); }
        for (int k = 0; k < ss.support_size(node); ++k) {
            M.coeffRef(apply_to_comp*ss.size() + node,
                       comp*ss.size() + ss.support(node, k)) += alpha * ss.d2(var1, var2, node, k);
        }
    }

    /**
     * Fills the matrix `M` with an equation of traction in a node along a normal being equal to
     * some value.
     * @param M Matrix to write the equation into.
     * @param node Index of the node. Only `node`, `node+N`, ..., `node+dim*N`-th rows are changed.
     * @param lam 1st Lame parameter.
     * @param mu 2nd Lame parameter.
     * @param normal Vector along which to compute the traction.
     * @param alpha Factor to multiply the equation with default 1.
     * @param comp At which component of the matrix to start writing.
     */
    template <typename matrix_t>
    void traction(matrix_t& M, int node, scalar_t lam, scalar_t mu, const vec_t& normal,
                  scalar_t alpha = 1.0, int comp = 0) {
        if (dim == 1) {
            assert_msg(false, "Not available for 1D.");
        } else if (dim == 2) {
            scalar_t nx = normal[0], ny = normal[1];
            // tx
            der1(M, 0, 1, node, alpha*mu*ny, comp+0);             // mu*ny*u_y +
            der1(M, 1, 1, node, alpha*lam*nx, comp+0);            // lam*nx*v_y +
            der1(M, 0, 0, node, alpha*(2.*mu+lam)*nx, comp+0);    // (2*mu+lam)*nx*u_x +
            der1(M, 1, 0, node, alpha*mu*ny, comp+0);             // mu*ny*v_x
            // ty
            der1(M, 0, 1, node, alpha*mu*nx, comp+1);             // mu*nx*u_y +
            der1(M, 1, 1, node, alpha*(2.*mu+lam)*ny, comp+1);    // (2*mu+lam)*ny*v_y +
            der1(M, 0, 0, node, alpha*lam*ny, comp+1);            // lam*ny*u_x +
            der1(M, 1, 0, node, alpha*mu*nx, comp+1);             // mu*nx*v_x
        } else if (dim == 3) {
            scalar_t nx = normal[0], ny = normal[1], nz = normal[2];
            der1(M, 0, 2, node, alpha*mu*nz, 0+comp);
            der1(M, 2, 2, node, alpha*lam*nx, 0+comp);
            der1(M, 0, 1, node, alpha*mu*ny, 0+comp);
            der1(M, 1, 1, node, alpha*lam*nx, 0+comp);
            der1(M, 0, 0, node, alpha*lam*nx, 0+comp);
            der1(M, 0, 0, node, 2*alpha*mu*nx, 0+comp);
            der1(M, 1, 0, node, alpha*mu*ny, 0+comp);
            der1(M, 2, 0, node, alpha*mu*nz, 0+comp);
            der1(M, 1, 2, node, alpha*mu*nz, 1+comp);
            der1(M, 2, 2, node, alpha*lam*ny, 1+comp);
            der1(M, 0, 1, node, alpha*mu*nx, 1+comp);
            der1(M, 1, 1, node, alpha*lam*ny, 1+comp);
            der1(M, 1, 1, node, 2*alpha*mu*ny, 1+comp);
            der1(M, 2, 1, node, alpha*mu*nz, 1+comp);
            der1(M, 0, 0, node, alpha*lam*ny, 1+comp);
            der1(M, 1, 0, node, alpha*mu*nx, 1+comp);
            der1(M, 0, 2, node, alpha*mu*nx, 2+comp);
            der1(M, 1, 2, node, alpha*mu*ny, 2+comp);
            der1(M, 2, 2, node, alpha*lam*nz, 2+comp);
            der1(M, 2, 2, node, 2*alpha*mu*nz, 2+comp);
            der1(M, 1, 1, node, alpha*lam*nz, 2+comp);
            der1(M, 2, 1, node, alpha*mu*ny, 2+comp);
            der1(M, 0, 0, node, alpha*lam*nz, 2+comp);
            der1(M, 2, 0, node, alpha*mu*nx, 2+comp);
        } else {
            assert_msg(false, "Not available for 1D.");
        }
    }

    template <typename T>
    friend std::ostream& operator<<(std::ostream& os, const MLSM<T>& op);
};

/// @cond
template <typename shape_storage_t>
const int MLSM<shape_storage_t>::dim;
/// @endcond

/// Output basic data about MLSM.
template <typename T>
std::ostream& operator<<(std::ostream& os, const MLSM<T>& op) {
    return os << "MLSM Operators with shape storage:\n" << op.ss;
}

/**
 * @brief make_mlsm returns mlsm operators
 * @param domain reference to the domain engine
 * @param approx reference to the approximation engine
 * @param ind range of nodes, in terms of indices, where the operators are prepared. If empty,
 * operators are prepared in all nodes, which is the default.
 * @param use_const_shape defines either to use constant shapes or to use dynamic scaling
 * read more in MLSM constructor documentation
 * @tparam mask bitmask indicating which flags to create
 * @tparam approx_engine type of approximation engine
 * @tparam domain_engine type of domain engine
 * @warning template parameters have a different order than in class MLSM, to support
 * calls in form `make_mlsm<mlsm::lap>(domain, engine, range)`.
 * @return mlms object with operators
 * @sa mlsm
 * @sa ShapeStorage
 */
template <mlsm::shape_flags mask = mlsm::all, typename domain_engine, typename approx_engine>
MLSM<UniformShapeStorage<domain_engine, approx_engine, mask>> make_mlsm(
        const domain_engine& domain, approx_engine& approx, Range<int> ind = {},
        bool use_const_shape = true) {
    if (ind.empty()) ind = domain.types != 0;
    auto shapes = UniformShapeStorage<domain_engine, approx_engine, mask>(
            domain, approx, ind, use_const_shape);
    return MLSM<UniformShapeStorage<domain_engine, approx_engine, mask>>(shapes);
}

}  // namespace mm

#endif  // SRC_MLSM_OPERATORS_HPP_
