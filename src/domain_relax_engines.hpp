#ifndef SRC_DOMAIN_RELAX_ENGINES_HPP_
#define SRC_DOMAIN_RELAX_ENGINES_HPP_

/**
 * @file
 * @brief Header file implementing relax engines.
 * @example domain_relax_engines_test.cpp
 */

#include "common.hpp"
#include "types.hpp"
#include "kdtree.hpp"
#include "kdtree_mutable.hpp"
#include "domain.hpp"

namespace mm {

/**
 * Redistributes nodes towards more uniform distribution by minimizing potential between nodes.
 * The engine first finds `num_neighbours` closest neighbours. Then it translates each point according to the
 * repelling "force" induced by its neighbours, where it also takes into account target potential.
 * The force is determined by summing the gradients of potentials in surrounding nodes, where amplitude
 * is determined based on distance to the closest neighbour and supplied target density function.
 * The algorithm uses also a primitive Simulated Annealing, i.e. the relax movement magnitude is multiplied with an
 * annealing factor that is linearly dropping from initial_heat to final_heat.
 *
 * The Engine supports two call types:
 * with supplied distribution function relax(func), where it tries to satisfy the user supplied nodal density function.
 * This can be achieved only when there is the total number of domain nodes the same as integral of density function
 * over the domain. If there is too much nodes a volatile relax might occur. If there is not enough nodes the relax
 * might become lazy. The best use of this mode is in combination with fillDistribution Engines, check test for
 * examples.
 *
 * Without distribution, where nodes always move towards less populated area regardless anything. The relax
 * magnitude is simply determined from annealing factor and distance to the closest node. A simple and stable
 * approach, however, note that this relax always converges towards uniformly distributed nodes.
 */
class BasicRelax {
  public:
    /// Indicating type of projection used when a relax node goes out of the domain.
    enum ProjectionType {
        /**
         * Escaped nodes are frozen outside the domain till the end of relax and
         * then removed. This is good option for violent and unstable relaxations.
         */
        DO_NOT_PROJECT,
        /**
         * Project on boundary in relax movement direction. In general produces better
         * distribution on boundaries, but the approximated normals can be bad.
         */
        PROJECT_IN_DIRECTION,
        /**
         * Project between two closest boundary nodes – this one might result in divergent
         * behaviour, when both closest nodes are on one side, leading in huge gaps on the
         * boundary. However, when there are enough boundary nodes in the first place, this
         * works very well.
         */
        PROJECT_BETWEEN_CLOSEST
    };

  private:
    int num_neighbours = 5;  ///< Number of nodes to consider when calculating the potential.
    int num_iterations = 50;  ///< Number of iterations performed.
    double initial_heat = 1;  ///< Initial heat, usually between 0 and 5.
    double final_heat = 0;  ///< Heat at the end of the relax, usually around 0.
    int potential_order = 2;  ///< Order of repulsing potential.
    int rebuild_tree_after = 1;  ///< How often engine rebuild search tree, 1 is perfect but slow.
    Range<int> nodes_;  ///< List of nodes to process.
    ProjectionType projection_type = DO_NOT_PROJECT;  ///< On boundary projection method.
    double boundary_projection_threshold = 0.75;  ///< Threshold for projecting nodes on boundary.

  public:
    BasicRelax();

    /// Move only given nodes.
    BasicRelax& onlyNodes(Range<int> nodes);

    /**
     * Sets initial heat.
     * @param in Initial heat, usually between 0 and 5, higher heat
     * means more volatile movement. High initial heat may cause divergence and erratic behaviour.
     * Setting too small initial heat might results in lazy relaxation.
     */
    BasicRelax& initialHeat(double in);

    /// Sets final heat.
    BasicRelax& finalHeat(double in);

    /// Sets num neighbours.
    BasicRelax& numNeighbours(int in);

    /// Sets order of repulsing potential
    BasicRelax& potentialOrder(int in);

    /// Sets number of iterations.
    BasicRelax& iterations(int in);

    /**
     * Sets rebuild tree frequency. Ir rebuild's tree every `in` iterations.
     * `in = 1` is perfect but slow. Using higher values results in better
     * performance at the cost of accuracy.
     */
    BasicRelax& rebuildTreeAfter(int in);

    /// Determines how to handle nodes that escape during relaxation.
    BasicRelax& projectionType(ProjectionType in);

    /**
     * Sets threshold for adding nodes on boundary, i.e. if node @f$d_1@f$ and @f$d_2@f$
     * are distances to closest boundary nodes, do not add node on boundary if
     * @f$d_1/d_2 < boundary_projection_threshold @f$.
     * If threshold is 0 all nodes are added, if it is greater than 1 no nodes are added.
     */
    BasicRelax& boundaryProjectionThreshold(double in);

    /**
     * Runs the relax on the selected domain with constant distribution equals
     * to domain characteristic distance
     * @param domain domain to process
     */
    template<class domain_t>
    void operator()(domain_t& domain) const {
        typedef typename domain_t::vector_t vec_t;
        operator()(domain, [](const vec_t& /* p */) { return -1.0; });
    }

    /**
     * Runs the relax on the selected domain with constant density
     * @param domain domain to process
     * @param r constant density
    */
    template<class domain_t>
    void operator()(domain_t& domain, double r) const {
        typedef typename domain_t::vector_t vec_t;
        operator()(domain, [r](const vec_t& /* p */) { return r; });
    }

    /// Runs the procedure of a given domain.
    template<class domain_t, class radius_func_type>
    void operator()(domain_t& domain, const radius_func_type& r_func) const {
        #if 0  // debug
        std::ofstream debug_out;
        debug_out.open("../../examples/domain_generation_plot/data/RELAX.m",
                       std::ofstream::out);
        #endif
        typedef typename domain_t::vector_t vec_t;
        typedef typename domain_t::scalar_t scalar_t;
        const int dim = domain_t::dim;

        // if no nodes are supplied, all interior nodes are processed
        Range<int> nodes = (nodes_.empty()) ? static_cast<Range<int>>(domain.types > 0) : nodes_;

        int N = domain.positions.size();
        assert_msg(N > num_neighbours, "Cannot relax a domain with less that num_neighbours nodes, "
                "num_neighbours = %d, N = %d.", num_neighbours, N);
        assert_msg(num_neighbours >= 1, "At least two neighbours are required, got %d.",
                   num_neighbours);
        for (int i : nodes) {
            assert_msg(0 <= i && i < N, "Node index %d out of range [0, %d) in relax.", i, N);
        }

        Range<int> bnd = domain.types < 0;
        assert_msg(bnd.size() > 0, "Relax requires boundary nodes, but this domain has none!");
        KDTreeMutable<vec_t> boundary_tree(domain.positions[bnd]);
        KDTree<vec_t> tree;
        Range<int> indices;

        Range<int> to_delete = {};  // nodes to be deleted from domain after relax
        bool removed_nodes_warning = false;

        for (int c = 0; c < num_iterations; ++c) {
            if (c == 0 || c % rebuild_tree_after == 0) {
                tree.resetTree(domain.positions);
            }
            // nodes to be removed from relax procedure
            Range<int> to_remove = {};
            // Annealing factor -- depends only on iteration step
            double a_f = ((initial_heat - final_heat) * (num_iterations - c) / num_iterations
                          + final_heat);
            // Range of all displacements
            Range<vec_t> dp(nodes.size(), vec_t(0.0));
            int k;
//            #if defined(_OPENMP)
//            #pragma omp parallel for private(k) schedule(static)
            // before enabling openmp vectors to_delete and to_remove
            // have to be introduced for each thread and combined at the end of the loop
            // otherwise expect seg. fault
//            #endif
            for (k = 0; k < nodes.size(); ++k) {
                int i = nodes[k];
                assert_msg(domain.types[i] > 0, "Only interior nodes are to be relaxed");
                vec_t pos = domain.positions[i];
                indices = tree.query(pos, num_neighbours + 1).first;
                double d_0 = std::pow((domain.positions[indices[1]] - pos).norm(), 1.0 / dim);

                // loop over support and compute potential contributions
                scalar_t r_0 = 0;
                for (int j = 1; j < indices.size(); ++j) {
                    // central node to support nodes vector
                    vec_t r = domain.positions[indices[j]] - pos;
                    // potential gradient
                    // target density in terms of radius to the closest node
                    r_0 = r_func(domain.positions[indices[j]]);
                    dp[k] += std::pow(std::abs(r_0) / r.norm(), potential_order) * r;
                }
                // no-distribution mode
                if (r_0 < 0) dp[k] = 0.2 * dp[k].normalized() * a_f * d_0;
                dp[k] = -a_f * d_0 * dp[k];
                // Project escaped nodes on the boundary
                if (!domain.contains(pos + dp[k])) {
                    vec_t tmp, normal;  // projection temp variables
                    bool project = true;
                    switch (projection_type) {
                        case DO_NOT_PROJECT:  // mark escaped nodes for deletion
                            to_remove.push_back(k);
                            to_delete.push_back(nodes[k]);
                            project = false;
                            break;
                        case PROJECT_IN_DIRECTION:  // project on boundary in direction of relax
                            tmp = 0.5 * (2 * pos + dp[k]);
                            normal = dp[k].normalized();
                            break;
                        case PROJECT_BETWEEN_CLOSEST:  // project between closest nodes on boundary
                            Range<int> idx = boundary_tree.query(pos, 2).first;
                            int s = bnd[idx[0]], t = bnd[idx[1]];
                            tmp = 0.5 * (domain.positions[s] + domain.positions[t]);
                            normal = (0.5 * (domain.normal(s) + domain.normal(t))).normalized();
                    }  // projection method selection end
                    if (project) {
                        // performs actual projection
                        bool success;
                        vec_t proj;
                        std::tie(success, proj) = domain.projectPointToBoundary(tmp, normal);
                        if (success) {
                            // check if projected node is not too close to boundary node
                            Range<double> dist;
                            Range<int> ind;
                            std::tie(ind, dist) = boundary_tree.query(proj, 2);
                            if (std::sqrt(dist[0] / dist[1]) < boundary_projection_threshold) {
                                dp[k] = -dp[k];
                                continue;
                            }
                            double d_1 = std::sqrt(dist[0]);
                            double d_2 = std::sqrt(dist[1]);
                            double d_0 = std::sqrt(dist[0] + dist[1]);

                            normal = ((1 - d_1 / d_0) * domain.normal(bnd[ind[0]])
                                      + (1 - d_2 / d_0) * domain.normal(bnd[ind[1]])).normalized();
                            int boundary_type = domain.types[bnd[ind[0]]];
                            domain.changeToBoundary(i, proj, boundary_type, normal);
                            boundary_tree.insert(proj);
                            bnd.push_back(i);
                            // mark projected node to be removed from the list for relax
                            to_remove.push_back(k);
                        } else {
                            removed_nodes_warning = true;
                            // if projection failed, remove node
                            to_remove.push_back(k);
                            to_delete.push_back(nodes[k]);
                        }
                    }  // project if end
                }  // escaped nodes if end
            }  // spatial loop end
            nodes[to_remove].remove();
            assert_msg(nodes.size() > num_neighbours + 1,
                       "No nodes in relax pool anymore, perhaps use lower heat");
            // apply displacement
            // #if defined(_OPENMP)
            //    #pragma omp parallel for private(k) schedule(static)
            // #endif
            for (k = 0; k < nodes.size(); ++k) {
                domain.positions[nodes[k]] += dp[k];
            }
            // DEBUG OUT -- for plotting
            // debug_out << "nodes(" << c + 1 << ",:,:)=" << domain.positions << ";\n";
        }
        // remove nodes at the end of relax to avoid mess with indices
        domain.clearNodes(to_delete);
        #if 0  // debug
        debug_out << "nodes_final(:,:)=" << domain.positions << ";\n";
        debug_out << "normal =" << domain.normals() << ";\n";
        Range<int> boundary = domain.types < 0;
        debug_out << "boundary =" << boundary << ";\n";
        debug_out << "boundary_map =" << domain.boundary_map() << ";\n";
        debug_out.close();
        #endif
        if (removed_nodes_warning) {
            print_red("warning -- nodes removed during relax due to projection fail\n");
        }
    }
};  // class BasicRelax

/**
 * Object representing a relax of fill function that is the same as current distribution
 * function in this domain.
 */
template <class vec_t>
class RelaxFunction {
    int num_closest;  ///< Number of closest points to include in calculations.
    KDTree<vec_t> tree;   ///< Tree of all points.
  public:
    /**
     * Creates a relax function that has approximately the same values of `dr` as given nodes.
     * @param pos Positions of given nodes.
     * @param num_closest How many neighbours to take into account when calculating `dr`.
     */
    RelaxFunction(const Range<vec_t>& pos, int num_closest) :
            num_closest(num_closest), tree(pos) {}

    /// Evaluate the distribution function.
    double operator()(const vec_t& point) const {
        Range<vec_t> pts = tree.get(tree.query(point, num_closest).first);
        double avg = 0.0;
        for (int i = 0; i < num_closest; ++i) {
            avg += std::sqrt(tree.query(pts[i], 2).second[1]);
        }
        return avg / num_closest;
    }
};

/**
 * Scattered node interpolant averaging over small neighbourhood.
 */
template <class vec_t, class value_t>
class ScatteredInterpolant {
    KDTree<vec_t> tree;  ///< Tree of all points.
    Range<value_t> values;  ///< Function values at given points.
    int num_closest;  ///< Number of closest points to include in calculations.
  public:
    /**
     * Creates a relax function that has approximately the same values of `dr` as given nodes.
     * @param pos Positions of given nodes.
     * @param values Function values at given positions.
     * @param num_closest How many neighbours to take into account when calculating function value.
     */
    ScatteredInterpolant(const Range<vec_t>& pos, const Range<value_t>& values, int num_closest)
            : tree(pos), values(values), num_closest(num_closest) {}

    /// Evaluate the distribution function.
    value_t operator()(const vec_t& point) const {
        Range<int> idx = tree.query(point, num_closest).first;
        value_t avg = value_t(0);
        for (int i : idx) {
            avg += values[i];
        }
        return avg / num_closest;
    }
};


/**
 * Scattered node interpolant averaging over small neighbourhood.
 * Implements modified Sheppard's method.
 */
template <class vec_t, class value_t>
class ModifiedSheppardsScatteredInterpolant {
    KDTree<vec_t> tree;  ///< Tree of all points.
    Range<value_t> values;  ///< Function values at given points.
    int num_closest;  ///< Number of closest points to include in calculations.
    typedef typename vec_t::scalar_t scalar_t;  ///< Scalar data type.
  public:
    /**
     * Creates a relax function that has approximately the same values of `dr` as given nodes.
     * @param pos Positions of given nodes.
     * @param values Function values at given positions.
     * @param num_closest How many neighbours to take into account when calculating function value.
     */
    ModifiedSheppardsScatteredInterpolant(const Range<vec_t>& pos, const Range<value_t>& values,
            int num_closest) : tree(pos), values(values), num_closest(num_closest) {}

    /// Evaluate the distribution function.
    value_t operator()(const vec_t& point) const {
        Range<int> idx;
        Range<scalar_t> dists2;
        std::tie(idx, dists2) = tree.query(point, num_closest);
        if (dists2[0] < 1e-6) return values[idx[0]];
        value_t avg = value_t(0);
        scalar_t weight_sum = scalar_t(0);
        scalar_t R = std::sqrt(dists2.back());
        for (int i = 0; i < idx.size(); ++i) {
            scalar_t rel_dist = (1 - std::sqrt(dists2[i]) / R);
            scalar_t weight = rel_dist*rel_dist / dists2[i];
            avg += weight*values[idx[i]];
            weight_sum += weight;
        }
        return avg / weight_sum;
    }
};

}  // namespace mm

#endif  // SRC_DOMAIN_RELAX_ENGINES_HPP_
