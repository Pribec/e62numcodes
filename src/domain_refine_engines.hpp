#ifndef SRC_DOMAIN_REFINE_ENGINES_HPP_
#define SRC_DOMAIN_REFINE_ENGINES_HPP_

/**
 * @file
 * @brief Implementations of different refinement strategies.
 * @example domain_refine_engines_test.cpp
 */

#include "common.hpp"
#include "domain_support_engines.hpp"
#include "domain_extended.hpp"
#include "kdtree.hpp"

namespace mm {

class HalfLinksSmoothRefine;

/**
 * Refine a region of nodes `region` by connecting every node in `region` to its support
 * domain and generating new nodes at half distances. The new nodes are filtered to meet a
 * minimum distance criterion to prevent points that would be too close.
 */
class HalfLinksRefine {
    Range<int> region_;  ///< Node indexes around which to refine.
    double fraction_;  ///< Minimal distance fraction.

  public:
    /// Construct a half links refine engine with given minimal fraction.
    explicit HalfLinksRefine(double fraction = -1);

  public:
    /// Set region to refine.
    HalfLinksRefine& region(Range<int> region);
    /**
     * Minimal distance criterion around point `p` is that nodes generated at `p` must be at least
     * `fraction * closest_support_node` away from all nodes. Fraction must be less than `1/2`.
     */
    HalfLinksRefine& min_dist(double fraction);

    /**
     * Refines given domain.
     * @return The indexes of the added nodes in `positions`.
     */
    template <class domain_t>
    Range<int> operator()(domain_t& domain) {
        typedef typename domain_t::vector_t vec_t;
        Range<int> region = region_;
        if (region.empty()) { region = domain.types != 0; }
        double fraction = fraction_;
        if (fraction == -1) { fraction = 0.4; }

        int N = domain.positions.size();

        KDTree<vec_t> domain_tree(domain.positions);
        // sort: boundary nodes first
        std::sort(region.begin(), region.end(),
                  [&](int i, int j) { return domain.types[i] < domain.types[j]; });

        KDTree<vec_t> region_tree(domain.positions[region]);

        // Remember which nodes added which points.
        Range<Range<int>> children(region.size());

        // Iterate through points in region and generate new points
        double max_support_radius = 0;
        for (int i = 0; i < N; ++i) {
            if (!domain.distances[i].empty() && domain.distances[i].back() > max_support_radius) {
                max_support_radius = domain.distances[i].back();
            }
        }
        assert_msg(max_support_radius > 0, "Max support radius should be positive, got 0. Did you"
                " forget to find support before refining?");
        max_support_radius = std::sqrt(max_support_radius);
        return refine_impl(domain, region, 0, fraction, domain_tree, region_tree,
                           children, max_support_radius);
    }

  private:
    /**
     * Refine implementation with more control over fine grained details.
     * @param domain Domain to refine.
     * @param region A list of indices to refine.
     * @param region_idx Index in `region` specifying that only nodes `region[region_idx]`,
     * `region[region_idx+1]`, ... are refined.
     * @param fraction Minimal distance fraction.
     * @param domain_tree Tree containing all domain points.
     * @param region_tree Tree containing all region points.
     * @param children A list of indexes of added nodes for each point in region.
     * @param max_support_radius Maximal radius of any support in the domain.
     * @return List of indices of added nodes.
     */
    template <class domain_t, class kdtree_t>
    static Range<int> refine_impl(
            domain_t& domain, const Range<int>& region, int region_idx, double fraction,
            const kdtree_t& domain_tree, const kdtree_t& region_tree, Range<Range<int>>& children,
            double max_support_radius) {
        int region_size = region.size();
        int N = domain.positions.size();
        typedef typename domain_t::vector_t vec_t;
        typedef typename domain_t::scalar_t scalar_t;

        assert_msg(region_size > 0, "The region to refine is empty.");

        assert_msg(domain.support.size() == N, "Support vector size does not match domain size "
                "(%d vs %d). Did you build support before calling refine?",
                   domain.support.size(), N);

        // Iterate through points in region and generate new points
        int num_new_points = 0;
        for (int i = region_idx; i < region_size; i++) {
            int c = region[i];  // the global domain index
            const vec_t pos = domain.positions[c];
            const Range<int> supp = domain.support[c];
            assert_msg(supp.size() >= 2, "At least 2 nodes must be in support of every node, %d "
                    "found in support of node %d.", supp.size(), c);
            double closest = std::sqrt(domain.distances[c][1]);
            scalar_t min_dist = fraction * closest;

            // then find all the nodes in the radius that admit collisions of children
            double critical_radius = max_support_radius + min_dist;
            Range<int> critical_idx = region_tree.query(pos, critical_radius * critical_radius,
                                                        region_size).first;
            int n = supp.size();
            // half links to my support
            for (int j = 1; j < n; ++j) {
                int s = supp[j];
                vec_t candidate = 0.5 * (domain.positions[c] + domain.positions[s]);

                // decide the type of new node and project to boundary if necessary
                int candidate_type = std::max(domain.types[c], domain.types[s]);
                vec_t normal_vector;
                if (candidate_type < 0) {
                    normal_vector = domain.normal(c) + domain.normal(s);
                    if (normal_vector.squaredNorm() < 1e-15) {
                        // normal_vector of given points point in opposite directions.
                        continue;
                    }
                    normal_vector.normalize();
                    auto result = domain.projectPointToBoundary(candidate, normal_vector, EPS);
                    if (result.first) {
                        candidate = result.second;
                    } else {
                        std::cerr << format("Adding point %s with type %d to the boundary along "
                                                    "normal %s was not successful.",
                                            candidate, candidate_type, normal_vector)
                                  << std::endl;
                        continue;
                    }
                }

                // If nodes are out of the domain, they should be thrown away.
                if (!domain.contains(candidate)) continue;

                // Check that it is not too close to old nodes
                double closest_old_node_dist_squared = domain_tree.query(candidate).second[0];
                bool too_close = closest_old_node_dist_squared < min_dist * min_dist;

                // Check possible collision with new nodes
                // Condition: dist(me, an old node's child) < min_dist
                // Because children are half-links with support, it can only happen if
                // d(p, q) <= max_support_size + min_dist
                if (!too_close) {
                    for (int k : critical_idx) {  // check children of k
                        for (int l : children[k]) {
                            if ((domain.positions[l] - candidate).squaredNorm() <
                                min_dist * min_dist) {
                                too_close = true;
                                break;
                            }
                        }
                        if (too_close) break;
                    }
                }

                if (!too_close) {  // add new point
                    children[i].push_back(
                            N + num_new_points++);  // increase new point counter
                    if (candidate_type < 0) {
                        domain.addBoundaryPoint(candidate, candidate_type, normal_vector);
                    } else {
                        domain.addPoint(candidate, candidate_type);
                    }
                }
            }
        }

        // return back indices of new nodes
        Range<int> new_ids(num_new_points);
        std::iota(new_ids.begin(), new_ids.end(), N);

        return new_ids;
    }

    friend HalfLinksSmoothRefine;   ///< Let smooth version see my private parts.
};

/**
 * Refines a region using half link refinement but takes care that there are no sharp jumps in
 * node density.
 */
class HalfLinksSmoothRefine {
    Range<int> region_;  ///< Region to refine.
    double fraction_;   ///< Minimal distance fraction.
    double max_imbalance_;   ///< Maximal allowed imbalance.

  public:
    /// Construct a amooth half link refine engine.
    explicit HalfLinksSmoothRefine(double fraction = -1, double max_imbalance = -1);
    /// Set region to refine.
    HalfLinksSmoothRefine& region(Range<int> region);
    /**
     * Minimal distance criterion around point `p` is that nodes generated at `p` must be at least
     * `fraction * closest_support_node` away from all nodes. Fraction must be less than `1/2`.
     */
    HalfLinksSmoothRefine& min_dist(double fraction);
    /**
     * Maximal allowed imbalance factor, usually > 2. This is the ratio between closest and
     * farthest support node in a minimal balanced support.
     */
    HalfLinksSmoothRefine& max_imbalance(double max_imbalance);

    /**
     * Refines a given region, but also refines nodes that might achieve too large imbalance,
     * resulting in a nicer distribution of nodes.
     * @return Indexes of new nodes.
     */
    template <class domain_t>
    Range<int> operator()(domain_t& domain) {
        typedef typename domain_t::vector_t vec_t;
        Range<int> region = region_;
        if (region.empty()) { region = domain.types != 0; }
        double fraction = fraction_;
        if (fraction == -1) { fraction = 0.4; }
        double max_imbalance = max_imbalance_;
        if (max_imbalance == -1) { max_imbalance = 3.0; }

        int region_size = region.size();
        int N = domain.positions.size();

        KDTree<vec_t> domain_tree(domain.positions);

        // sort: boundary nodes first
        std::sort(region.begin(), region.end(),
                  [&](int i, int j) { return domain.types[i] < domain.types[j]; });
        KDTree<vec_t> region_tree(domain.positions[region]);

        // Remember which nodes added which points.
        Range<Range<int>> children(region_size);

        // Iterate through points in region and generate new points
        double max_support_radius = 0;
        for (int i = 0; i < N; ++i) {
            if (!domain.distances[i].empty() && domain.distances[i].back() > max_support_radius) {
                max_support_radius = domain.distances[i].back();
            }
        }
        assert_msg(max_support_radius > 0, "Max support radius should be positive, got 0. Did you"
                " forget to find support before refining?");
        max_support_radius = std::sqrt(max_support_radius);

        Range<int> new_ids;
        Range<bool> in_region(N, false);
        for (int i : region) { in_region[i] = true; }
        int region_idx = 0;
        while (region.size() != region_idx) {
            auto ids = HalfLinksRefine::refine_impl(
                    domain, region, region_idx, fraction, domain_tree, region_tree, children,
                    max_support_radius);
            new_ids.append(ids);
            region_idx = region.size();

            KDTree<vec_t> all_pts(domain.positions);
            Range<int> all_idx = domain.types != 0;
            for (int i = 0; i < N; ++i) {
                double imbalance = computeImbalance(domain, all_pts, all_idx, i);
                if (imbalance >= max_imbalance && !in_region[i]) {
                    region.push_back(i);
                    in_region[i] = true;
                }
            }
            // sort: boundary nodes first
            std::sort(region.begin()+region_idx, region.end(),
                      [&](int k, int l) { return domain.types[k] < domain.types[l]; });
            children.resize(region.size());
            region_tree = KDTree<vec_t>(domain.positions[region]);
        }
        return new_ids;
    }


    /**
     * Computes imbalance for node `node` in a given domain by finding support among points in
     * `tree` constructed from points in `search_among`.
     */
    template <class domain_t>
    double computeImbalance(domain_t& domain, const KDTree<typename domain_t::vector_t>& tree,
                            const Range<int>& search_among, int node) {
        Range<typename domain_t::scalar_t> dists = FindBalancedSupport::balancedSupport(
                domain, tree, node, 4, tree.size(), search_among).second;
        return std::sqrt(dists.back() / dists[1]);
    }
};

}  // namespace mm

#endif  // SRC_DOMAIN_REFINE_ENGINES_HPP_
