add_library(ann ANN.cpp brute.cpp kd_tree.cpp kd_util.cpp kd_split.cpp
            kd_dump.cpp kd_search.cpp kd_pr_search.cpp kd_fix_rad_search.cpp
            bd_tree.cpp bd_search.cpp bd_pr_search.cpp bd_fix_rad_search.cpp
            perf.cpp)
target_include_directories(ann PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/../include")
# supress warnings for ann
target_compile_options(ann PRIVATE "-w")
