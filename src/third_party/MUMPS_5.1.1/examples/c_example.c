/*
 *
 *  This file is part of MUMPS 5.1.1, released
 *  on Tue Mar 21 14:55:59 UTC 2017
 *
 */
/* Example program using the C interface to the
 * double real arithmetic version of MUMPS, dmumps_c.
 * We solve the system A x = RHS with
 *   A = diag(1 2) and RHS = [1 4]^T
 * Solution is [1 2]^T */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mpi.h"
#include "dmumps_c.h"
#define JOB_INIT -1
#define JOB_END -2
#define USE_COMM_WORLD -987654
#define N 1000000

void prn(double arr[], int n) {
  for (int i = 0; i < n; i++) {
    printf("% 3.2f ", arr[i]);
  }
  printf("\n");
}
void prn_int(int arr[], int n) {
  for (int i = 0; i < n; i++) {
    printf("%5d ", arr[i]);
  }
  printf("\n");
}

#if defined(MAIN_COMP)
/*
 * Some Fortran compilers (COMPAQ fort) define main inside
 * their runtime library while a Fortran program translates
 * to MAIN_ or MAIN__ which is then called from "main". This
 * is annoying because MAIN__ has no arguments and we must
 * define argc/argv arbitrarily !!
 */
int MAIN__();
int MAIN_()
  {
    return MAIN__();
  }

int MAIN__()
{
  int argc=1;
  char * name = "c_example";
  char ** argv ;
#else
int main(int argc, char ** argv)
{
#endif
  DMUMPS_STRUC_C id;
  MUMPS_INT n = N;
  MUMPS_INT8 nnz = 3*N-4;
  MUMPS_INT *irn = (MUMPS_INT*) malloc((3*N-4) * sizeof(MUMPS_INT));
  MUMPS_INT *jcn = (MUMPS_INT*) malloc((3*N-4) * sizeof(MUMPS_INT));
  double *a = (double*) malloc((3*N-4) * sizeof(double));
  // MUMPS_INT irn[3*N-4];
  // MUMPS_INT jcn[3*N-4];
  // double a[3*N-4];
  double rhs[N];

  MUMPS_INT myid, ierr;

  int error = 0;
#if defined(MAIN_COMP)
  argv = &name;
#endif
  ierr = MPI_Init(&argc, &argv);
  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  /* Define A and rhs */

  if (N == 2) {
    irn[0] = 1; irn[1] = 2;
    jcn[0] = 1; jcn[1] = 2;
    rhs[0]=1.0;rhs[1]=4.0;
    a[0]=1.0;a[1]=2.0;
  } else {
    for (int i = 0; i < N; i++) {
      if (i == 0) {
        rhs[i] = 1;
        irn[0] = 1; jcn[0] = 1; a[0] = 1;
      } else if (i == N-1) {
        rhs[i] = 1;
        irn[3*N-5] = N; jcn[3*N-5] = N; a[3*N-5] = 1;
      } else {
        rhs[i] = 0;
        irn[3*i-2] = i+1; jcn[3*i-2] = i; a[3*i-2] = 1;
        irn[3*i-1] = i+1; jcn[3*i-1] = i+1;   a[3*i-1] = -2;
        irn[3*i] = i+1;   jcn[3*i] = i+2;   a[3*i] = 1;
      }
    }
  }

  // prn_int(irn, 3*N-4);
  // prn_int(jcn, 3*N-4);
  // prn(a, 3*N-4);
  // prn(rhs, N);

  // rhs[0]=1.0;rhs[1]=4.0;
  // a[0]=1.0;a[1]=2.0;

  /* Initialize a MUMPS instance. Use MPI_COMM_WORLD */
  id.comm_fortran=USE_COMM_WORLD;
  id.par=1; id.sym=0;
  id.job=JOB_INIT;
  dmumps_c(&id);

  /* Define the problem on the host */
  if (myid == 0) {
    id.n = n; id.nnz =nnz; id.irn=irn; id.jcn=jcn;
    id.a = a; id.rhs = rhs;
  }
#define ICNTL(I) icntl[(I)-1] /* macro s.t. indices match documentation */
  /* No outputs */
  id.ICNTL(1)=-1; id.ICNTL(2)=-1; id.ICNTL(3)=-1; id.ICNTL(4)=0;

  /* Call the MUMPS package (analyse, factorization and solve). */
  id.job=6;
  dmumps_c(&id);
  if (id.infog[0]<0) {
    printf(" (PROC %d) ERROR RETURN: \tINFOG(1)= %d\n\t\t\t\tINFOG(2)= %d\n",
        myid, id.infog[0], id.infog[1]);
    error = 1;
  }

  /* Terminate instance. */
  id.job=JOB_END;
  dmumps_c(&id);
  if (myid == 0) {
    if (!error) {
      printf("Solution is : (%8.2f  %8.2f)\n", rhs[0],rhs[N/2]);
    } else {
      printf("An error has occured, please check error code returned by MUMPS.\n");
    }
  }
  ierr = MPI_Finalize();
  return 0;
}
