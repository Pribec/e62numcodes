#ifndef SRC_DOMAIN_FILL_ENGINES_HPP_
#define SRC_DOMAIN_FILL_ENGINES_HPP_

/**
 * @file
 * @brief Header file implementing fill engines.
 * @example domain_fill_engines_test.cpp
 */

#include "common.hpp"
#include "types.hpp"
#include "kdtree_mutable.hpp"
#include "domain.hpp"
#include <random>
#include <cmath>
#include <queue>
#include <functional>

namespace mm {

/**
 * Poisson Disk Sampling based fill algorithm. The algorithm starts in random node and adds
 * nodes around it on a circle with radius defined in a supplied density function.
 * In each next iteration it selects one of the previously added nodes and repeats the adding.
 * The main bottleneck right now is a search structure. First step in optimization
 * would be better tree bookkeeping, i.e. determine which nodes cannot be in a way anymore
 * this might become tricky with variable nodal densities.
 */
class PoissonDiskSamplingFill {
    int num_iterations = 5000000;  ///< max number of iterations performed
    size_t optimize_after = 50000;  ///< after how many nodes the mutable tree is optimized
    double proximity_relaxation = 0.95;  ///< threshold for proximity cutoff
    bool use_MC_in_iteration = true;  ///< use MC for selection of expanding nodes
    int rseed = -1;  ///<  User supplied seed if -1 use generate std::random_device
    int internal_t = NODE_TYPE::DEFAULT_INTERNAL;  ///< added nodes type
  public:
    PoissonDiskSamplingFill();

    /**
     * Sets maximal number of iterations.
     * @param iterations maximal allowed number of iteration before exiting
     */
    PoissonDiskSamplingFill& iterations(int iterations);

    /**
     * Sets random seed.
     * @param in random random
     */
    PoissonDiskSamplingFill& seed(int in);

    /**
     * Sets random seed.
     * @param in internal nodes type
     */
    PoissonDiskSamplingFill& internal_type(int in);
    /**
     * Sets proximity relaxation that defines minimal distance between nodes,
     * in other words, nodes closer then target_radius*proximity_relaxation
     * will be deleted.
     * @param in proximity_relaxation
     */
    PoissonDiskSamplingFill& proximity_relax(double in);

    /**
     * Use monte carlo in selection of the next expanded node. Using monte carlo
     * results in slower but better algorithm, especially in variable density situations.
     * @param in use MC if true
     */
    PoissonDiskSamplingFill& randomize(bool in);

    /**
     * Number of added nodes before the mutable kdtree is optimized
     * @param in after how many nodes the mutable tree is optimized
     */
    PoissonDiskSamplingFill& optim_after(size_t in);

    /**
     * Runs the PDSfill on the selected domain with constant density
     * @param domain domain to process
     * @param r constant density
     * @param initial_point Point at which growth begins, def random point 
    */
    template<class domain_t>
    void operator()(domain_t& domain, double r,
                    typename domain_t::vector_t initial_point
                    = std::numeric_limits<double>::quiet_NaN()) const {
        typedef typename domain_t::vector_t vec_t;
        operator()(domain, [r](const vec_t& /* p */) { return r; },
            initial_point);
    }

    /**
     * Runs the PDSfill on the selected domain with target density function
     * @param r_func target density function
     * @param domain domain to process
     * @param initial_point Point at which growth begins, def random point 
     */
    template<class domain_t, class radius_func_type>
    void operator()(domain_t& domain, const radius_func_type& r_func,
                    typename domain_t::vector_t initial_point
                    = std::numeric_limits<double>::quiet_NaN()) const {
        typedef typename domain_t::vector_t vec_t;
        typedef typename domain_t::scalar_t scalar_t;
        const int dim = domain_t::dim;

        vec_t lo_bound, hi_bound;
        std::tie(lo_bound, hi_bound) = domain.getBBox();

        Range<vec_t> new_nodes;

        // use MC to to find a random node within the domain regarding the BBox
        // and the domain shape
        vec_t random_node;  // temp. variable
        std::mt19937 gen;
        if (rseed == -1) {
            gen.seed(get_seed());
        } else {
            gen.seed(rseed);
        }
        if (std::isnan(initial_point[0])) {
            do {
                // generate random node in the domain
                for (int j = 0; j < dim; ++j) {
                    std::uniform_real_distribution<scalar_t> dis(lo_bound[j], hi_bound[j]);
                    random_node[j] = dis(gen);
                }
            } while (!domain.contains(random_node));
        } else {
            random_node = initial_point;
        }
        new_nodes.push_back(random_node);
        std::deque<size_t> to_expand;  // list of nodes to be expanded
        to_expand.push_back(0);  // adds first node to expand list

        // size_t current_parent = 0;  // temp variable
        // build tree with seed node only

        KDTreeMutable<vec_t> kd3(domain.positions);
        kd3.insert(random_node);
        size_t added_node_counter = 0;

        // pre-compute local distribution for new candidates
        // this vector also defines the candidate list
        Range<vec_t> pre_comp_dist;
        if (dim == 2) {
            int n = 6;  // no. of new nodes per iteration
            for (int j = 0; j < n; ++j) {
                double f = 2 * M_PI / n * j;
                pre_comp_dist.push_back(vec_t({std::cos(f), std::sin(f)}));
            }
        } else if (dim == 3) {
            int n = 13;  // 4*pi*r^2/r^2
            scalar_t offset = 2.0 / n;
            scalar_t increment = M_PI * (3.0 - std::sqrt(5));
            scalar_t x, y, z, r, phi;
            for (int i = 0; i < n; ++i) {
                y = ((i * offset) - 1) + (offset / 2);
                r = std::sqrt(1 - y * y);
                phi = i * increment;
                x = std::cos(phi) * r;
                z = std::sin(phi) * r;
                pre_comp_dist.push_back(vec_t({x, y, z}));
            }
        } else {
            assert_msg(false, "Only 2D and 3D Poisson Disk sampling supported");
        }

        bool alter = false;  // temp variable
        // iteration loop
        for (int c = 0; c < num_iterations; ++c) {
            // break criterion
            if (to_expand.empty()) break;
            // optimize tree after specific number of added nodes
            if (added_node_counter > optimize_after) {
                added_node_counter = 0;
                kd3.optimize();
            }
            size_t node_i = to_expand.front();
            to_expand.pop_front();
            // filter candidates regarding the domain and proximity criteria
            for (auto f : pre_comp_dist) {
                // target radius,
                scalar_t radius = r_func(new_nodes[node_i]);
                assert_msg(radius > 0, "Poisson Disk Sampling:: Target radius should be > 0");
                // compute candidate node from precomputed values
                vec_t node = new_nodes[node_i] + f * radius;

                if (!domain.contains(node)) continue;
                Range<double> d = kd3.query(node, 1).second;
                if (d[0] < radius * radius * proximity_relaxation * proximity_relaxation)
                    continue;

                new_nodes.push_back(node);
                // if we use MC add new to_expand index on a random position
                if (use_MC_in_iteration && !to_expand.empty()) {
                    std::uniform_int_distribution<> dis(0, to_expand.size() - 1);
                    to_expand.insert(to_expand.begin() + dis(gen), new_nodes.size() - 1);
                } else {
                    // cheap trick to increase randomness by alternating
                    // push front and push back.
                    if (alter) to_expand.push_back(new_nodes.size() - 1);
                    else to_expand.push_front(new_nodes.size() - 1);
                    alter = !alter;
                }
                kd3.insert(node);
                added_node_counter++;
            }
        }
        // Add nodes to the domain
        // all as internal nodes, let the relax take care of that
        for (const auto& node : new_nodes) domain.addPoint(node, internal_t);
    }
};

}  // namespace mm

#endif  // SRC_DOMAIN_FILL_ENGINES_HPP_
