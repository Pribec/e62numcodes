#include "io.hpp"
#include "common.hpp"

namespace mm {

/**
 * @file io.cpp
 * @brief Implementations of IO functions.
 **/

/*************** XML ***************/
XMLloader::XMLloader() = default;
XMLloader::XMLloader(const std::string& file) {
    doc.LoadFile(file.c_str());
    assert_msg(!doc.Error(), "Error opening file '%s': %s.", file, doc.ErrorName());
}
void XMLloader::operator()(const std::string& file) {
    doc.LoadFile(file.c_str());
    assert_msg(!doc.Error(), "Error opening file '%s': %s.", file, doc.ErrorName());
}

XMLloader::Proxy::operator double() const {
    return parent->getDoubleAttribute(path, att);
}
XMLloader::Proxy::operator float() const {
    return parent->getFloatAttribute(path, att);
}
XMLloader::Proxy::operator int() const { return parent->getIntAttribute(path, att); }
XMLloader::Proxy::operator size_t() const {
    return static_cast<size_t>(parent->getIntAttribute(path, att));
}
XMLloader::Proxy::operator bool() const { return parent->getBoolAttribute(path, att); }
XMLloader::Proxy::operator std::string() const { return parent->getStringAttribute(path, att); }

std::string XMLloader::textRead(const std::vector<std::string>& path) const {
    const tinyxml2::XMLNode* e = walk(path)->FirstChild();
    assert_msg(e != nullptr, "Could not find node at path '/%s'.", join(path, "/"));
    const tinyxml2::XMLText* eT = e->ToText();
    assert_msg(eT != nullptr, "Element at path '/%s' is cannot be cast to text.", join(path, "/"));
    return eT->Value();
}

XMLloader::Proxy XMLloader::getAttribute(const std::vector<std::string>& path,
                                         const std::string& att) const {
    return XMLloader::Proxy(this, path, att);
}

/// cout overload
std::ostream& operator<<(std::ostream& os, const XMLloader::Proxy&) {
    return os << "Use appropriate static_cast<> or assignment to typed variable to change "
            "into a value." << std::endl;
}

double XMLloader::getDoubleAttribute(const std::vector<std::string>& path,
                                     const std::string& att) const {
    const tinyxml2::XMLElement* e = walk(path);
    return e->DoubleAttribute(att.c_str());
}

float XMLloader::getFloatAttribute(const std::vector<std::string>& path,
                                   const std::string& att) const {
    const tinyxml2::XMLElement* e = walk(path);
    return e->FloatAttribute(att.c_str());
}

int XMLloader::getIntAttribute(const std::vector<std::string>& path, const std::string& att) const {
    const tinyxml2::XMLElement* e = walk(path);
    return e->IntAttribute(att.c_str());
}

bool XMLloader::getBoolAttribute(const std::vector<std::string>& path,
                                 const std::string& att) const {
    const tinyxml2::XMLElement* e = walk(path);
    return e->BoolAttribute(att.c_str());
}

std::string XMLloader::getStringAttribute(const std::vector<std::string>& path,
                                          const std::string& att) const {
    const tinyxml2::XMLElement* e = walk(path);
    const char* s = e->Attribute(att.c_str());
    assert_msg(s != nullptr, "Attribute '%s' not found on path '/%s'.", att, join(path, "/"));
    return s;
}

const tinyxml2::XMLElement* XMLloader::walk(const std::vector<std::string>& path) const {
    const tinyxml2::XMLElement* e = doc.FirstChildElement(path[0].c_str());
    for (size_t i = 1; i < path.size(); ++i) {
        assert_msg(e != nullptr, "Path '/%s' does not have a child '%s'.",
                   join(std::vector<std::string>(path.begin(), path.begin()+i-1), "/"), path[i-1]);
        e = e->FirstChildElement(path[i].c_str());
    }
    assert_msg(e != nullptr, "Path '/%s' does not have a child '%s'.",
               join(std::vector<std::string>(path.begin(), path.end()-1), "/"), path.back());
    return e;
}

std::vector<std::pair<std::string, std::string>> XMLloader::getKeyValuePairs() const {
    const tinyxml2::XMLNode* root = doc.FirstChildElement();
    std::vector<std::pair<std::string, std::string>> kv;
    for (const tinyxml2::XMLElement* e = root->FirstChildElement(); e != nullptr;
         e = e->NextSiblingElement()) {
        for (const tinyxml2::XMLAttribute* attr = e->FirstAttribute(); attr != nullptr;
             attr = attr->Next()) {
            kv.push_back({std::string(e->Name()) + "." + attr->Name(), attr->Value()});
        }
    }
    return kv;
}

#ifndef __MIC__  // Disable hdf5 support on MIC
/*************** HDF5 ***************/
H5::H5File HDF5IO::openFileHelper(const std::string& file_name, unsigned mode) {
    H5::H5File file;
    try {
        if (mode == HDF5IO::APPEND) {
            std::ifstream test_access(file_name);
            if (test_access.good()) {  // exists
                file = H5::H5File(file_name, H5F_ACC_RDWR);
            } else {
                file = H5::H5File(file_name, H5F_ACC_EXCL);
            }
        } else if (mode == HDF5IO::DESTROY) {
            file = H5::H5File(file_name, H5F_ACC_TRUNC);
        } else if (mode == HDF5IO::READONLY) {
            std::ifstream test_access(file_name);
            assert_msg(test_access.good(), "To use readonly access, file '%s' must exists "
                       "and be accessible.", file_name);
            file = H5::H5File(file_name, H5F_ACC_RDONLY);
        } else {
            file = H5::H5File(file_name, mode);
        }
    } catch (const H5::Exception& error) {
        assert_msg(false, "Error opening file '%s' with mode %d.\nDetails: %s", file_name, mode,
                   error.getDetailMsg());
    }
    return file;
}
HDF5IO::HDF5IO(int _exception_handling) : exception_handling(_exception_handling) {
    if (exception_handling == 0) H5::Exception::dontPrint();
}
HDF5IO::HDF5IO(const std::string& _file_name, unsigned mode, int _exception_handling) :
        file_name(_file_name), exception_handling(_exception_handling) {
    file = HDF5IO::openFileHelper(file_name, mode);
    if (exception_handling == 0) H5::Exception::dontPrint();
}

void HDF5IO::printError(const H5::Exception& error, const std::string& function) const {
    std::stringstream err;
    err << "Warning: Exception thrown in HDF5IO::" << function << "\n"
              << "\tError:\t\t" << error.getDetailMsg() << "\n"
              << "\tFile name:\t" << file_name << "\n"
              << "\tFolder name:\t" << folder_name << "\n";
    if (folder_name.empty()) err << "Did you open a folder before reading/writing?\n";
    if (exception_handling == 1) {
        std::cerr << err.str();
    } else if (exception_handling == 2) {
        throw err.str();
    } else if (exception_handling == 3) {
        std::cerr << err.str() << std::endl;
        assert_msg(false, "HDF5IO Exception thrown and `exception_handling` was set to assert.");
    }
}

void HDF5IO::openFile(const std::string& _file_name, int mode) {
    file_name = _file_name;
    file = openFileHelper(file_name, mode);
}

void HDF5IO::reopenFile(int mode) { openFile(getFileName(), mode); }

void HDF5IO::closeFile() {
    try {
        group.close();
        file.close();
    } catch (const H5::Exception& error) {
        printError(error, "closeFile");
    }
}

void HDF5IO::createFolder(const std::string& _folder_name) {
    file.createGroup(_folder_name);
}

void HDF5IO::openFolder(const std::string& _folder_name) {
    assert_msg(!_folder_name.empty(), "Folder name must not be empty.");
    assert_msg(_folder_name[0] == '/', "Folder name must start with '/', got '%s'.", _folder_name);
    assert_msg(_folder_name.size() == 1 || _folder_name.back() != '/',
               "Folder name must not end with a '/'unless it's root, got '%s'.", _folder_name);
    try {
        group.close();
        folder_name = _folder_name;

        H5E_auto2_t func;
        void* client_data;
        H5::Exception::getAutoPrint(func, &client_data);
        H5::Exception::dontPrint();
        // create all folders along the path if they do not exist
        std::string::size_type idx = 0;
        do {
            idx = folder_name.find('/', idx+1);
            if (idx == std::string::npos) idx = folder_name.size();
            try { file.createGroup(folder_name.substr(0, idx)); }
            catch (H5::FileIException not_found_err) {}
        } while (idx != folder_name.size());
        H5::Exception::setAutoPrint(func, client_data);
        // Created if it did not exist

        group = file.openGroup(folder_name);
    } catch (const H5::Exception& error) {
        printError(error, "openFolder");
    }
}

void HDF5IO::reopenFolder() { openFolder(getFolderName()); }

void HDF5IO::closeFolder() { group.close(); }

std::vector<std::string> HDF5IO::ls() {
    std::vector<std::string> result;
    if (group.getId() > 0) {
        size_t n = group.getNumObjs();
        result.reserve(n);
        for (size_t i = 0; i < n; i++) {
            result.push_back(group.getObjnameByIdx(i));
        }
    } else {
        size_t n = file.getNumObjs();
        result.reserve(n);
        for (size_t i = 0; i < n; i++) {
            result.push_back(file.getObjnameByIdx(i));
        }
    }
    return result;
}

bool HDF5IO::setStringAttribute(const std::string& name, const std::string& value) {
    try {
        H5::StrType vlst(0, H5T_VARIABLE);
        assert_msg(group.getId() > 0, "Open a folder before writing attrubute '%s'.", name);
        H5E_auto2_t func;
        void* client_data;
        H5::Exception::getAutoPrint(func, &client_data);
        H5::Exception::dontPrint();
        H5::Attribute attribute;
        try {
            attribute = group.openAttribute(name);
        } catch (H5::AttributeIException error) {
            attribute = group.createAttribute(name, vlst  , H5::DataSpace());
        }
        H5::Exception::setAutoPrint(func, client_data);
        attribute.write(vlst, value);
        attribute.close();
        return true;
    } catch (const H5::Exception& error) {
        printError(error, "setStringAttribute");
    }
    return false;
}

bool HDF5IO::setDoubleAttribute(const std::string& name, double value) {
    try {
        return setAttribute(name, value, H5::PredType::NATIVE_DOUBLE);
    } catch (const H5::Exception& error) {
        printError(error, "setStringAttribute");
    }
    return false;
}

bool HDF5IO::setFloatAttribute(const std::string& name, float value) {
    try {
        return setAttribute(name, value, H5::PredType::NATIVE_FLOAT);
    } catch (const H5::Exception& error) {
        printError(error, "setStringAttribute");
    }
    return false;
}

bool HDF5IO::setIntAttribute(const std::string& name, int value) {
    try {
        return setAttribute(name, value, H5::PredType::NATIVE_INT);
    } catch (const H5::Exception& error) {
        printError(error, "setStringAttribute");
    }
    return false;
}

std::string HDF5IO::getStringAttribute(const std::string& name) const {
    try {
        assert_msg(group.getId() > 0, "Open a folder before reading attrubute '%s'.", name);
        std::string result;
        auto attribute = group.openAttribute(name);
        attribute.read(attribute.getDataType(), result);
        attribute.close();
        return result;
    } catch (const H5::Exception& error) {
        printError(error, "getStringAttribute");
    }
    return HDF5IO_STRING_ERR;
}

int HDF5IO::getIntAttribute(const std::string& name) const {
    try {
        return getAttribute<int>(name);
    } catch (const H5::Exception& error) {
        printError(error, "getIntAttribute");
    }
    return HDF5IO_INT_ERR;
}

float HDF5IO::getFloatAttribute(const std::string& name) const {
    try {
        return getAttribute<float>(name);
    } catch (const H5::Exception& error) {
        printError(error, "getFloatAttribute");
    }
    return HDF5IO_FLOAT_ERR;
}

double HDF5IO::getDoubleAttribute(const std::string& name) const {
    try {
        return getAttribute<double>(name);
    } catch (const H5::Exception& error) {
        printError(error, "getDoubleAttribute");
    }
    return HDF5IO_DOUBLE_ERR;
}

std::vector<std::vector<unsigned char>> HDF5IO::getBScope(const std::string& name) const {
    std::vector<std::vector<unsigned char>> result;
    try {
        assert_msg(group.getId() > 0, "Open a folder before reading attrubute '%s'.", name);
        auto dataset = group.openDataSet(name);
        auto space = dataset.getSpace();

        assert_msg(space.getSimpleExtentNdims() == 2, "Wrong number of dimensions when reading "
                   "BScope, expected 2, got %d.", space.getSimpleExtentNdims());

        std::vector<hsize_t> dims(2);
        space.getSimpleExtentDims(&dims[0]);
        H5::DataSpace memspace(2, &dims[0]);

        std::vector<unsigned char> buffer(dims[1] * dims[0]);
        dataset.read(&buffer[0], dataset.getDataType(), memspace, space);
        memspace.close();
        space.close();
        dataset.close();

        result.resize(dims[0]);
        for (size_t i = 0; i < dims[0]; i++) {
            result[i].resize(dims[1]);
            for (size_t j = 0; j < dims[1]; j++) {
                result[i][j] = buffer[i * dims[1] + j];
            }
        }
        return result;
    } catch (const H5::Exception& error) {
        printError(error, "getBScope");
    }
    return result;
}

std::vector<double> HDF5IO::getDoubleArray(const std::string& name) const {
    return getArray<double>(name);
}
std::vector<float> HDF5IO::getFloatArray(const std::string& name) const {
    return getArray<float>(name);
}
std::vector<int> HDF5IO::getIntArray(const std::string& name) const {
    return getArray<int>(name);
}
std::vector<std::vector<double>> HDF5IO::getDouble2DArray(const std::string& name) const {
    return get2DArray<double>(name);
}
std::vector<std::vector<float>> HDF5IO::getFloat2DArray(const std::string& name) const {
    return get2DArray<float>(name);
}
std::vector<std::vector<int>> HDF5IO::getInt2DArray(const std::string& name) const {
    return get2DArray<int>(name);
}
std::string HDF5IO::getFileName() const {
    return file_name;
}
std::string HDF5IO::getFolderName() const {
    return folder_name;
}

constexpr hsize_t HDF5IO::default_chunk_size;

#endif  // ifndef __MIC__

}  // namespace mm
