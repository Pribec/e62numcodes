#include "domain_relax_engines.hpp"

namespace mm {
// RelaxWithDistribution
BasicRelax::BasicRelax() {}

BasicRelax& BasicRelax::onlyNodes(Range<int> nodes) {
    nodes_ = nodes; return *this;
}

BasicRelax& BasicRelax::initialHeat(double in) {
    initial_heat = in; return *this;
}

BasicRelax& BasicRelax::finalHeat(double in) {
    final_heat = in; return *this;
}

BasicRelax& BasicRelax::projectionType(ProjectionType in) {
    projection_type = in; return *this;
}

BasicRelax& BasicRelax::boundaryProjectionThreshold(double in) {
    assert_msg(0.0 <= in && in < 2.0,
               "Weird projection threshold, it is equal to %.6f, but expected in range [0, 1]", in);
    boundary_projection_threshold = in; return *this;
}


BasicRelax& BasicRelax::numNeighbours(int in) {
    num_neighbours = in; return *this;
}

BasicRelax& BasicRelax::iterations(int in) {
    num_iterations = in; return *this;
}

BasicRelax& BasicRelax::potentialOrder(int in) {
    potential_order = in; return *this;
}

BasicRelax& BasicRelax::rebuildTreeAfter(int in) {
    rebuild_tree_after = in; return *this;
}

}  // namespace mm
