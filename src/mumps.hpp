#ifndef SRC_MUMPS_HPP_
#define SRC_MUMPS_HPP_
/**
 * @file
 * @brief Header file for out MUMPS wrapper.
 * @example mumps_test.cpp
 */


#include "common.hpp"
#include "includes.hpp"
#include "types.hpp"
#include "util.hpp"
#include <Eigen/Sparse>

#include "mpi.h"
#include "omp.h"
#include "dmumps_c.h"

/**
 * @brief A linear solver that is using MUMPS as its backend
 *
 * @tparam scalar_t As of this moment it has to be double (will exapnd as soon
 * as anything works)
 */
template <class scalar_t>
class MumpsSolver {
  public:
    /// Eigen matrix type
    typedef typename Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic> ei_matrix_t;
    /// Eigen vector type
    typedef typename Eigen::Matrix<scalar_t, Eigen::Dynamic, 1> ei_vec_t;
    /// MPI process ID
    MUMPS_INT myid;
    /// MUMPS all-purpose struct
    DMUMPS_STRUC_C id;
    /// Steps in mumps calculation
    enum MumpsJob {INIT = -1, END = -2, ANALYSIS = 1, FACTOR = 2, SOLVE = 3,
                   ANA_FACT = 4, FACT_SOL = 5, ANA_FACT_SOL = 6};

    MumpsSolver():myid(0) {
        omp_set_num_threads(2);
        static_assert(std::is_same<scalar_t, double>::value,
                      "MUMPS is currently limited to double");
        int mpi_ready;
        MPI_Initialized(&mpi_ready);
        prn(mpi_ready);
        if (!mpi_ready) {
            // Init MPI
            int provided;
            MPI_Init_thread(NULL, NULL, MPI_THREAD_FUNNELED, &provided);
            MPI_Comm_rank(MPI_COMM_WORLD, &myid);
        }
        prn(myid);

        /* Initialize a MUMPS instance. Use MPI_COMM_WORLD */
        id.comm_fortran = -987654;  // -987654 is a special value used by MUMPS
        // Also use the host computer to compute
        id.par = 1;
        // (0=unsymmetric matrix, 1=symmetric positive definite, 2=general symmetric)
        id.sym = 0;
        // Disable logging:

        id.icntl[1-1] = -1; id.icntl[2-1] = -1; id.icntl[3-1] = -1; id.icntl[4-1] = 1;
        // Init mumps
        id.job = MumpsJob::INIT;
        dmumps_c(&id);
    }
    /// Converts matrix to sparse and calls that sparse compute function
    void compute(const ei_matrix_t& A) {
        omp_set_num_threads(2);
        /* Convert the matrix into sparse */
        compute(static_cast<Eigen::SparseMatrix<scalar_t>>(A.sparseView()));
    }
    /// Computes the decomposition (if any) for matrix A
    void compute(const Eigen::SparseMatrix<scalar_t>& A) {
        omp_set_num_threads(2);

        id.n = A.rows();
        id.nnz = A.nonZeros();

        std::vector<MUMPS_INT> irn, jcn;
        std::vector<scalar_t> a;
        irn.reserve(id.nnz); jcn.reserve(id.nnz); a.reserve(id.nnz);

        for (int k = 0; k < A.outerSize(); ++k) {
            for (typename Eigen::SparseMatrix<scalar_t>::InnerIterator it(A, k); it; ++it) {
                a.push_back(it.value());
                irn.push_back(it.row()+1);
                jcn.push_back(it.col()+1);
            }
        }

        id.a = &a[0];
        id.irn = &irn[0];
        id.jcn = &jcn[0];
        id.job = MumpsJob::ANA_FACT;
        // id.icntl[4-1]=1;
        dmumps_c(&id);
      if (id.infog[0] < 0) {
            printf(" (PROC %d) ERROR RETURN: \tINFOG(1)= %d\n\t\t\t\tINFOG(2)= %d\n",
                   myid, id.infog[0], id.infog[1]);
      }
    }

    /// After `compute` it returns such x that Ax=b
    ei_vec_t solve(const ei_vec_t& b) {
        omp_set_num_threads(2);
        ei_vec_t rhs(b);
        id.rhs = &rhs[0];
        id.job = MumpsJob::SOLVE;
        dmumps_c(&id);
        // prn(rhs);
        return rhs;
    }
    ~MumpsSolver() {
        int mpi_ready;
        MPI_Initialized(&mpi_ready);
        if (mpi_ready) {
            id.job = MumpsJob::END;
            dmumps_c(&id);
            MPI_Finalize();
        }
    }
};


#endif  // SRC_MUMPS_HPP_
