#ifndef SRC_INTEGRATORS_HPP_
#define SRC_INTEGRATORS_HPP_

/**
 * @file integrators.hpp
 * ODE integrators, such as Runge-Kutta, Adams-Bashford and Milne predictor-corrector
 */

#include <Eigen/Dense>
#include "common.hpp"

namespace mm {

template <typename Scalar, int num_steps>
class AdamsBashforth;

/**
 * Class representing an explicit Runge-Kutta method.
 * @tparam Scalar type of scalars to use for calculations, eg. double
 * @tparam num_stages number of stages of the method
 */
template <typename Scalar, int num_stages>
class RKExplicit {
  public:
    typedef Scalar scalar_t;  ///< floating point type
    static const int stages = num_stages;  ///< number of stages

  private:
    Eigen::Matrix<scalar_t, stages, 1> alpha_;  ///< left row of the tableau
    Eigen::Matrix<scalar_t, stages, stages> beta_;  ///< central matrix of the tableau
    Eigen::Matrix<scalar_t, stages, 1> gamma_;  ///< bottom row of the tableau

  public:
    /**
     * Construct a Runge-Kutta method with tableau
     * @f$
     * \begin{array}{l|l}
     * \alpha & \beta \\ \hline
     *        & \gamma^\mathsf{T}
     * \end{array}
     * @f$
     * @param alpha Vector of size `stages`.
     * @param beta Matrix of size `stages` times `stages`. Only strictly lower triangular part is
     * used.
     * @param gamma Vector of size `stages`.
     */
    RKExplicit(const Eigen::Matrix<scalar_t, stages, 1>& alpha,
               const Eigen::Matrix<scalar_t, stages, stages>& beta,
               const Eigen::Matrix<scalar_t, stages, 1>& gamma)
            : alpha_(alpha), beta_(beta), gamma_(gamma) {}

    /**
     * Integrator class for RK methods.
     */
    template <typename func_t>
    class Integrator {
        const RKExplicit<scalar_t, num_stages> method_;  ///< method used
        scalar_t t0_;  ///< start time
        scalar_t dt_;  ///< time step
        int max_steps;  ///< number of steps
        Eigen::VectorXd y0_;  ///< initial value
        const func_t& func_;  ///< function to integrate

      public:
        /**
         * Constructs stepper. It is recommended to use the RKExplicit::solve() factory method
         * to construct this object.
         */
        Integrator(const RKExplicit<scalar_t, num_stages>& method, const func_t& func, scalar_t t0,
                   scalar_t t_max, scalar_t dt, Eigen::VectorXd y0) :
                method_(method), t0_(t0), dt_(dt), max_steps(iceil((t_max - t0) / dt)),
                y0_(std::move(y0)), func_(func) {}

        /**
         * Class representing a step in the integration process. This class satisfies
         * std::forward_iterator requirements and can therefore be used with STL-type algorithms.
         */
        class IterationStep : public std::iterator<
                std::forward_iterator_tag,   // iterator_category
                IterationStep,               // value_type
                int,                         // difference_type
                const IterationStep*,        // pointer
                IterationStep                // reference
        > {
            const Integrator& integrator;  ///< reference to underlying integrator
            scalar_t t;  ///< current time
            Eigen::VectorXd y;  ///< current value
            int cur_step;  ///< current step

          public:
            /// Construct an iterator at the initial values of the equation.
            explicit IterationStep(const Integrator& integrator)
                    : integrator(integrator), t(integrator.t0_), y(integrator.y0_), cur_step(0) {}

            /// Construct an (invalid) iterator pointing past the last step
            IterationStep(const Integrator& integrator, int) : integrator(integrator), t(), y(),
                                                               cur_step(integrator.max_steps + 1) {}

            /// Advance the stepper for one time step, returning the new value.
            IterationStep& operator++() {
                y = integrator.method_.step(integrator.func_, t, y, integrator.dt_);
                t += integrator.dt_;
                cur_step++;
                return *this;
            }

            /**
             * Advance the stepper for one time step, returning the old value.
             * @warning usually the prefix increment `++step` is preferred, due to not having a
             * temporary variable.
             */
            IterationStep operator++(int) {
                IterationStep retval = *this;
                ++(*this);
                return retval;
            }

            /// Compare two steppers if they are on the same step.
            bool operator==(const IterationStep& other) const { return cur_step == other.cur_step; }

            /// Negation of IterationStep::operator==.
            bool operator!=(const IterationStep& other) const { return !(*this == other); }

            /// Noop, used to conmform to std::iterator requirements
            IterationStep& operator*() { return *this; }

            /// const version of IterationStep::operator*
            const IterationStep& operator*() const { return *this; }

            /// Returns `false` if integrator went past the last step and `true` otherwise.
            explicit operator bool() {
                return cur_step <= integrator.max_steps;
            }

            /// Returns true if integrator just completed its last step.
            bool is_last() const {
                return cur_step == integrator.max_steps;
            }

            /// Get current time.
            scalar_t time() const { return t; }

            /// Get current value.
            Eigen::VectorXd value() const { return y; }

            /// Read-write access to current time
            scalar_t& time() { return t; }

            /// Read-write access to current value.
            Eigen::VectorXd& value() { return y; }

            /// Output current state of the stepper
            friend std::ostream& operator<<(std::ostream& os,
                                            const typename Integrator::IterationStep& step) {
                os << "t = " << step.t << "; y = " << step.y;
                return os;
            }
        };

        typedef IterationStep iterator;  ///< iterator type
        typedef IterationStep const_iterator;  ///< const iterator type

        /// Creates stepper at positioned at initial value.
        iterator begin() { return IterationStep(*this); }

        /// Same as Integrator::begin()
        const_iterator cbegin() { return IterationStep(*this); }

        /// Creates an invalid stepper at positioned past the last step, meant to be used for
        /// comparison `it == end()` only
        iterator end() { return IterationStep(*this, 0); }

        /// Same as Integrator::end()
        const_iterator cend() { return IterationStep(*this, 0); }
    };


    /**
     * Returns a solver using this method.
     * @param func Function to integrate.
     * @param t0 Start time.
     * @param t_max End time.
     * @param dt Time step.
     * @param y0 Initial value.
     * @warning Last step might not finish exactly on `t_max`. The number of steps is calculated as
     * `std::ceil((t_max - t0)/dt)`.
     * @return A solver object.
     * Example:
     * @snippet integrators_test.cpp RK4
     */
    template <typename func_t>
    Integrator<func_t> solve(const func_t& func, scalar_t t0, scalar_t t_max, scalar_t dt,
                             const Eigen::VectorXd& y0) const {
        return Integrator<func_t>(*this, func, t0, t_max, dt, y0);
    }

  private:
    /// Returns next value
    template <typename func_t>
    Eigen::VectorXd step(const func_t& func, scalar_t t, const Eigen::VectorXd& y,
                         scalar_t dt) const {
        Eigen::Matrix<scalar_t, Eigen::Dynamic, stages> k(y.size(), stages);
        k.col(0) = func(t + alpha_(0) * dt, y);
        for (int i = 1; i < stages; ++i) {
            k.col(i) = func(t + alpha_(i) * dt,
                            y + dt * k.leftCols(i) * beta_.row(i).head(i).transpose());
        }
        return y + dt * k * gamma_;
    }

  public:
    /// Output the method's tableau for debugging.
    template <typename scalar_t, int stages>
    friend std::ostream& operator<<(std::ostream& os, const RKExplicit<scalar_t, stages>&);

    template <typename Scalar2, int num_steps2>
    friend class AdamsBashforth;
};

/// @cond
template <typename Scalar, int num_stages>
const int RKExplicit<Scalar, num_stages>::stages;
/// @endcond

/// Output the method's tableau for debugging.
template <typename scalar_t, int num_stages>
std::ostream& operator<<(std::ostream& os, const RKExplicit<scalar_t, num_stages>& method) {
    os << "RKExplicit with " << num_stages << " stages and\n"
       << "alpha = " << method.alpha_.transpose() << '\n'
       << "beta = " << method.beta_ << '\n'
       << "gamma = " << method.gamma_.transpose();
    return os;
}

/**
 * Namespace containing most known methods for integrating ODEs.
 */
namespace integrators {

/**
 * Namespace containing factory functions for explicit single step integrators,
 * such as Euler's method, Midpoint rule, Runge Kutta method, ...
 */
namespace Explicit {

/// Standard RK4 4th order method
template <class scalar_t = double>
static RKExplicit<scalar_t, 4> RK4() {
    Eigen::Matrix<scalar_t, 4, 1> alpha;
    alpha << 0, 0.5, 0.5, 1.0;
    Eigen::Matrix<scalar_t, 4, 4> beta;
    beta << 0.0, 0.0, 0.0, 0.0,
            0.5, 0.0, 0.0, 0.0,
            0.0, 0.5, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0;
    Eigen::Matrix<scalar_t, 4, 1> gamma;
    gamma << 1. / 6., 2. / 6., 2. / 6., 1. / 6.;
    return {alpha, beta, gamma};
}

/// 3/8 rule 4th order method
template <class scalar_t = double>
static RKExplicit<scalar_t, 4> RK38() {
    Eigen::Matrix<scalar_t, 4, 1> alpha;
    alpha << 0, 1. / 3., 2. / 3., 1.0;
    Eigen::Matrix<scalar_t, 4, 4> beta;
    beta << 0.0, 0.0, 0.0, 0.0,
            1. / 3., 0.0, 0.0, 0.0,
            -1. / 3., 1.0, 0.0, 0.0,
            1.0, -1.0, 1.0, 0.0;
    Eigen::Matrix<scalar_t, 4, 1> gamma;
    gamma << 1. / 8., 3. / 8., 3. / 8., 1. / 8.;
    return {alpha, beta, gamma};
}

/// Explicit Euler's method, 1st order
template <class scalar_t = double>
static RKExplicit<scalar_t, 1> Euler() {
    Eigen::Matrix<scalar_t, 1, 1> alpha;
    alpha << 0.0;
    Eigen::Matrix<scalar_t, 1, 1> beta;
    beta << 0.0;
    Eigen::Matrix<scalar_t, 1, 1> gamma;
    gamma << 1.0;
    return {alpha, beta, gamma};
}

/// Explicit midpoint rule, 2nd order
template <class scalar_t = double>
static RKExplicit<scalar_t, 2> Midpoint() {
    Eigen::Matrix<scalar_t, 2, 1> alpha;
    alpha << 0.0, 0.5;
    Eigen::Matrix<scalar_t, 2, 2> beta;
    beta << 0.0, 0.0, 0.5, 0.0;
    Eigen::Matrix<scalar_t, 2, 1> gamma;
    gamma << 0.0, 1.0;
    return {alpha, beta, gamma};
}

/// Kutta's third order method
template <class scalar_t = double>
static RKExplicit<scalar_t, 3> RK3() {
    Eigen::Matrix<scalar_t, 3, 1> alpha;
    alpha << 0, 0.5, 1.0;
    Eigen::Matrix<scalar_t, 3, 3> beta;
    beta << 0.0, 0.0, 0.0,
            0.5, 0.0, 0.0,
            -1.0, 2.0, 0.0;
    Eigen::Matrix<scalar_t, 3, 1> gamma;
    gamma << 1. / 6., 2. / 3., 1. / 6.;
    return {alpha, beta, gamma};
}

/// Fifth order method appearing in Fehlberg's method
template <class scalar_t = double>
static RKExplicit<scalar_t, 6> Fehlberg5() {
    Eigen::Matrix<scalar_t, 6, 1> alpha;
    alpha << 0, 1. / 4., 3. / 8., 12. / 13., 1., 1. / 2.;
    Eigen::Matrix<scalar_t, 6, 6> beta;
    beta << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
            1. / 4., 0.0, 0.0, 0.0, 0.0, 0.0,
            3. / 32., 9. / 32., 0.0, 0.0, 0.0, 0.0,
            1932. / 2197, -7200. / 2197., 7296. / 2197., 0.0, 0.0, 0.0,
            439. / 216., -8., 3680. / 513., -845. / 4104., 0.0, 0.0,
            -8. / 27., 2., -3544. / 2565., 1859. / 4104., -11. / 40., 0.0;
    Eigen::Matrix<scalar_t, 6, 1> gamma;
    gamma << 16. / 135., 0.0, 6656. / 12825., 28561. / 56430., -9. / 50., 2. / 55.;
    return {alpha, beta, gamma};
}

/// Internal namespace containing implementation details for Explicit::of_order function.
namespace of_order_internal {

/// @cond
template <int order, class scalar_t>
struct _of_order_impl {
    static RKExplicit<scalar_t, order> of_order() {
        static_assert(order > 0, "Order must be positive.");
        static_assert(order < 5, "Methods of higher orders do not correspond with the "
                "number of stages, you must call them manually. If you got this error calling "
                "AB5() method, supply eg. Fehlberg5 as the first parameter and its type as"
                "the template parameter.");
        throw;  // to avoid warnings
    }
};

/// Specialization of order 1, Euler's method.
template <class scalar_t>
struct _of_order_impl<1, scalar_t> {
    static RKExplicit<scalar_t, 1> of_order() { return Euler(); }
};

/// Specialization of order 2, Midpoint method.
template <class scalar_t>
struct _of_order_impl<2, scalar_t> {
    static RKExplicit<scalar_t, 2> of_order() { return Midpoint(); }
};

/// Specialization of order 3, RK3.
template <class scalar_t>
struct _of_order_impl<3, scalar_t> {
    static RKExplicit<scalar_t, 3> of_order() { return RK3(); }
};

/// Specialization of order 4, RK4.
template <class scalar_t>
struct _of_order_impl<4, scalar_t> {
    static RKExplicit<scalar_t, 4> of_order() { return RK4(); }
};
/// @endcond
}  // namespace of_order_internal

/**
 * Returns Runge Kutta explicit method of requested order with given floating point type.
 */
template <int order, class scalar_t = double>
static RKExplicit<scalar_t, order> of_order() {
    return of_order_internal::_of_order_impl<order, scalar_t>::of_order();
}

}  // namespace Explicit
}  // namespace integrators

/**
 * Class representing an AdamsBashforth method, an explicit linear multistep method.
 * @tparam Scalar type of scalars to use for calculations, eg. double
 * @tparam num_steps number of previous steps the method uses
 */
template <typename Scalar, int num_steps>
class AdamsBashforth {
  public:
    typedef Scalar scalar_t;  ///< floating point type
    static const int steps = num_steps;  ///< number of steps of the multistep method

  private:
    Eigen::Matrix<scalar_t, steps, 1> b_;  ///< coefficients of the multistep method

  public:
    /**
     * Construct an `num_steps`-step Adams Bashforth method with coefficients `b`, given as
     * @f$y_{n+s} = y_{n+s-1} + h \sum_{j=0}^{s-1} b_j f(t_j, y_j)@f$.
     * @param b Vector of size `num_steps` representing the coefficients of the method.
     */
    AdamsBashforth(const Eigen::Matrix<scalar_t, steps, 1>& b) : b_(b) {
        assert_msg(std::abs(b_.sum() - 1.0) < 1e-15, "AB method coefficients do not sum up to 1, "
                "but %.16f instead.", b_.sum());
    }

    /**
     * Integrator class for AB methods.
     * @tparam starting_method_t Types of the underlying starting method, required to get first
     * `num_steps` values.
     * @warning
     * You should choose a method that provides same order of accuracy, is a single step method and
     * has a fixed time step. A default choice is `num_steps` stage Runge Kutta method,
     * which might not always be correct. Predefined methods that are called by name, such
     * as ExplicitMultistep::AB5(), have correct initial methods.
     */
    template <typename func_t, typename initial_method_t = RKExplicit<Scalar, num_steps>>
    class Integrator {
        const initial_method_t initial_method_;  ///< method used for first `num_steps` steps
        const AdamsBashforth<scalar_t, num_steps> method_;  ///< method used for all next steps
        scalar_t t0_;  ///< start time
        scalar_t dt_;  ///< time step
        int max_steps;  ///< number of steps
        Eigen::VectorXd y0_;  ///< initial value
        const func_t& func_;  ///< function to integrate

      public:
        /**
         * Constructs stepper with given initial method. It is recommended to use the
         * AdamsBashforth::solve() factory method to construct this object.
         */
        Integrator(const AdamsBashforth<scalar_t, num_steps>& method,
                   const initial_method_t& initial_method,
                   const func_t& func, scalar_t t0, scalar_t t_max, scalar_t dt, Eigen::VectorXd y0)
                : initial_method_(initial_method), method_(method), t0_(t0),
                  dt_(dt), max_steps(iceil((t_max - t0) / dt)), y0_(std::move(y0)),
                  func_(func) {}

        /**
         * Class representing a step in the integration process. This class satisfies
         * std::forward_iterator requirements and can therefore be used with STL-type algorithms.
         */
        class IterationStep : public std::iterator<
                std::forward_iterator_tag,   // iterator_category
                IterationStep,               // value_type
                int,                         // difference_type
                const IterationStep*,        // pointer
                IterationStep                // reference
        > {
            const Integrator& integrator;  ///< reference to underlying integrator
            scalar_t t;  ///< current time
            Eigen::Matrix<scalar_t, Eigen::Dynamic, steps> last_ys;  ///< current value
            int cur_step;  ///< current step

          public:
            /// Construct an iterator at the initial values of the equation.
            explicit IterationStep(const Integrator& integrator)
                    : integrator(integrator), t(integrator.t0_),
                      last_ys(integrator.y0_.size(), steps), cur_step(0) {
                last_ys.col(steps-1) = integrator.y0_;
            }

            /// Construct an (invalid) iterator pointing past the last step
            IterationStep(const Integrator& integrator, int) :
                    integrator(integrator), t(), last_ys(), cur_step(integrator.max_steps + 1) {}

            /// Advance the stepper for one time step, returning the new value.
            IterationStep& operator++() {
                cur_step++;
                Eigen::VectorXd next;
                if (cur_step < steps) {  // don't have enough steps, do a RK method.
                    next = integrator.initial_method_.step(
                            integrator.func_, t, last_ys.col(steps - 1), integrator.dt_);
                } else {
                    next = integrator.method_.step(integrator.func_, t, last_ys, integrator.dt_);
                }
                t += integrator.dt_;
                // shift values
                for (int i = 0; i < steps-1; ++i) {
                    last_ys.col(i) = last_ys.col(i+1);
                }
                last_ys.col(steps-1) = next;
                return *this;
            }

            /**
             * Advance the stepper for one time step, returning the old value.
             * @warning usually the prefix increment `++step` is preferred, due to not having a
             * temporary variable.
             */
            IterationStep operator++(int) {
                IterationStep retval = *this;
                ++(*this);
                return retval;
            }

            /// Compare two steppers if they are on the same step.
            bool operator==(const IterationStep& other) const { return cur_step == other.cur_step; }

            /// Negation of IterationStep::operator==.
            bool operator!=(const IterationStep& other) const { return !(*this == other); }

            /// Noop, used to conmform to std::iterator requirements
            IterationStep& operator*() { return *this; }

            /// const version of IterationStep::operator*
            const IterationStep& operator*() const { return *this; }

            /// Returns `false` if integrator went past the last step and `true` otherwise.
            explicit operator bool() {
                return cur_step <= integrator.max_steps;
            }

            /// Returns true if integrator just completed its last step.
            bool is_last() const {
                return cur_step == integrator.max_steps;
            }

            /// Get current time.
            scalar_t time() const { return t; }

            /// Get current value.
            typename Eigen::Matrix<scalar_t, Eigen::Dynamic, steps>::ConstColXpr value() const {
                return last_ys.col(steps-1);
            }

            /// Read-write access to current time
            scalar_t& time() { return t; }

            /// Read-write access to current value.
            typename Eigen::Matrix<scalar_t, Eigen::Dynamic, steps>::ColXpr value() {
                return last_ys.col(steps-1);
            }

            /// Output current state of the stepper
            friend std::ostream& operator<<(std::ostream& os,
                                            const typename Integrator::IterationStep& step) {
                os << "t = " << step.t << "; y = " << step.last_ys.col(steps-1);
                return os;
            }
        };

        typedef IterationStep iterator;  ///< iterator type
        typedef IterationStep const_iterator;  ///< const iterator type

        /// Creates stepper at positioned at initial value.
        iterator begin() { return IterationStep(*this); }

        /// Same as Integrator::begin()
        const_iterator cbegin() { return IterationStep(*this); }

        /// Creates an invalid stepper at positioned past the last step, meant to be used for
        /// comparison `it == end()` only
        iterator end() { return IterationStep(*this, 0); }

        /// Same as Integrator::end()
        const_iterator cend() { return IterationStep(*this, 0); }
    };

    /**
     * Returns a solver using this method.
     * @param func Function to integrate.
     * @param t0 Start time.
     * @param t_max End time.
     * @param dt Time step.
     * @param y0 Initial value.
     * @warning Last step might not finish exactly on `t_max`. The number of steps is calculated as
     * `std::ceil((t_max - t0)/dt)`.
     * @return A solver object.
     */
    template <typename func_t>
    Integrator<func_t> solve(const func_t& func, scalar_t t0, scalar_t t_max, scalar_t dt,
                             const Eigen::VectorXd& y0) const {
        return Integrator<func_t, RKExplicit<scalar_t, num_steps>>(
                *this, integrators::Explicit::of_order<num_steps>(), func, t0, t_max, dt, y0);
    }

    /// Same as Integrator::solve, but offers the option of supplying your own initial method.
    template <typename initial_method_t, typename func_t>
    Integrator<func_t, initial_method_t> solve(const initial_method_t& method, const func_t& func,
                                               scalar_t t0, scalar_t t_max, scalar_t dt,
                                               const Eigen::VectorXd& y0) const {
        return Integrator<func_t, initial_method_t>(*this, method, func, t0, t_max, dt, y0);
    }

  private:
    /// Returns next value
    template <typename func_t>
    Eigen::VectorXd step(const func_t& func, scalar_t t,
                         const Eigen::Matrix<scalar_t, Eigen::Dynamic, steps>& last_ys,
                         scalar_t dt) const {
        Eigen::VectorXd result = last_ys.col(steps-1);
        for (int i = 0; i < steps; ++i) {
            result += dt * b_(i) * func(t - i*dt, last_ys.col(i));
        }
        return result;
    }

  public:
    /// Output the method's tableau for debugging.
    template <typename scalar_t, int stages>
    friend std::ostream& operator<<(std::ostream& os, const AdamsBashforth<scalar_t, stages>&);
};

/// @cond
template <typename Scalar, int num_steps>
const int AdamsBashforth<Scalar, num_steps>::steps;
/// @endcond

/// Output the method's tableau for debugging.
template <typename scalar_t, int num_steps>
std::ostream& operator<<(std::ostream& os, const AdamsBashforth<scalar_t, num_steps>& method) {
    os << "AdamsBashforth with " << num_steps << " stages and\n"
       << "b = " << method.b_.transpose();
    return os;
}

namespace integrators {

/**
 * Namespace containing factory functions for explicit linear multistep integrators.
 */
namespace ExplicitMultistep {

/// Standard Euler's method.
template <class scalar_t = double>
static AdamsBashforth<scalar_t, 1> AB1() {
    Eigen::Matrix<scalar_t, 1, 1> b(1); b << 1.0;
    return {b};
}

/// Two step AB method.
template <class scalar_t = double>
static AdamsBashforth<scalar_t, 2> AB2() {
    Eigen::Matrix<scalar_t, 2, 1> b(2); b << -1./2., 3./2.;
    return {b};
}

/// Three step AB method.
template <class scalar_t = double>
static AdamsBashforth<scalar_t, 3> AB3() {
    Eigen::Matrix<scalar_t, 3, 1> b(3); b << 5./12., -4./3., 23./12.;
    return {b};
}

/// Four step AB method.
template <class scalar_t = double>
static AdamsBashforth<scalar_t, 4> AB4() {
    Eigen::Matrix<scalar_t, 4, 1> b(4); b << -3./8., 37./24., -59./24., 55./24.;
    return {b};
}

/// Five step AB method.
template <class scalar_t = double>
static AdamsBashforth<scalar_t, 5> AB5() {
    Eigen::Matrix<scalar_t, 5, 1> b(5);
    b << 251./720., -637./360., 109./30., -1387./360., 1901./720.;
    return {b};
}

/// Internal namespace containing implementation details for Explicit::of_order function.
namespace of_order_internal {

/// @cond
template <int order, class scalar_t>
struct _of_order_impl {
    static RKExplicit<scalar_t, order> of_order() {
        static_assert(order > 0, "Order must be positive.");
        static_assert(order < 6, "Methods of so high orders are not implemented yet, but it's"
                " very simple to add them!");
        throw;
    }
};

/// Specialization of order 1
template <class scalar_t>
struct _of_order_impl<1, scalar_t> {
    static AdamsBashforth<scalar_t, 1> of_order() { return AB1(); }
};

/// Specialization of order 2
template <class scalar_t>
struct _of_order_impl<2, scalar_t> {
    static AdamsBashforth<scalar_t, 2> of_order() { return AB2(); }
};

/// Specialization of order 3
template <class scalar_t>
struct _of_order_impl<3, scalar_t> {
    static AdamsBashforth<scalar_t, 3> of_order() { return AB3(); }
};

/// Specialization of order 4
template <class scalar_t>
struct _of_order_impl<4, scalar_t> {
    static AdamsBashforth<scalar_t, 4> of_order() { return AB4(); }
};

/// Specialization of order 5
template <class scalar_t>
struct _of_order_impl<5, scalar_t> {
    static AdamsBashforth<scalar_t, 5> of_order() { return AB5(); }
};

/// @endcond
}  // namespace of_order_internal

/**
 * Returns Runge Kutta explicit method of requested order with given floating point type.
 */
template <int order, class scalar_t = double>
static AdamsBashforth<scalar_t, order> of_order() {
    return of_order_internal::_of_order_impl<order, scalar_t>::of_order();
}

}  // namespace ExplicitMultistep

}  // namespace integrators

}  // namespace mm

#endif  // SRC_INTEGRATORS_HPP_
