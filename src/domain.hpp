#ifndef SRC_DOMAIN_HPP_
#define SRC_DOMAIN_HPP_

/**
 * @file
 * @brief Header file implementing domains.
 * All domains are derived from an abstract base class Domain.
 * Specific basi geometric-shaped domains are supported:
 *  - rectangular
 *  - circular
 *
 * as well as their compositions:
 * - union
 * - difference
 * @example domain_test.cpp
 * @todo TODO(jureslak): add rotations and translations.
 */

#include "common.hpp"
#include "includes.hpp"
#include "types.hpp"
#include "util.hpp"

namespace mm {

template <class T>
class KDTree;

/**
 * Enum holding the defaults for node types;
 */
enum NODE_TYPE : int {
    DEFAULT_BOUNDARY = -1,  ///< Boundary is -1
    DEFAULT_INTERNAL = 1  ///< Internal is +1
};

/**
 * Enum for the normal direction vector, telling in which side of the shape are the nodes
 * contained.
 */
enum NORMAL : int {
    INSIDE = 1,  ///< Indicating normal vector is pointing inside.
    OUTSIDE = -1  ///< Indicating normal vector is pointing outside.
};

/// Enum for the thicknessness of domain.
enum THICKNESS : int {
    THICK = 1,  ///< Indicating domain is thick.
    THIN = -1  ///< Indicating domain is thin.
};

/**
 * Enum to indicate what type of border conflict resolution is wanted
 * when two borders overlap in add or subtract.
 * <b>This only affects overlapping borders.</b>
 */
enum class BOUNDARY_TYPE {
    NONE,  ///< No border.
    SINGLE,  ///< Single border.
    DOUBLE  ///< Duplicated border.
};

/**
 * @brief An abstract base class for domains.
 */
template <class vec_t>
class Domain {
  public:  // any variable added here should also be copied in the clone function
    typedef vec_t vector_t;  ///< Vector data type.
    typedef typename vec_t::Scalar scalar_t;   ///< Numeric data type.
    ///< Dimensionality of the domain.
    enum { dim = vector_t::dimension };
    Range<vec_t> positions;  ///< Positions of internal discretization points.
    /**
     * Variable used to store support points of each node. For each node i, support[i] is
     * list of indices of i's support ordered by distance from i.
     * The first element in the list is always i (if support had been set), followed by one or more
     * others. The according squared distances are stored in `distances` array.
     * Support positions can for example be accessed using simply:
     * @code
     * positions[support[i]]
     * @endcode
     **/
    Range<Range<int>> support;
    /**
     * This variable stores squared distances to each point of point's support. `distances[i]` is a
     * sorted array of squared distances to support points, starting with zero and continuing in
     * non descending fashion.
     **/
    Range<Range<scalar_t>> distances;
    /**
     * Storing types of nodes aligned with positions. <b>Negative types are reserved for
     * boundary nodes, positive for interior. Zero type is not used</b>
     * Example:
     * @code
     * positions = {p1, p2, p3, p4, p5};
     * types =     { 2, -1,  2,  1, -1};
     * @endcode
     * Nodes p2 and p5 are of type -1, p1 and p3 of type 2 and so on.
     */
    Range<int> types;

  protected:
    /**
     * Mapping index of a boundary node among all nodes, to its index among boundary nodes.
     * Indices of internal nodes are set to -1.
     */
    Range<int> boundary_map_;

    /// List of normals in boundary nodes, indexed by boundary nodes only.
    Range<vec_t> normals_;

  public:
    Range<std::unique_ptr<Domain<vec_t>>> child_domains;  ///< List of child domains.

    /// Allow default constructor.
    Domain() : normal_vector(INSIDE), thickness(THICK), contains_precision(EPS) {}
    /// Allow move constructor.
    Domain(Domain&&) = default;
    /// Allow move assignment.
    Domain& operator=(Domain&&) = default;
    /// Remove copy constructor.
    Domain(const Domain&) = delete;
    /// Remove copy assignment.
    Domain& operator=(const Domain&) = delete;
    /// Virtual destructor, so derived destructors get invoked and we avoid memory leaks.
    virtual ~Domain() = default;
    /// Makes a clone of a domain.
    template <typename T>
    static T&& makeClone(const T& t) {
        return std::move(*static_cast<T*>(static_cast<const Domain<vec_t>*>(&t)->clone()));
    }

    /// Get the collection of normals.
    const Range<vec_t>& normals() const {
        return normals_;
    }

    /// Get boundary map.
    const Range<int>& boundary_map() const {
        return boundary_map_;
    }

    /// Returns unit normal in `i`-th node. The node must be a boundary node.
    const vec_t& normal(int i) const {
        assert_msg(0 <= i && i <= size(), "Index %d out of range [0, %d)", i, size());
        assert_msg(types[i] < 0, "Node %d must be a boundary node, got type %d.", i, types[i]);
        assert_msg(boundary_map_[i] != -1, "Node %d does not have a normal. Maybe you manually set"
                " supports and positions instead of using addPoint* methods?", i);
        return normals_[boundary_map_[i]];
    }

    /**
     * @brief Returns a range of all boundary nodes.
     * @returns A range of all nodes with type < 0
     */
    Range<vec_t> getBoundaryNodes() const { return positions[types < 0]; }
    /**
     * @brief Returns a range of all internal nodes.
     * @returns A range of all nodes with type > 0
     */
    Range<vec_t> getInternalNodes() const { return positions[types > 0]; }
    /// returns number of nodes in a domain
    int size() const { return positions.size(); }
    /**
     * @brief Tests if a point is contained in a domain and not contained
     * in any obstacles.
     * @param point The point we are interested in,
     * @return True if point is contained in the basic domain
     * and false otherwise.
     */
    bool contains(const vec_t& point) const {
        auto it = child_domains.rbegin();  // reverse iterator, because new add
        for (; it != child_domains.rend(); ++it) {  // overrides old one
            switch ((*it)->normal_vector) {
                case INSIDE:  // inside one of the union members
                    if ((*it)->contains(point)) return true;
                    break;
                case OUTSIDE:  // inside one of the obstacles
                    if (!(*it)->contains(point)) return false;
                    break;
            }
        }
        return inside(point);
    }

    /**
     * Adds a single point with specified type to the domain.
     * @param point Point to add.
     * @param type Type of the point to add.
     */
    void addPoint(const vec_t& point, int type) {
        assert_msg(contains(point), "Point %s with type %d you are tyring to add is not contained "
                "in the domain!", point, type);
        positions.push_back(point);
        types.push_back(type);
        support.emplace_back();
        distances.emplace_back();
        boundary_map_.push_back(-1);
    }

    /**
     * Adds a boundary point to the domain.
     * @param point Coordinates of a point.
     * @param type Type of the point, must be negative.
     * @param normal Outside unit normal to the boundary at point `point`.
     */
    void addBoundaryPoint(const vec_t& point, int type, const vec_t& normal) {
        assert_msg(type < 0, "Type of boundary points must be negative, got %d.", type);
        addPoint(point, type);
        boundary_map_.back() = normals_.size();
        normals_.push_back(normal);
    }

    /** Changes point i to boundary point with given `type` and `normal`. */
    void changeToBoundary(int i, const vec_t& pos, int type, const vec_t& normal) {
        assert_msg(0 <= i && i < size(), "Index %d out of range [0, %d).", i, size());
        assert_msg(types[i] >= 0, "Point %d is already a boundary point with type %d.", i,
                   types[i]);
        assert_msg(type, "New type must be negative, got %d.", type);
        assert_msg(contains(pos), "Point %s with type %d you are tyring to add is not contained "
                "in the domain!", pos, type);
        positions[i] = pos;
        types[i] = type;
        boundary_map_[i] = normals_.size();
        normals_.push_back(normal);
    }

    /**
     * @brief Adds a domain as a member to the union. Domain given as a parameter is deep
     * copied.
     * Domain nodes are moved to the union and removed from given domain.
     * @param d Domain to be added as a union.
     * @param bt How to solve overlapping boundary conflicts.
     */
    void add(const Domain& d, BOUNDARY_TYPE bt = BOUNDARY_TYPE::SINGLE) {
        add(std::unique_ptr<Domain>(d.clone()), bt);
    }
    /**
     * @brief Adds a domain as a member to the union. The ownership of a pointer is
     * transferred to this instance and the pointer is freed when this instance
     * goes out of scope. Domain nodes are moved to the union and removed from given
     * domain.
     * @param d Pointer to a domain that is to be added as a union.
     * @param bt How to solve overlapping boundary conflicts.
     */
    void add(std::unique_ptr<Domain<vec_t>> d, BOUNDARY_TYPE bt = BOUNDARY_TYPE::SINGLE) {
        d->setUnionNormal();
        switch (bt) {
            case BOUNDARY_TYPE::DOUBLE:
                setThin();
                d->setThin();
                break;
            case BOUNDARY_TYPE::SINGLE:
                setThick();
                d->setThin();
                break;
            case BOUNDARY_TYPE::NONE:
                setThick();
                d->setThick();
                break;
        }
        Range<int> invalid_old;
        for (int i = 0; i < size(); ++i) {
            if (types[i] < 0 && d->contains(positions[i])) {
                invalid_old.push_back(i);
            }
        }
        clearNodes(invalid_old);

        Range<int> invalid_new;
        for (int i = 0; i < d->size(); ++i) {
            if (d->types[i] < 0 && contains(d->positions[i])) {
                invalid_new.push_back(i);
            }
        }
        d->clearNodes(invalid_new);
        positions.append(d->positions);
        types.append(d->types);
        support.append(d->support);
        distances.append(d->distances);
        for (int i : d->boundary_map_) {
            boundary_map_.push_back((i == -1) ? -1 : (normals_.size()+i));
        }
        normals_.append(d->normals_);

        // update bbox
        for (int i = 0; i < vec_t::dimension; ++i) {
            bbox.first[i] = std::min(bbox.first[i], d->bbox.first[i]);
            bbox.second[i] = std::max(bbox.second[i], d->bbox.second[i]);
        }


        d->clear();  // we don't need these any more
        child_domains.push_back(std::move(d));
        setThick();
    }

    /**
     * @brief Adds a domain as an obstacle. Given domain is deep copied.
     * Nodes are automatically updated and nodes that are now inside and
     * obstacle are automatically deleted.
     * @param d Domain to add as an obstacle.
     * @param bt How to solve overlapping boundary conflicts.
     * @returns A mapping `m` of indices of subtracted domain to its indices in new domain.
     * If `domain.subtract(obstacle)` is called, then node in obstacle with index
     * `i`, has index `m[i]` in the new domain, or is not present, if `m[i] == -1`.
     */
    Range<int> subtract(const Domain& d, BOUNDARY_TYPE bt = BOUNDARY_TYPE::SINGLE) {
        return subtract(std::unique_ptr<Domain>(d.clone()), bt);
    }
    /**
     * @brief Adds a domain as an obstacle. Ownership of the unique_ptr is
     * transferred to this instance. Nodes are automatically updated and nodes
     * that are now inside and obstacle are automatically deleted.
     * @param d unique_ptr to Domain that is to be added as an obstacle.
     * @param bt How to solve overlapping boundary conflicts.
     */
    Range<int> subtract(std::unique_ptr<Domain<vec_t>> d,
                        BOUNDARY_TYPE bt = BOUNDARY_TYPE::SINGLE) {
        d->setObstacleNormal();
        // set boundary type
        switch (bt) {
            case BOUNDARY_TYPE::DOUBLE:
                setThick();
                d->setThick();
                break;
            case BOUNDARY_TYPE::SINGLE:
                setThin();
                d->setThick();
                break;
            case BOUNDARY_TYPE::NONE:
                setThin();
                d->setThin();
                break;
        }
        // clear invalid positions and their types
        Range<int> invalid = positions.filter(
            [&](const vec_t& point) { return !d->contains(point); });
        clearNodes(invalid);

        // add new boundary nodes
        Range<int> new_boundary;
        int num_of_nodes = d->size();
        int count = positions.size();
        Range<int> new_indexes(num_of_nodes);
        for (int i = 0; i < num_of_nodes; ++i) {
            if (d->types[i] < 0 && contains(d->positions[i])) {
                new_boundary.push_back(i);
                new_indexes[i] = count++;
            } else {
                new_indexes[i] = -1;
            }
        }
        positions.append(d->positions[new_boundary]);
        types.append(d->types[new_boundary]);
        support.append(d->support[new_boundary]);
        distances.append(d->distances[new_boundary]);
        normals_.reserve(new_boundary.size());
        for (int i : new_boundary) {
            boundary_map_.push_back(normals_.size());
            normals_.push_back(-d->normal(i));
        }

        // leave bbox as is
        d->clear();  // we don't need these any more
        child_domains.push_back(std::move(d));  // add it to the obstacle list
        setThick();
        return new_indexes;
    }

    /// Checks if domain is in valid state.
    bool valid() const {
        if (size() != types.size()) return false;
        if (size() != boundary_map_.size()) return false;
        if (size() != support.size()) return false;
        if (size() != distances.size()) return false;
        for (const vec_t& pos : getInternalNodes()) {
            if (!contains(pos)) return false;
        }
        int cnt = 0;
        for (const vec_t& pos : getBoundaryNodes()) {
            cnt++;
            if (contains(pos) == (thickness == THIN)) return false;
        }
        if (cnt != normals_.size()) return false;
        return true;
    }

    /// Clears all data about discretization in the domain.
    void clear() {
        positions.clear();
        types.clear();
        support.clear();
        distances.clear();
        normals_.clear();
        boundary_map_.clear();
    }

    /**
     * Remove nodes with given indices.
     */
    void clearNodes(const Range<int>& to_remove) {
        int n = size();
        Range<bool> remove_mask(n, false);
        remove_mask[to_remove] = true;
        Range<int> remove_normals;
        for (int i = 0; i < n; ++i) {
            if (boundary_map_[i] >= 0 && remove_mask[i]) {  // boundary node that has to be removed
                remove_normals.push_back(boundary_map_[i]);
            } else if (boundary_map_[i] >= 0) {
                boundary_map_[i] -= remove_normals.size();
            }
        }
        normals_[remove_normals].remove();
        boundary_map_[to_remove].remove();
        types[to_remove].remove();
        positions[to_remove].remove();
        if (support.size() == n) support[to_remove].remove();
        if (distances.size() == n) distances[to_remove].remove();
    }

    /// Clears internal nodes
    void clearInternalNodes() {
        clearNodes(types > 0);
    }

    /// Clears boundary nodes
    void clearBoundaryNodes() {
        clearNodes(types < 0);
    }

    /**
     * Guesses appropriate precision for contains tests.
     * Our guess is
     * @f$\left(\displaystyle\frac{V(T)}{\mathrm{num\_of\_points}}\right)^{\frac{1}{\mathrm{dim}}}*\varepsilon@f$
     * where @f$V(T)@f$ is the volume of the object of discretization, @f$\mathrm{dim}@f$
     * is its
     * dimension and @f$\varepsilon@f$ is standard error tolerance, eg. 1e-6.
     * For sphere the values are @f$V(T) = 4 \pi r^2@f$ and @f$\mathrm{dim} = 2@f$.
     * Above is calculated for boundary and interior of the domain and the minimum is
     * taken. <b>If domain contains any children, this guesses may be very bad!</b>
     */
    void guessSetContainsPrecision() {
        int internal_count = (types > 0).size();
        int boundary_count = (types < 0).size();
        if (internal_count == 0 && boundary_count == 0) contains_precision = EPS;
        scalar_t precision_interior = std::numeric_limits<scalar_t>::infinity();
        scalar_t precision_boundary = std::numeric_limits<scalar_t>::infinity();

        if (internal_count > 0) {
            precision_interior = EPS * std::pow(volume() / internal_count, 1.0 / vec_t::dimension);
        }
        if (vec_t::dimension > 1 && boundary_count > 0) {
            precision_boundary = EPS * std::pow(surface_area() / boundary_count,
                                                1.0 / (vec_t::dimension - 1));
        }

        contains_precision = std::min(precision_boundary, precision_interior);
        if (std::isinf(contains_precision)) contains_precision = EPS;
    }
    /// Sets contains precision. @sa contains
    void setContainsPrecision(scalar_t precision) { contains_precision = precision; }
    /// Returns contains precision. @sa contains
    scalar_t getContainsPrecision() const { return contains_precision; }

    /// Returns thickness of the domain
    THICKNESS getThickness() const { return thickness; }

    /**
     * Calculates characteristic distance. This is the same order of magnitude as
     * distance between two neighbouring points if they were distributed evenly.
     * The formula is
     * @f$ \chi_r = \left(\frac{V(T)}{num\_of\_points}\right)^{\frac{1}{dim}} @f$
     */
    scalar_t characteristicDistance() const {
        return std::pow(volume() / size(), 1.0 / vec_t::dimension);
    }

    /// Returns boundary box of a domain.
    const std::pair<vec_t, vec_t>& getBBox() const { return bbox; }

    /// Outputs a simple report about out domain, like number of nodes, support size.
    std::ostream& output_report(std::ostream& os = std::cout) const {
        int internal_count = (types > 0).size();
        int boundary_count = (types < 0).size();
        size_t mem = sizeof(*this);
        size_t mem_pos = mem_used(positions);
        size_t mem_types = mem_used(types);
        size_t mem_supp = mem_used(support);
        size_t mem_dist = mem_used(distances);
        int max_support_size = -1;
        for (int i = 0; i < support.size(); ++i) {
            max_support_size = std::max(max_support_size, support[i].size());
            mem_supp += mem_used(support[i]);
        }
        size_t mem_bnd_map = mem_used(boundary_map_);
        size_t mem_normals = mem_used(normals_);
        size_t mem_total = mem + mem_pos + mem_types + mem_supp + mem_dist + mem_bnd_map +
                mem_normals;
        os << "    dimension: " << vec_t::dimension << '\n'
           << "    number of points: " << size() << " of which " << internal_count
           << " internal and " << boundary_count << " boundary\n"
           << "    max support size: ";
        if (max_support_size == -1) os << "support not found yet";
        else os << max_support_size;
        os << "\n    children: " << child_domains.size() << "\n"
           << "    characteristic distance: " << characteristicDistance() << '\n'
           << "    mem used total: " << mem2str(mem_total) << '\n'
           << "        pos & types:   " << mem2str(mem_pos + mem_types) << '\n'
           << "        supp & dist:   " << mem2str(mem_supp + mem_dist) << '\n'
           << "        bnd & normals: " << mem2str(mem_bnd_map + mem_normals) << '\n';
        return os;
    }

    /// Returns surface area of base domain, paying no attention to children.
    virtual scalar_t surface_area() const = 0;
    /// Returns volume of base domain, paying no attention to children.
    virtual scalar_t volume() const = 0;

    // ENGINE PLUGINS
    /// Calls a given function and supplies any additional arguments to it.
    template<typename callable_t, typename... Args>
    auto apply(callable_t& callable, Args&&... args)
            -> decltype(callable(*this, std::forward<Args>(args)...)) {
        return callable(*this, std::forward<Args>(args)...);
    }
    /// Calls a given function and supplies any additional arguments to it.
    template<typename callable_t, typename... Args>
    auto apply(const callable_t& callable, Args&&... args)
            -> decltype(callable(*this, std::forward<Args>(args)...)) {
        return callable(*this, std::forward<Args>(args)...);
    }

    // FUNCTIONS IMPLEMENTED IN DOMAIN EXTENSION
    void findSupport(int support_size);
    void findSupport(int support_size, const Range<int>& for_which);
    void findSupport(int support_size, const Range<int>& for_which, const Range<int>& search_among,
                     bool force_self = false);
    void findSupport(scalar_t radius_squared, int max_support_size);
    void findSupport(scalar_t radius_squared, int max_support_size, const Range<int>& for_which);
    void findSupport(scalar_t radius_squared, int max_support_size, const Range<int>& for_which,
                     const Range<int>& search_among, bool force_self = false);
    void findSupport(const KDTree<vec_t>& tree, scalar_t radius_squared, int max_support_size,
                     const Range<int>& for_which, const Range<int>& search_among,
                     bool force_self = false);
    std::pair<bool, vec_t> projectPointToBoundary(const vec_t& point, vec_t unit_normal,
                                                  scalar_t precision = EPS) const;

  protected:  // any variable added here should also be copied in the clone function
    /**
     * Normal vector pointing towards nodes.
     * - true = nodes inside
     * - false = nodes outside
     */
    NORMAL normal_vector;
    THICKNESS thickness;  ///< Is the domain thick or thin?
    std::pair<vec_t, vec_t> bbox;  ///< Bounding box. Children should set it in their constructor.
    /**
     * Margin of error for contains tests.
     * @sa contains
     * @sa guessSetContainsPrecision
     * @sa setContainsPrecision
     * @sa getContainsPrecision
     */
    scalar_t contains_precision;

    /// Clones base class variables into domain d.
    void clone_base(Domain* d) const {
        d->positions = positions;
        d->types = types;
        d->support = support;
        d->distances = distances;
        d->normals_ = normals_;
        d->boundary_map_ = boundary_map_;
        d->normal_vector = normal_vector;
        d->contains_precision = contains_precision;
        d->thickness = thickness;
        d->bbox = bbox;
        d->child_domains.reserve(child_domains.size());
        for (const auto& p : child_domains) {
            d->child_domains.push_back(std::unique_ptr<Domain>(p->clone()));
        }
    }

  private:
    /// Sets normal vector to outside and flip child normals.
    void setObstacleNormal() {
        if (normal_vector == INSIDE) flipNormal();
    }
    /// Sets normal vector to inseide and preserve child normals.
    void setUnionNormal() {
        if (normal_vector == OUTSIDE) flipNormal();
    }
    /// Flip normal from inside to outside and vice versa, including children
    void flipNormal() {
        normal_vector = (normal_vector == INSIDE) ? OUTSIDE : INSIDE;
        for (const auto& d : child_domains) d->flipNormal();
    }
    /// Sets all domains thickness
    void setThick() {
        if (thickness == THIN) thickness = THICK;
        for (const auto& d : child_domains) d->setThick();
    }
    /// Sets all domains thin
    void setThin() {
        if (thickness == THICK) thickness = THIN;
        for (const auto& d : child_domains) d->setThin();
    }
    /**
     * A pure virtual function inherited classes need to implement to
     * test is a point lies in a domain, paying no respect to obstacles.
     * It is used by `contains` function to test if a point lies inside the
     * area of a given geometric shape, e.g. circle or rectangle. They should take
     * into account the normal vector - making domain include the boundary for EPS constant.
     */
    virtual bool inside(const vec_t& point) const = 0;

    /**
     * A clone function to be overridden by subclasses and copy their own data.
     */
    virtual Domain* clone() const = 0;
};

/// Print basic info about the domain.
template <class vec_t>
std::ostream& operator<<(std::ostream& os, const Domain<vec_t>& d) {
    os << "Domain:\n";
    d.output_report(os);
    return os;
}

/**
 * @brief Class to hold information about Rectangle domains.
 * Domain is a vec_t::dimension cuboid between beg and end points.
 */
template <class vec_t>
class RectangleDomain : public Domain<vec_t> {
  public:  // any variable added here should also be copied in the clone function
    vec_t beg;  ///< Start point of a domain.
    vec_t end;  ///< End point of a domain.
    using Domain<vec_t>::positions;
    using Domain<vec_t>::types;
    using Domain<vec_t>::clearBoundaryNodes;
    using Domain<vec_t>::clearInternalNodes;
    using Domain<vec_t>::addPoint;
    using Domain<vec_t>::addBoundaryPoint;
    using Domain<vec_t>::guessSetContainsPrecision;
    using typename Domain<vec_t>::scalar_t;

    /// Allow default constructor
    RectangleDomain() {}
    /// Allow move constructor
    RectangleDomain(RectangleDomain<vec_t>&&) = default;
    /// Allow move assignment
    RectangleDomain<vec_t>& operator=(RectangleDomain<vec_t>&&) = default;
    /// Remove copy constructor
    RectangleDomain(const RectangleDomain<vec_t>&) = delete;
    /// Remove copy assignment
    RectangleDomain<vec_t>& operator=(const RectangleDomain<vec_t>&) = delete;

    /**
     * Creates a rectangular domain. The dimensions of both points must be the same.
     * @param beg_ A starting point for a domain.
     * @param end_ An ending point for a domain.
     */
    RectangleDomain(const vec_t& beg_, const vec_t& end_) : beg(beg_), end(end_) {
        for (int i = 0; i < vec_t::dimension; ++i) {
            assert_msg(beg[i] != end[i], "Rectangle domain has no area, sides in dimension %d "
                       "have same coordinate %g.", i, beg[i]);
            if (beg[i] > end[i]) std::swap(beg[i], end[i]);
        }
        bbox = {beg, end};
    }
    /**
     * Overload for unambiguous RectangleDomain({1.2, 2.1}, {-2.3, 4.56});
     */
    RectangleDomain(std::initializer_list<scalar_t> beg, std::initializer_list<scalar_t> end)
            : RectangleDomain<vec_t>(vec_t(beg), vec_t(end)) {
        assert_msg(beg.size() == vec_t::dimension,
                   "Expected begin initializer list size to be "
                   "equal to domain dimension = %d, but got %d.",
                   vec_t::dimension, beg.size());
        assert_msg(end.size() == vec_t::dimension,
                   "Expected begin initializer list size to be "
                   "equal to domain dimension = %d, but got %d.",
                   vec_t::dimension, end.size());
    }

    /**
     * @brief Fills boundary and interior uniformly by supplying node counts.
     * @sa fillUniformInterior
     * @sa fillUniformBoundary
     **/
    void fillUniform(const Vec<int, vec_t::dimension>& internal_counts,
                     const Vec<int, vec_t::dimension>& boundary_counts) {
        fillUniformInterior(internal_counts);
        fillUniformBoundary(boundary_counts);
    }
    /**
     * @brief Fills boundary and interior uniformly by supplying spatial step.
     * @sa fillUniformInterior
     * @sa fillUniformBoundary
     **/
    void fillUniformWithStep(const vec_t& internal_steps, const vec_t& boundary_steps) {
        fillUniformInteriorWithStep(internal_steps);
        fillUniformBoundaryWithStep(boundary_steps);
    }

    /**
     * @brief Uniformly discretizes underlying domain interior with counts[i] points
     * in dimension i. It fills positions with appropriate coordinates and types with 1s.
     * See test_domain.hpp for examples. Similar to numpy's linspace and Matlab's
     * meshgrid.
     * @param counts how many discretization points to use in each dimension.
     */
    void fillUniformInterior(const Vec<int, vec_t::dimension>& counts) {
        clearInternalNodes();
        for (const vec_t& v : linspace(beg, end, counts, false)) {
            addPoint(v, NODE_TYPE::DEFAULT_INTERNAL);
        }
        guessSetContainsPrecision();
    }

    /**
     * @brief Uniformly discretizes underlying domain interior by calling fillUniformInterior with
     * appropriate counts.
     * @param steps what discretization step to use in each dimension
     */
    void fillUniformInteriorWithStep(const vec_t& steps) {
        for (auto step : steps)
            assert(step > EPS &&
                   "Step in interior discretization of rectangle domain is too small!");
        Vec<int, vec_t::dimension> counts;
        for (int i = 0; i < vec_t::dimension; ++i)
            counts[i] = static_cast<int>(std::ceil((end[i] - beg[i]) / steps[i])) - 1;
        fillUniformInterior(counts);
    }
    /**
     * @brief Uniformly discretizes underlying domain boundary. It fills positions
     * with appropriate coordinates and types with -1s. Dimensions higher than 3 are
     * currently unsupported.
     * @param counts how many discretization points to use in each dimension.
     */
    void fillUniformBoundary(const Vec<int, vec_t::dimension>& counts) {
        static_assert(vec_t::dimension <= 3,
                      "Discretization of surfaces of cuboids of dimension >=4 is "
                      "currently not supported.");
        clearBoundaryNodes();
        for (auto x : counts) {
            assert_msg(x >= 2, "All counts must be greater than 2, got counts = %s", counts);
        }
        if (vec_t::dimension == 1) {
            addBoundaryPoint(beg, NODE_TYPE::DEFAULT_BOUNDARY, vec_t(-1));
            addBoundaryPoint(end, NODE_TYPE::DEFAULT_BOUNDARY, vec_t(1));
        } else if (vec_t::dimension == 2) {
            Vec<scalar_t, 1> subbeg; subbeg[0] = beg[0];
            Vec<scalar_t, 1> subend; subend[0] = end[0];
            Vec<int, 1> subcounts; subcounts[0] = counts[0]-2;
            for (const Vec<scalar_t, 1>& c : linspace(subbeg, subend, subcounts, {false})) {
                vec_t v; v[0] = c[0]; v[1] = beg[1];
                addBoundaryPoint(v, NODE_TYPE::DEFAULT_BOUNDARY, {0, -1});
                v[1] = end[1];
                addBoundaryPoint(v, NODE_TYPE::DEFAULT_BOUNDARY, {0, 1});
            }
            subbeg[0] = beg[1]; subend[0] = end[1]; subcounts[0] = counts[1]-2;
            for (const Vec<scalar_t, 1>& c : linspace(subbeg, subend, subcounts, {false})) {
                vec_t v; v[0] = beg[0]; v[1] = c[0];
                addBoundaryPoint(v, NODE_TYPE::DEFAULT_BOUNDARY, {-1, 0});
                v[0] = end[0];
                addBoundaryPoint(v, NODE_TYPE::DEFAULT_BOUNDARY, {1, 0});
            }
            addBoundaryPoint(beg,  NODE_TYPE::DEFAULT_BOUNDARY, vec_t(-1, -1).normalized());
            addBoundaryPoint(end,  NODE_TYPE::DEFAULT_BOUNDARY, vec_t(1, 1).normalized());
            addBoundaryPoint({beg[0], end[1]}, NODE_TYPE::DEFAULT_BOUNDARY,
                             vec_t(-1, 1).normalized());
            addBoundaryPoint({end[0], beg[1]}, NODE_TYPE::DEFAULT_BOUNDARY,
                             vec_t(1, -1).normalized());
        } else if (vec_t::dimension == 3) {  // TODO(jureslak): fix normals on edges in 3D
            Vec<scalar_t, 2> subbeg; subbeg[0] = beg[0]; subbeg[1] = beg[1];
            Vec<scalar_t, 2> subend; subend[0] = end[0]; subend[1] = end[1];
            Vec<int, 2> subcounts; subcounts[0] = counts[0]; subcounts[1] = counts[1];
            for (const Vec<scalar_t, 2>& c : linspace(subbeg, subend, subcounts, {true, true})) {
                vec_t v; v[0] = c[0]; v[1] = c[1]; v[2] = beg[2];
                addBoundaryPoint(v, NODE_TYPE::DEFAULT_BOUNDARY, {0, 0, -1});
                v[2] = end[2];
                addBoundaryPoint(v, NODE_TYPE::DEFAULT_BOUNDARY, {0, 0, 1});
            }
            subbeg[1] = beg[2]; subend[1] = end[2]; subcounts[1] = counts[2]-2;
            for (const Vec<scalar_t, 2>& c : linspace(subbeg, subend, subcounts, {true, false})) {
                vec_t v; v[0] = c[0]; v[1] = beg[1]; v[2] = c[1];
                addBoundaryPoint(v, NODE_TYPE::DEFAULT_BOUNDARY, {0, -1, 0});
                v[1] = end[1];
                addBoundaryPoint(v, NODE_TYPE::DEFAULT_BOUNDARY, {0, 1, 0});
            }
            subbeg[0] = beg[1]; subend[0] = end[1]; subcounts[0] = counts[1]-2;
            for (const Vec<scalar_t, 2>& c : linspace(subbeg, subend, subcounts, {false, false})) {
                vec_t v; v[0] = beg[0]; v[1] = c[0]; v[2] = c[1];
                addBoundaryPoint(v, NODE_TYPE::DEFAULT_BOUNDARY, {-1, 0, 0});
                v[0] = end[0];
                addBoundaryPoint(v, NODE_TYPE::DEFAULT_BOUNDARY, {1, 0, 0});
            }
        }
        guessSetContainsPrecision();
    }
    /**
     * @brief Uniformly discretizes underlying domain boundary by calling
     * fillUniformBoundary with appropriate counts.
     * @param steps Which spatial step to use in each dimension.
     */
    void fillUniformBoundaryWithStep(const vec_t& steps) {
        for (auto step : steps) {
            assert_msg(step > EPS, "Step in fillBoundary of rectangle domain is too small.");
        }
        Vec<int, vec_t::dimension> counts;
        for (int i = 0; i < vec_t::dimension; ++i)
            counts[i] = static_cast<int>(std::ceil((end[i] - beg[i]) / steps[i])) + 1;
        fillUniformBoundary(counts);
    }

    /// Fills boundary with given distribution.
    template <typename func_t>
    void fillBoundaryWithDistribution(const func_t& dp) {
        static_assert(vec_t::dimension== 2, "only available in 2 dimensions");
        const int TOP = -1;
        const int RIGHT = -2;
        const int BOTTOM = -3;
        const int LEFT = -4;

        vec_t point(beg[0], end[1]);
        point[0] += dp(point);
        while (point[0] < end[0]) {
            addBoundaryPoint(point, TOP, vec_t(0, 1));
            point[0] += dp(point);
        }
        point = {end[0], end[1]};
        point[1] -= dp(point);
        while (point[1] > beg[1]) {
            addBoundaryPoint(point, RIGHT, vec_t(1, 0));
            point[1] -= dp(point);
        }
        point = {end[0], beg[1]};
        point[0] -= dp(point);
        while (point[0] > beg[0]) {
            addBoundaryPoint(point, BOTTOM, vec_t(0, -1));
            point[0] -= dp(point);
        }
        point = {beg[0], beg[1]};
        point[1] += dp(point);
        while (point[1] < 0) {
            addBoundaryPoint(point, LEFT, vec_t(-1, 0));
            point[1] += dp(point);
        }
    }
    /**
     * @brief Fills boundary and interior randomly with uniform distribution.
     * @sa fillRandomInterior
     * @sa fillRandomBoundary
     **/
    void fillRandom(int internal_count, int boundary_count) {
        fillRandomInterior(internal_count);
        fillRandomBoundary(boundary_count);
    }
    /// Randomly fills cuboid interior with points.
    void fillRandomInterior(int num_of_points) {
        clearInternalNodes();  // http://mathworld.wolfram.com/BallPointPicking.html
        std::mt19937 generator(get_seed());
        std::uniform_real_distribution<scalar_t> uniform(0, 1);
        for (int i = 0; i < num_of_points; ++i) {
            vec_t x;
            for (int j = 0; j < vec_t::dimension; ++j)
                x[j] = beg[j] + uniform(generator) * (end[j] - beg[j]);
            addPoint(x, NODE_TYPE::DEFAULT_INTERNAL);
        }
        guessSetContainsPrecision();
    }
    /// Randomly fills cuboid boundary with points.
    void fillRandomBoundary(int num_of_points) {
        static_assert(vec_t::dimension <= 3,
                      "Random point picking of surfaces of cuboids of dimension >=4 is "
                      "currently not supported.");
        clearBoundaryNodes();
        if (vec_t::dimension == 1) {
            for (int i = 0; i < num_of_points; ++i) {
                std::mt19937 generator(get_seed());
                int choice = random_choice(
                        Range<int>{0, 1}, Range<double>{0.5, 0.5}, true, generator);
                if (choice) {
                    addBoundaryPoint(beg, NODE_TYPE::DEFAULT_BOUNDARY, {-1.});
                } else {
                    addBoundaryPoint(end, NODE_TYPE::DEFAULT_BOUNDARY, {1.});
                }
            }
        } else if (vec_t::dimension == 2) {
            std::mt19937 generator(get_seed());
            std::uniform_real_distribution<scalar_t> uniform(0, 1);
            Range<bool> elem = {false, true};
            vec_t diff = end - beg;
            Range<scalar_t> weights = {std::abs(diff[0]), std::abs(diff[1])};
            vec_t point;
            for (int i = 0; i < num_of_points; ++i) {
                bool choice = random_choice(elem, weights, false, generator);
                bool first = uniform(generator) < 0.5;
                if (choice) {  // x-axis random
                    if (first) {  // lower side
                        point = {beg[0] + uniform(generator) * diff[0], beg[1]};
                        addBoundaryPoint(point, NODE_TYPE::DEFAULT_BOUNDARY, {0., -1.});
                    } else {
                        point = {beg[0] + uniform(generator) * diff[0], end[1]};
                        addBoundaryPoint(point, NODE_TYPE::DEFAULT_BOUNDARY, {0., 1.});
                    }
                } else {  // y-axis random
                    if (first) {  // left side
                        point = {beg[0], beg[1] + uniform(generator) * diff[1]};
                        addBoundaryPoint(point, NODE_TYPE::DEFAULT_BOUNDARY, {-1., 0.});
                    } else {
                        point = {end[0], beg[1] + uniform(generator) * diff[1]};
                        addBoundaryPoint(point, NODE_TYPE::DEFAULT_BOUNDARY, {1., 0.});
                    }
                }
            }
        } else if (vec_t::dimension == 3) {
            std::mt19937 generator(get_seed());
            std::uniform_real_distribution<scalar_t> uniform(0, 1);
            Range<int> elem = {0, 1, 2};
            vec_t diff = end - beg;
            Range<scalar_t> weights = {std::abs(diff[0] * diff[1]), std::abs(diff[0] * diff[2]),
                                       std::abs(diff[1] * diff[2])};
            for (int i = 0; i < num_of_points; ++i) {
                int choice = random_choice(elem, weights, false, generator);
                bool first = uniform(generator) < 0.5;
                vec_t point;
                scalar_t a = beg[0] + uniform(generator) * diff[0];  // random sides
                scalar_t b = beg[1] + uniform(generator) * diff[1];
                scalar_t c = beg[2] + uniform(generator) * diff[2];
                if (choice == 0) {  // xy
                    if (first) {
                        addBoundaryPoint({a, b, beg[2]}, NODE_TYPE::DEFAULT_BOUNDARY, {0, 0, -1});
                    } else {
                        addBoundaryPoint({a, b, end[2]}, NODE_TYPE::DEFAULT_BOUNDARY, {0, 0, 1});
                    }
                } else if (choice == 1) {  // xz
                    if (first) {
                        addBoundaryPoint({a, beg[1], c}, NODE_TYPE::DEFAULT_BOUNDARY, {0, -1, 0});

                    } else {
                        addBoundaryPoint({a, end[1], c}, NODE_TYPE::DEFAULT_BOUNDARY, {0, 1, 0});
                    }
                } else {  // yz
                    if (first) {
                        addBoundaryPoint({beg[0], b, c}, NODE_TYPE::DEFAULT_BOUNDARY, {-1, 0, 0});
                    } else {
                        addBoundaryPoint({end[0], b, c}, NODE_TYPE::DEFAULT_BOUNDARY, {1, 0, 0});
                    }
                }
            }
        }
        guessSetContainsPrecision();
    }

    scalar_t volume() const override {
        vec_t diff = end - beg;
        return std::abs(
            std::accumulate(std::begin(diff), std::end(diff), 1.0, std::multiplies<scalar_t>()));
    }
    scalar_t surface_area() const override {
        vec_t diff = (end - beg).cwiseAbs();
        scalar_t vol = volume();
        scalar_t pov = 0;
        for (scalar_t x : diff)
            if (x != 0) pov += vol / x;
        return 2 * pov;
    }

  protected:
    using Domain<vec_t>::bbox;
    using Domain<vec_t>::contains_precision;
    using Domain<vec_t>::normal_vector;
    using Domain<vec_t>::thickness;

  private:
    /**
     * @brief vec_test if a point is contained in a basic domain, paying
     * no notice to subdomains. A point is inside a cuboid if every of
     * its coordinates lies in the interval defined with corresponding
     * begin and end coordinates. An EPS margin pointing away from normal is used.
     * @param point The point we are interested in,
     * @return True if point is contqined in the basic domain
     * and false otherwise.
     */
    bool inside(const vec_t& point) const override {
        scalar_t margin = contains_precision * thickness * normal_vector;
        for (int i = 0; i < vec_t::dimension; ++i) {
            bool inside_case_1 = (beg[i] - margin < point[i] && point[i] < end[i] + margin);
            bool inside_case_2 = (end[i] - margin < point[i] && point[i] < beg[i] + margin);
            if (!(inside_case_1 || inside_case_2)) return normal_vector == OUTSIDE;
        }
        return normal_vector == INSIDE;
    }
    /**
     * Return a new RectangleDomain, which is a copy of this one.
     */
    RectangleDomain* clone() const override {
        auto p = new RectangleDomain(beg, end);
        this->clone_base(p);
        return p;
    }
};

/// Print basic info about the domain.
template <class vec_t>
std::ostream& operator<<(std::ostream& os, const RectangleDomain<vec_t>& d) {
    os << "RectangleDomain:\n"
       << "    beg: " << d.beg << "\n"
       << "    end: " << d.end << "\n";
    return d.output_report(os);
}

/**
 * @brief Class to hold information about circular domains of dimension
 * vec_t::dimension.
 * For dimension 1 this is effectively the same as RectangleDomain.
 */
template <class vec_t>
class CircleDomain : public Domain<vec_t> {
  public:  // any variable added here should also be copied in the clone function
    using Domain<vec_t>::positions;
    using Domain<vec_t>::types;
    using Domain<vec_t>::clearBoundaryNodes;
    using Domain<vec_t>::clearInternalNodes;
    using Domain<vec_t>::addPoint;
    using Domain<vec_t>::addBoundaryPoint;
    using Domain<vec_t>::guessSetContainsPrecision;
    using typename Domain<vec_t>::scalar_t;  ///< Numeric data type.
    vec_t center;  ///< Center of a circle.
    scalar_t radius;  ///< Radius of a circle.

    /// Allow default constructor
    CircleDomain() {}
    /// Allow move constructor
    CircleDomain(CircleDomain<vec_t>&&) = default;
    /// Allow move assignment
    CircleDomain<vec_t>& operator=(CircleDomain<vec_t>&&) = default;
    /// Remove copy constructor
    CircleDomain(const CircleDomain<vec_t>&) = delete;
    /// Remove copy assignment
    CircleDomain<vec_t>& operator=(const CircleDomain<vec_t>&) = delete;

    /**
     * Create a circle domain around a center with given radius. It
     * automatically generates discretization points on the edge.
     * Parameter can be omitted as is suitable for one dimension.
     * @param center_ Center of a circle.
     * @param radius_ Radius of a circle.
    */
    CircleDomain(const vec_t& center_, scalar_t radius_) : center(center_), radius(radius_) {
        assert_msg(radius > 0, "Circle radius must be greater than 0, got %f.", radius);
        bbox = {center - vec_t(radius), center + vec_t(radius)};
    }
    /**
     * @brief Fills boundary and interior approximately uniformly.
     * @sa fillUniformInterior
     * @sa fillUniformBoundary
     **/
    void fillUniform(int internal_count, int boundary_count) {
        fillUniformInterior(internal_count);
        fillUniformBoundary(boundary_count);
    }
    /**
     * @brief
     * Adds uniform discretization points to positions and type arrays. For one
     * dimension the constructor parameter is ignored, as the edge contains only
     * two points, for 2 dimensions it is generated uniformly according to the
     * angle. For 3 dimensions an approximation using Fibonacci spheres is used.
     * Higher dimensions are currently unsupported.
     * @param num_of_points Number of points to fill the boundary with.
     */
    void fillUniformBoundary(int num_of_points) {
        static_assert(vec_t::dimension <= 3,
                      "Discretization of surfaces of spheres of dimension >=4 is "
                      "currently not supported.");
        clearBoundaryNodes();
        if (vec_t::dimension == 1) {  // always only two points
            addBoundaryPoint({center[0] - radius}, NODE_TYPE::DEFAULT_BOUNDARY, vec_t(-1));
            addBoundaryPoint({center[0] + radius}, NODE_TYPE::DEFAULT_BOUNDARY, vec_t(1));
        } else if (vec_t::dimension == 2) {
            // normal uniform distribution by angle
            scalar_t dfi = 2 * M_PI / num_of_points;
            for (int i = 0; i < num_of_points; ++i) {
                vec_t normal = {std::cos(i * dfi), std::sin(i * dfi)};
                addBoundaryPoint(center + radius * normal, NODE_TYPE::DEFAULT_BOUNDARY, normal);
            }
        } else if (vec_t::dimension == 3) {  // Fibonacci spheres
            // code from http://stackoverflow.com/questions/9600801
            // explanation: http://blog.marmakoide.org/?p=1
            scalar_t offset = 2.0 / num_of_points;
            scalar_t increment = M_PI * (3.0 - std::sqrt(5));
            scalar_t x, y, z, r, phi;
            for (int i = 0; i < num_of_points; ++i) {
                y = ((i * offset) - 1) + (offset / 2);
                r = std::sqrt(1 - y * y);
                phi = i * increment;
                x = std::cos(phi) * r;
                z = std::sin(phi) * r;
                addBoundaryPoint(center + radius * vec_t({x, y, z}),
                                 NODE_TYPE::DEFAULT_BOUNDARY, vec_t({x, y, z}));
            }
        }
        guessSetContainsPrecision();
    }
    /**
     * Fills boundary of a 2D circle at an arc length of every `step` units.
     * This is the same as calling fillUniformBoundary(ceil(2*pi*radius / step));
     **/
    void fillUniformBoundaryWithStep(double step) {
        static_assert(vec_t::dimension == 2,
                      "Boundary filling with step for circles is supported for 2 "
                      "dimensions only.");
        fillUniformBoundary(static_cast<int>(std::ceil(2 * M_PI * radius / step)));
    }
    /**
     * @brief
     * Fills domain interior with approximately uniformly distributed nodes.
     * For one dimension this is simple linspace, for 2 dimensions we use
     * Fibonacci spiral. Default type of nodes is 1.
     * Higher dimensions are currently unsupported.
     * @param num_of_points Number of points to fill the area with.
     */
    void fillUniformInterior(int num_of_points) {
        static_assert(vec_t::dimension <= 3,
                      "Uniform discretization of volumes of spheres of dimension >=4 is "
                      "currently not supported.");
        clearInternalNodes();
        if (vec_t::dimension == 1) {
            for (const vec_t& v : linspace(bbox.first, bbox.second, {num_of_points}, false)) {
                addPoint(v, NODE_TYPE::DEFAULT_INTERNAL);
            }
        } else if (vec_t::dimension == 2) {  // Fill circle using Fibonacci spiral.
            scalar_t golden_angle = M_PI * (3 - std::sqrt(5));
            scalar_t sqrt_num_of_points = std::sqrt(num_of_points);
            for (int i = 0; i < num_of_points; ++i) {
                scalar_t theta = i * golden_angle;
                scalar_t r = std::sqrt(i) / sqrt_num_of_points;
                addPoint(center + radius * r * vec_t{std::cos(theta), std::sin(theta)},
                         NODE_TYPE::DEFAULT_INTERNAL);
            }
        } else if (vec_t::dimension == 3) {  // Uniform cuboid, taking ones inside
            int count = iceil(std::cbrt(num_of_points * 6 / M_PI)) - 2;
            scalar_t E = count * count * count * M_PI / 6;
            scalar_t R = std::cbrt(E / num_of_points) * radius;
            for (const vec_t& point : linspace(vec_t(center - vec_t(R)), vec_t(center + vec_t(R)),
                                               {count, count, count}, false)) {
                if ((point - center).squaredNorm() < radius * radius) {
                    addPoint(point, NODE_TYPE::DEFAULT_INTERNAL);
                }
            }
        }
        guessSetContainsPrecision();
    }
    /**
     * @brief Fills boundary and interior randomly with uniform distribution.
     * @sa fillRandomInterior
     * @sa fillRandomBoundary
     **/
    void fillRandom(int internal_count, int boundary_count) {
        fillRandomInterior(internal_count);
        fillRandomBoundary(boundary_count);
    }
    /// Randomly fills ball interior with points.
    void fillRandomInterior(int num_of_points) {
        clearInternalNodes();  // http://mathworld.wolfram.com/BallPointPicking.html
        std::mt19937 generator(get_seed());
        std::uniform_real_distribution<scalar_t> uniform(0, 1);
        std::normal_distribution<scalar_t> gauss(0, 1);
        for (int i = 0; i < num_of_points; ++i) {
            vec_t x;
            for (int j = 0; j < vec_t::dimension; ++j) x[j] = gauss(generator);
            scalar_t norm = x.norm();
            if (norm < 1e-6) continue;
            scalar_t u = std::pow(uniform(generator), 1.0 / vec_t::dimension);
            addPoint(center + u * radius / norm * x, NODE_TYPE::DEFAULT_INTERNAL);
        }
        guessSetContainsPrecision();
    }
    /// Randomly fills ball boundary with points.
    void fillRandomBoundary(int num_of_points) {
        clearBoundaryNodes();  // http://mathworld.wolfram.com/SpherePointPicking.html
        std::mt19937 generator(get_seed());
        std::normal_distribution<scalar_t> gauss(0, 1);
        for (int i = 0; i < num_of_points; ++i) {
            vec_t x;
            for (int j = 0; j < vec_t::dimension; ++j) x[j] = gauss(generator);
            scalar_t norm = x.norm();
            if (norm < 1e-6) continue;
            x /= norm;
            addBoundaryPoint(center + radius * x , NODE_TYPE::DEFAULT_BOUNDARY, x);
        }
        guessSetContainsPrecision();
    }

    /// Fills boundary with given distribution.
    template <typename func_t>
    void fillBoundaryWithDistribution(const func_t& dp) {
        static_assert(vec_t::dimension== 2, "only available in 2 dimensions");
        vec_t point = {0, radius};
        double alpha = 0.0;
        double dp0 = dp(point);
        double max_alpha = 2*M_PI - dp0 / radius;
        alpha += dp0 / radius;
        while (alpha < max_alpha) {
            point = {std::cos(alpha), std::sin(alpha)};
            addBoundaryPoint(radius*point, -1, point);
            alpha += dp(radius*point) / radius;
        }
    }

    scalar_t volume() const override {
        switch (vec_t::dimension) {
            case 1: return 2.0 * radius;
            case 2: return M_PI * radius * radius;
            case 3: return 4.0 / 3.0 * M_PI * radius * radius * radius;
            default: return std::pow(std::sqrt(M_PI) * radius, vec_t::dimension) /
                                     std::tgamma(1 + vec_t::dimension / 2.0);
        }
    }
    scalar_t surface_area() const override { return vec_t::dimension * volume() / radius; }

  protected:
    using Domain<vec_t>::bbox;
    using Domain<vec_t>::contains_precision;
    using Domain<vec_t>::normal_vector;
    using Domain<vec_t>::thickness;

  private:
    /**
     * @brief vec_test if a point is contained in the domains circle.
     * A point is inside if @f$\|point - center\| \leq radius@f$
     * @param point The point we are interested in,
     * @return True if point is conatined in the domain circle and false otherwise.
     */
    bool inside(const vec_t& point) const override {
        scalar_t margin = contains_precision * thickness * normal_vector;
        bool inside = (point - center).squaredNorm() <= margin + radius * radius;
        return inside == (normal_vector == INSIDE);
    }
    /**
     * Return a new CircleDomain, which is a copy of this one.
     */
    CircleDomain* clone() const override {
        auto p = new CircleDomain(center, radius);
        this->clone_base(p);
        return p;
    }
};

/// Print basic info about the domain.
template <class vec_t>
std::ostream& operator<<(std::ostream& os, const CircleDomain<vec_t>& d) {
    os << "CircleDomain:\n"
       << "    center: " << d.center << "\n"
       << "    radius: " << d.radius << "\n";
    return d.output_report(os);
}

/**
 * Class representing a loaded domain, that only knows its point and has no
 * analytical boundary, It cannot be relaxed or refined.
 */
template <typename vec_t>
class LoadedDomain : public Domain<vec_t> {
  public:
    using typename Domain<vec_t>::scalar_t;  ///< Numeric data type.

    /**
     * Load domain from file. Domain must be stored in the given folder `folder`
     * as it is were save by HDF5IO::setDomain.
     */
    template <typename hdf5_file>
    static LoadedDomain load(hdf5_file& file, std::string folder) {
        LoadedDomain domain;
        assert_msg(!folder.empty(), "Please specify a folder.");
        if (folder.front() != '/') folder = '/' + folder;
        file.openFolder(folder);
        auto pos = file.getDouble2DArray("pos");
        int n = pos.size();
        int dim = vec_t::dimension;
        domain.positions.resize(n);
        vec_t low = 1e100;
        vec_t high = -1e100;
        for (int i = 0; i < n; ++i) {
            assert_msg(pos[i].size() == dim, "Node %d has invalid number of coordinates: %s, "
                    "expected %d.", i, pos[i], dim);
            for (int j = 0; j < dim; ++j) {
                domain.positions[i][j] = pos[i][j];
                if (pos[i][j] <= low[j]) low[j] = pos[i][j];
                if (pos[i][j] >= high[j]) high[j] = pos[i][j];
            }
        }
        domain.types = file.getIntArray("types");
        int bnd_count = (domain.types < 0).size();
        domain.boundary_map_ = file.getIntArray("bmap");
        assert_msg((domain.boundary_map_ == -1).size() == n - bnd_count, "Number of nodes with "
                "boundary index (%d) in bmap is not the same as number of nodes with "
                "negative type (%d).", (domain.boundary_map_ == -1).size(), n - bnd_count);
        auto normals = file.getDouble2DArray("normals");
        assert_msg(normals.size() == bnd_count, "Number of normals (%d) must be equal to number of "
                "boundary nodes (%d).", normals.size(), bnd_count);
        domain.normals_.resize(normals.size());
        for (int i = 0; i < bnd_count; ++i) {
            assert_msg(normals[i].size() == dim, "Normal %d has invalid number of coordinates: %s, "
                    "expected %d.", i, normals[i], dim);
            for (int j = 0; j < dim; ++j) {
                domain.normals_[i][j] = normals[i][j];
            }
        }
        // boundary map consistency check
        for (int i = 0; i < n; ++i) {
            if (domain.types[i] < 0) {
                assert_msg(0 <= domain.boundary_map_[i] && domain.boundary_map_[i] < bnd_count,
                           "Normals list and boundary map inconsistent. Node %d with type %d "
                           "has boundary index %d, but the list of normals is only %d long.",
                           i, domain.types[i], domain.boundary_map_[i], bnd_count);
            } else {
                assert_msg(domain.boundary_map_[i] == -1, "Boundary map assigned a normal to an "
                        "internal node %d with type %d, bmap[%d] = %d, but should be -1.",
                           i, domain.types[i], i, domain.boundary_map_[i]);
            }
        }
        domain.support.resize(n);
        domain.distances.resize(n);
        domain.bbox = {low, high};
        return domain;
    }

    /// Must not be called.
    scalar_t surface_area() const override {
        throw std::domain_error("You cannot call surface_area on a LoadedDomain.");
    }

    /// Must not be called.
    scalar_t volume() const override {
        throw std::domain_error("You cannot call volume on a LoadedDomain.");
    }

  private:
    /// Must not be called.
    bool inside(const vec_t& /* point */) const override {
        throw std::domain_error("You cannot call inside on a LoadedDomain.");
    }

    /// Copy
    LoadedDomain* clone() const override {
        auto p = new LoadedDomain;
        this->clone_base(p);
        return p;
    }
};

}  // namespace mm

#endif  // SRC_DOMAIN_HPP_
