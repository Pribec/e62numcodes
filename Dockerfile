FROM ubuntu
RUN apt-get update
RUN apt-get install git g++ python cmake doxygen graphviz libboost-dev libsfml-dev libhdf5-serial-dev -y
RUN git clone https://gitlab.com/e62Lab/e62numcodes.git
RUN (cd e62numcodes && ./run_tests.sh)

CMD ["bash"]
