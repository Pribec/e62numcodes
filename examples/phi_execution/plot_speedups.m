(* ::Package:: *)

speedups = Import["speedups_big.txt", "Table"];
data = GroupBy[speedups, #[[1]] &];
leg = Keys[data];
data = Table[Table[Drop[x, {1}], {x, dataset}], {dataset, data}]



speedups = Table[
{idx, time} = Transpose[dataset];
time = time[[1]] / time;
Transpose[{idx, time}],
 {dataset, data}];
fig = ListPlot[speedups, Frame -> True, PlotStyle->Black, FrameStyle-> Black, FrameLabel -> {"number of threads", "speedup relative to first run"}, ImageSize->800, PlotLegends->PointLegend[leg, LegendMarkerSize->10, LegendLabel->"problem size"],
   PlotMarkers->markers, PlotLabel -> Style["Speedup with respect to number of threads for different problem sizes", FontSize->14, FontColor->Black]]
Export["speedups.png", fig]


mark = Disk[{0,0}, 0.01];
cols = ColorData[97]
size = 0.015;
markers = Table[{Graphics[{cols[i], mark}], size}, {i, 1, Length[leg]}];

fig = ListPlot[data, Frame -> True, PlotStyle->Black, FrameStyle-> Black, FrameLabel -> {"number of threads", "time [s]"}, ImageSize->800, PlotLegends->PointLegend[leg, LegendMarkerSize->10, LegendLabel->"problem size"],
   PlotMarkers->markers, PlotLabel -> Style["Time of execution with respect to number of threads for different problem sizes", FontSize->14, FontColor->Black]]
Export["times.png", fig]



