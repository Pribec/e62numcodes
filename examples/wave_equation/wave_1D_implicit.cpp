#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include <domain_fill_engines.hpp>
#include <domain_relax_engines.hpp>
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "types.hpp"
#include "io.hpp"
#include <Eigen/Sparse>
#include <cmath>
#include <omp.h>

using namespace mm;
using namespace Eigen;



int main(int argc, char* argv[]) {
    if (argc < 2) {print_red("Supply parameter file as the second argument.\n"); return 1;}

    XMLloader conf(argv[1]);
    double pi = 3.14159265358979323846;
    double inner_radius = conf.get<double>("params.domain.inner_radius");
    double outer_radius = conf.get<double>("params.domain.outer_radius");
    double dx = conf.get<double>("params.domain.dx");
    double w = conf.get<double>("params.problem.omega");
    double A = conf.get<double>("params.problem.e0");
    double v = conf.get<double>("params.problem.v");
    int n = conf.get<int>("params.mls.n");  // support size
    int m = conf.get<int>("params.mls.m");  // monomial basis of second order, i.e. 6 monomials
    double sigma = conf.get<double>("params.mls.sigma");
    double time = conf.get<double>("params.problem.time");  // time
    double dt = conf.get<double>("params.problem.dt"); // time step
    int t_steps = std::ceil(time / dt);
    // analytic
    double l = outer_radius - inner_radius;
    auto Fn = [=](int n) {
        return (A * std::pow(w,2.0) * std::sqrt(2 * l) / pi * std::pow(-1, n) / n);
    };
    auto kn = [=](int n) {
        return (n * pi / l);
    };
    auto omega_n = [=](int n) {
        return v * kn(n);
    };
    auto Cn = [=](int n) {
        return (-Fn(n) / (std::pow(omega_n(n), 2.0) - std::pow(w,2.0)));
    };

    auto bn=[=](int n) {
        return (A * w / n / pi / omega_n(n) * std::sqrt(2 * l) * std::pow(-1, n) - Cn(n) * w / omega_n(n));
    };

    int n_max =1000;
    auto analytic = [=](double pos,double t) {
        double vsota = 0;
        for (int n = 1; n<n_max; ++n) {
            vsota += Cn(n) * std::sqrt(2 / l) * std::sin(kn(n) * pos);
        }
        vsota *= std::sin(w * t);
        for (int n = 1; n<n_max; ++n) {
            vsota += bn(n) * std::sin(omega_n(n) * t) * std::sqrt(2 / l) * std::sin(kn(n) * pos);
        }
        vsota += pos / l * A * std::sin(w * t);
        return vsota;
    };
    HDF5IO hdf_out;
    std::string hdf_out_filename = conf.get<std::string>("params.output.path");
    hdf_out.openFile(hdf_out_filename, HDF5IO::DESTROY);

    // Prepare domain
    RectangleDomain<Vec1d> domain({inner_radius}, {outer_radius});
    domain.fillUniformWithStep(dx, dx);

    int domain_size = domain.size();
    domain.findSupport(n);

    Range<int> all = domain.types.filter([](Vec1d p) { return true; });
    Range<int> interior = domain.types > 0;
    Range<int> boundary = domain.types < 0;

    // Prepare operators and matrix
    EngineMLS<Vec1d, Monomials, NNGaussians> mls(m, domain.positions, sigma);
    SparseMatrix<double> M(domain_size, domain_size);
//    SparseLU<SparseMatrix<double>> solver;  // system solve
    BiCGSTAB<SparseMatrix<double, RowMajor>> solver;
    M.reserve(Range<int>(domain_size ,n ));
    auto op = make_mlsm(domain, mls, all, false);  // All nodes, including boundary

    Eigen::VectorXd rhs = Eigen::VectorXd::Zero(domain_size); //set empty vector for rhs
    Eigen::VectorXd E0 = Eigen::VectorXd::Zero(domain_size); //0-th step
    Eigen::VectorXd E1 = Eigen::VectorXd::Zero(domain_size); //1-st step

    // set E1
    for (int i : all) {
        double x = domain.positions[i][0];
        E1(i) = analytic(x, dt);
    }

    // Set equation on interior
    for (int i : interior) {
        M.coeffRef(i, i) = 1;
        op.lap(M, i,-(v*v*dt*dt));  // laplace in interior
        rhs(i) = 2*E1(i)-E0(i);
    }

    // Set boundary conditions
    for (int i : boundary) {
        double x = domain.positions[i][0];
        if (x == inner_radius) {
            M.coeffRef(i, i) = 1;  // antenna
//            rhs(i) = A * std::sin(w*dt*2); //step 2
            rhs(i) = 0.0;
        } else if (x == outer_radius) {
            M.coeffRef(i, i) = 1;  // fixed
//            rhs(i) = 0.0;
            rhs(i) = A * std::sin(w*dt*2); //step 2
        } else {
            assert(!"Should not be here.");
        }
    }
    M.makeCompressed();
    solver.compute(M);

    hdf_out.openFolder("/");
    hdf_out.setSparseMatrix("M", M);

    // time stepping
    int tt;
    int t_save=0;
    hdf_out.openFolder("/");
    hdf_out.setDouble2DArray("pos", domain.positions);
    hdf_out.openFolder("/step" + std::to_string(t_save));
    hdf_out.setDoubleAttribute("TimeStep", t_save);
    hdf_out.setDoubleAttribute("time", 0.0);
    hdf_out.setDoubleArray("E", E0);
    ++t_save;

    for (tt = 2; tt < t_steps; ++tt) {
        if (tt%(500)==0) {
            // assigning new time step
            hdf_out.openFolder("/step" + std::to_string(t_save));
            hdf_out.setDoubleAttribute("TimeStep", t_save);
            std::cout<<t_save<<std::endl;
            ++t_save;
        }
        // save
        VecXd E2 = solver.solve(rhs);
        if (tt%(500)==0) {
            hdf_out.setDoubleArray("E", E2);
            hdf_out.setDoubleAttribute("time", dt * tt);
        }
        // time advance
        E0.swap(E1);
        E1.swap(E2);
        // Set equation on interior
        #pragma omp parallel for  schedule(static)
        for (int j = 0; j < interior.size(); ++j) {
            int i = interior[j];
            rhs(i) = 2*E1(i)-E0(i);
        }

        // Set boundary conditions
        for (int i : boundary) {
            double x = domain.positions[i][0];
            if (x == outer_radius) {
                rhs(i) = A * std::sin(w * tt * dt); //step 2
            }
        }

    }
    std::cout << domain_size << std::endl;
}

