/*
[Solution of de Vahl Davis natural convection benchmark test solved with Pressure correction scheme
 execute with parameters from params and adaptive time stepping
]
*/
#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "overseer.hpp"
#include "types.hpp"
#include <omp.h>
#include <Eigen/Sparse>
#include <Eigen/SparseLU>
#include "domain_fill_engines.hpp"
#include "domain_relax_engines.hpp"

#define _MAX_ITER_ 500

using namespace mm;
using namespace Eigen;

typedef Vec2d vec_t;
typedef double scal_t;

Overseer<scal_t, vec_t> O;

int main(int arg_num, char* arg[]) {
    O.timings.addCheckPoint("time_start");
    O(arg_num, arg);

    O.timings.addCheckPoint("domain");
    RectangleDomain<vec_t> domain(O.domain_lo, O.domain_hi);  // domain definition
    domain.fillUniformBoundaryWithStep(O.d_space);
    CircleDomain<vec_t> circ(O.domain_hi/2, 0.01);
    circ.fillUniformBoundaryWithStep(O.d_space[0]);
    circ.types = -2;
    domain.subtract(circ);

    PoissonDiskSamplingFill fill;
    fill.seed(1);
    BasicRelax relax;
    relax.iterations(O.relax_iter_num)
            .initialHeat(O.relax_initial_heat).
                    finalHeat(O.relax_final_heat).
                    projectionType(BasicRelax::PROJECT_BETWEEN_CLOSEST).
                    numNeighbours(3);
    fill.proximity_relax(O.fill_proximity);
    fill(domain, O.d_space[0]);
    relax(domain);

    prn(domain.size());
    size_t N = domain.size();

    Range<int> all = domain.types.filter([](vec_t p) { return true; });
    Range<int> interior = domain.types > 0;
    Range<int> boundary = domain.types < 0;
    Range<int> circle = domain.types == -2;

    double tol = 1e-6;
    Range<int> right = domain.positions.filter([=](const Vec2d& p) { return std::abs(p[0] - O.domain_hi[0]) < tol; }),
            left = domain.positions.filter([=](const Vec2d& p) { return std::abs(p[0] - O.domain_lo[0]) < tol; }),
            top = domain.positions.filter([=](const Vec2d& p) {
        return std::abs(p[1] - O.domain_hi[1]) < tol && std::abs(p[0] - O.domain_hi[0]) > tol
               && std::abs(p[0] - O.domain_lo[0]) > tol;
    }),
            bottom = domain.positions.filter([=](const Vec2d& p) {
        return std::abs(p[1] - O.domain_lo[1]) < tol && std::abs(p[0] - O.domain_hi[0]) > tol &&
               std::abs(p[0] - O.domain_lo[0]) > tol;
    });

    domain.findSupport(O.n, all);
    O.timings.addCheckPoint("setup");

    EngineMLS<vec_t, NNGaussians, NNGaussians> mls
            ({O.sigmaB, O.m},
             O.sigmaW);

    auto op = make_mlsm(domain, mls, all, false);

    O.timings.addCheckPoint("time loop");
    // init state
    Range<vec_t> v_1(domain.size(), 0);
    Range<vec_t> v_2(domain.size(), 0);

    Range<double> T_1(domain.size(), O.T_init);
    Range<double> T_2(domain.size(), O.T_init);

    // boundary conditions
    v_1 = v_2;
    T_2[left] = O.T_cold;
    T_2[right] = O.T_cold;
    T_2[bottom] = O.T_cold;
    T_2[circle] = O.T_hot;
    T_1 = T_2;

    std::chrono::high_resolution_clock::time_point time_1, time_2;

    // Prepare PRESSURE CORRECTION matrix
    SparseLU<SparseMatrix<double>> solver_p;
    //SparseMatrix<double, RowMajor> M_pressure(N + 1, N + 1);
    //Range<int> res(domain.size() + 1, O.n + 1);
    //res[res.size() - 1] = domain.size() + 1;
    SparseMatrix<double, RowMajor> M_pressure(N, N);
    Range<int> res(domain.size(), O.n);

    M_pressure.reserve(res);
    // pressure conditions dp/dn = (v_2 - v_1)*n*rho/dt;
    for (int i : left) op.neumann_implicit(M_pressure, i, domain.normal(i), 1);
    for (int i : right) op.neumann_implicit(M_pressure, i, domain.normal(i), 1);
    for (int i : bottom) op.neumann_implicit(M_pressure, i, domain.normal(i), 1);
    for (int i : circle) op.neumann_implicit(M_pressure, i, domain.normal(i), 1);
    for (int i : top) M_pressure.coeffRef(i, i) = 1; //open boundary
    //Poisson equation
    for (int i : interior) op.lap(M_pressure, i, 1.0);
    M_pressure.makeCompressed();
    SparseMatrix<double> MM(M_pressure);
    solver_p.compute(MM);
    // END OF PRESSURE MATRIX

    // TIME LOOP
    double alpha;
    int step = 0;
    double phy_time = 0;
    do {
        Range<double> v_tmp;
        double v_max, dt;
        //recompute time step every Cu_f steps
        if (step % O.Cu_f == 0 || step == 0) {
            for (auto c : all)
                v_tmp.push_back(std::pow(v_2[c][0] * v_2[c][0] + v_2[c][1] * v_2[c][1], 0.5));
            v_max = *std::max_element(v_tmp.begin(), v_tmp.end());
            dt = O.Cu_max * O.d_space[0] / v_max;
            if (O.dt <= dt) dt = O.dt;
        }
        time_1 = std::chrono::high_resolution_clock::now();
        // Explicit Navier-Stokes computed on whole domain, including boundaries
        // without pressure -- Fraction step
        int i;
        #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < all.size(); ++i) {
            int c = i;//interior[i];
            v_2[c] = v_1[c] + dt * (// fraction step
                    +O.mu / O.rho * op.lap(v_1, c)
                    - op.grad(v_1, c) * v_1[c]
                    + O.g * (1 - O.beta * (T_1[c] - O.T_ref)));
        }

        // Pressure correction
        //VecXd rhs_pressure(N + 1, 0); //Note N+1, +1 stands for regularization equation
        //rhs_pressure(N) = 0; // = 0 part of the regularization equation
        VecXd rhs_pressure(N, 0);
        #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < interior.size(); ++i) {
            int c = interior[i];
            rhs_pressure(c) = O.rho / dt * op.div(v_2, c);
        }

        //for (int c : bottom) rhs_pressure(c) = O.rho / dt * v_2[c].dot(domain.normal(c)); // no slip wall
        for (int c : left) rhs_pressure(c) = O.rho / dt * v_2[c].dot(domain.normal(c)); // no slip wall
        for (int c : right) rhs_pressure(c) = O.rho / dt * v_2[c].dot(domain.normal(c)); // no slip wall
        for (int c : bottom) rhs_pressure(c) = O.rho / dt * v_2[c].dot(domain.normal(c)); // no slip wall
        for (int c : circle) rhs_pressure(c) = O.rho / dt * v_2[c].dot(domain.normal(c)); // no slip wall
        for (int c : top) rhs_pressure(c) = 0; //open boundary

        VecXd solution = solver_p.solve(rhs_pressure);
        // alpha = solution[N];
        VecXd P_c = solution.head(N);

        // veclocity correction due to the pressure correction
        #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < interior.size(); ++i) {
            int c = interior[i];
            v_2[c] -= dt / O.rho * op.grad(P_c, c);
        }
        // force boundary conditions
        v_2[left] = 0;
        v_2[right] = 0;
        v_2[circle] = 0;
        // NS boundary conditions
        for (int i:top) {
            v_2[i] = op.neumann(v_2, i, domain.normal(i), vec_t{0, 0});
            v_2[i][0] = 0;
        }
        v_2[bottom] = 0;

        // heat transport
        #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < interior.size(); ++i) {
            int c = interior[i];
            T_2[c] = T_1[c] + dt * O.lam / O.rho / O.c_p * op.lap(T_1, c) -
                     dt * v_1[c].transpose() * op.grad(T_1, c);
        }

        // heat Neumann condition
        #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < top.size(); ++i) {
            int c = top[i];
            T_2[c] = op.neumann(T_2, c, vec_t{0, -1}, 0.0);
        }

        // time step
        v_1.swap(v_2);
        T_1.swap(T_2);

        // intermediate IO
        if (std::floor(phy_time/O.out_no) > O.out_recordI || phy_time + dt > O.time || phy_time == 0) {
            O.reOpenHDF();
            // assigning new time step
            O.hdf_out.createFolder("/step" + std::to_string(O.out_recordI));
            O.hdf_out.openFolder("/step" + std::to_string(O.out_recordI));
            O.hdf_out.setDoubleAttribute("TimeStep", step);
            // initial output :: nodes, supports and all other static stuff
            if (step == 0) {
                O.hdfWrite("support", domain.support);
                O.hdfWrite("types", domain.types);
                O.hdfWrite("pos", domain.positions);
            }

            //prepare Nusselt
            Range<scal_t> Nu; //normalized Nusselt
            for (auto c : all)
                Nu.push_back((v_2[c][0] * T_2[c] * O.c_p * O.rho / O.lam - op.grad(T_2, c)[0]) *
                             O.height / (O.T_hot - O.T_cold));

            O.hdfWrite("Nu", Nu);
            O.hdfWrite("v", v_2);
            O.hdfWrite("T", T_2);
            O.hdfWrite("time", phy_time);
            O.closeHDF();
            O.out_recordI++;
        }
        // on screen reports
        if ((O.on_screen > 0 && std::chrono::duration<double>(time_1 - time_2).count() > O.on_screen)
            || step == O.t_steps - 1 || step == 0) {
            double time_elapsed = (O.timings.getTimeToNow("time loop"));
            double time_estimated = time_elapsed * (O.time / phy_time);
            for (auto c : all)
                v_tmp.push_back(std::pow(v_2[c][0] * v_2[c][0] + v_2[c][1] * v_2[c][1], 0.5));
            v_max = *std::max_element(v_tmp.begin(), v_tmp.end());
            Range<scal_t> Nu_x;     // Nu
            for (auto c : left)
                Nu_x.push_back(-op.grad(T_2, c)[0] * O.height /
                               (O.T_hot - O.T_cold));  // Nusselt

            Range<scal_t> div_v(domain.size(), 0);     // divergence

            for (auto c : all) div_v = op.div(v_2, c);
            std::cout.setf(std::ios::fixed, std::ios::floatfield);
            std::cout << std::setprecision(1)
                      << (double) phy_time / O.time * 100 << " % " << std::setprecision(0)
                      << "exe[m]:" << time_elapsed / 60 << std::setprecision(0) << "-"
                      << time_estimated / 60 << " " << std::setprecision(4)
                      << "v_max:" << v_max << " "
                      << std::setprecision(2)
                      //<< " C:" << C << " "
                      << "a:" << alpha << " "
                      //<< " PV:" << static_cast<double> (i_count_cumulative) / (step - step_history + 1) << " "
                      << "div:" << *std::max_element(div_v.begin(), div_v.end()) << " "
                      << "Nu_l:" << std::accumulate(Nu_x.begin(), Nu_x.end(), 0.0) / Nu_x.size() << " "
                      << "Cu:" << dt / O.d_space[0] * v_max << " "
                      << std::setprecision(4)
                      << "dt:" << dt << " "
                      << std::setprecision(1)
                      << "t:" << phy_time << " "
                      << std::setprecision(2)
                      << "D:" << dt / O.d_space[0] / O.d_space[0] * O.mu << std::endl;

            //i_count_cumulative = 0;
            time_2 = time_1;

        }
        phy_time += dt;
        step++;
    } while (phy_time <= O.time);
    O.timings.addCheckPoint("time_end");
    double exec_time =
            std::chrono::duration<double>(O.timings.getTime("time_start", "time_end")).count();
    // DEBUG OUTPUTS
    O.debug_out << "exec_time = " << exec_time << ";\n";
    O.timings.showTimings();
}
