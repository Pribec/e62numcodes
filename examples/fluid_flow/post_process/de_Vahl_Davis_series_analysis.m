% Simple script that runs over all files in given directory 
% and extracts some data from those files. Useful for 
% parameter scans like convergence analysis. 

close all
clear;
working_dir = '../bin/out/';
working_dir = 'Q:/DVD_analyses/out/';

regV={'v', 'p', 'T', 'Nu'};atts={};

out = 'data/';

addpath(working_dir)

files = dir(fullfile(working_dir,'*.m'));
files = {files.name};

i = 1;
for fn = files(1:end);
   f = cell2mat(fn);case_name = f(1:end-2);
   eval(case_name); 
   load_data;
    
   left = pos(1,:) == 0;
   right = pos(1,:) == 1;
   mid_x = pos(1,:) == 0.5;
   mid_y = pos(2,:) == 0.5;

   conv.Ra(i) = Ra;
   conv.N(i) = length(pos);
   conv.t_c(i) = exec_time;
   
   conv.Nu_left_max(i) = max(Nu(left));
   conv.Nu_left_min(i) = min(Nu(left));
   conv.Nu_left_mean(i) = mean(Nu(left));
   
   conv.Nu_right_max(i) = max(Nu(right));
   conv.Nu_right_min(i) = min(Nu(right));
   conv.Nu_right_mean(i)  = mean(Nu(right));
   
   conv.v_mid_x(i) = max(v(1, mid_x))*rho*cp/lam;

   conv.v_mid_y(i) = max(v(2, mid_y))*rho*cp/lam;
   
   conv.fn(i) = {case_name};
   disp(case_name);
   i = i + 1;
end

%% store full data for most accurate computations
cases = unique(conv.Ra);
j = 1;
for Ra_ = cases
  N_max = max(conv.N(conv.Ra == Ra_));
  i = conv.Ra == Ra_ & conv.N == N_max; % finds file name for given Ra with max N
  case_name = cell2mat(conv.fn(i));
  load_data;
  fields.Ra(j) = conv.Ra(i);
  fields.v(j) = {v};
  fields.T(j) = {T};
  fields.Nu(j) = {Nu};
  fields.pos(j) = {pos};
  j = j + 1;
end

save([out,'series_analysis.mat'])