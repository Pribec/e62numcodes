%debug load.
addpath(working_dir);
eval(case_name);

fn=[working_dir,case_name,'.h5'];
info = hdf5info(fn);
no_steps=size(info.GroupHierarchy.Groups,2);
if(~exist('ii','var')); ii=no_steps - 1; end
support = h5read(fn,'/step0/support');
types = h5read(fn,'/step0/types');
pos = h5read(fn,'/step0/pos');

%actual load
step=['/step',num2str(ii),'/'];
for var=regV eval([cell2mat(var),'=','h5read(''',fn,''',''',[step,cell2mat(var)],''');']);end
for var=atts eval( [cell2mat(var),'=','h5readatt(''',fn,''',''',step,''',''',cell2mat(var),''');']);end
 

curr_step = ii;