# CreateXMLFiles.m

creates series of xml input files based on the default.xml. It might come handy when running analyses regarding some parameters, e.g. convergence analysis. 

# run_series.sh

Simple script to execute you favourite executable on all parameters in supplied directory. For example running a solver on series of generated XMLs.

# WIPNO_series_analysis

A Matlab post-process tool for crunching series of computed results.

# WIPNO_plot 

Is a semi-interactive tool for reviewing the computed results. It opens supplied hdf5 file and plots registered analyses. The useful part is that you can "walk" through the folders in hdf, i.e. time steps, simply by pressing left and right arrow keys, while focused on figure windows. Not only the figure, but also the data in workspace is updated. 
