% Collects data from series of simulations 
close all
clear;
input = '../bin/out/';
out = 'data/';

addpath(input)

files = dir(fullfile(input,'*.m'));
files = {files.name};

regV={'var1', 'var1', 'var2'};
atts = {'att1'};

for fn = files;
   f = cell2mat(fn);
   % load debug data example
   eval(f(1:end-2));
   % load from hdf5 example
   case_name=f(1:end-2);
   load_data;

   data.var_1(i) = var1;
   data.var_2(i) = var2;
   data.var_3(i) = var3;

   disp(fn);
end
save([out,'series_analysis.mat'])

%% quick filter of collected data

var_1_s = unique(data.var_1);
var_2_s = unique(data.var_2);
var_3_s = unique(data.var_3);

select = 2; % select field to analyse
fields = {'var_1', 'var_2', 'var_3'};
plot_labels = {'name_1', 'name_2', 'name_3'};

% select at which values from unique list of other 
% fields the analysis is to be done 
var_1_sel = 1;
var_2_sel = 3;
var_3_sel = 2;

S = size(data.var_1);
ii = (data.var_1 == var_1_s(var_1_sel) | (1 == select)*ones(S)) & ...
	 (data.var_2 == var_2_s(var_2_sel) | (2 == select)*ones(S)) & ...
	 (data.var_2 == var_2_s(var_2_sel) | (3 == select)*ones(S));

x_data = eval(['data.',cell2mat(fields(select)),'(ii)']);
y_data = data.desired_field(ii);
f1=createFig('ps', 'm2 1x3 p1','scale',0);hold on;   
plot(x_data, y_data);