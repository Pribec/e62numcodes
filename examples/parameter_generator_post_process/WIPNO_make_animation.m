

close all
clc;clear;

cmap = open('cmap.mat');

fontname = 'Times';
set(0,'defaultaxesfontname',fontname);
set(0,'defaulttextfontname',fontname);

fontsize = 13;
set(0,'defaultaxesfontsize',fontsize);
set(0,'defaulttextfontsize',fontsize);

working_dir='../bin/out/'; addpath(working_dir);
case_name='thermo_fluid_cylinder_TFC_wip_ani';
% case_name='de_Vahl_Davis_explicit_ACM_DVD_Ra1e8_100';

%% PLOT DEFINITIONS
plot_pos        = 1;
    plot_IT     = 1;
    scale       = 3.5;

contour_resolution = [50,50]; level_step = 0.05;
%REGISTERED VARIABLES -- variables to load from HDF5
regV={'v', 'p', 'T', 'Nu'};
atts = {'time'};

load_data;

cold = pos(1,:) == 0;
res = 100;level_steps = 100;

video = VideoWriter('d:\video','MPEG-4');
video.Quality = 50;   
video.FrameRate = 25;
cmap = open('cmap.mat');
createFig('ps', 'm1 2x2 p1', 'scale', 0)
open(video);
for ii = 1:no_steps - 1;
    load_data;
    
    %%plot animation
    [x_v, y_v, u_]=interpolateToMeshGrid(pos(1,:), pos(2,:) ,v(1,:) ,res, res, 'interpolation', 'cubic', 'out', 'mesh'); 
    [x_v, y_v, v_]=interpolateToMeshGrid(pos(1,:), pos(2,:) ,v(2,:) ,res, res, 'interpolation', 'cubic', 'out', 'mesh'); 
    [x_t, y_t, T_]=interpolateToMeshGrid(pos(1,:), pos(2,:) ,T ,res, res, 'interpolation', 'cubic', 'out', 'mesh'); 
        
     subplot(1,2,1);
     contour(x_v,y_v,(u_.^2+v_.^2).^0.5 , 'fill', 'on','levelstep',max(max(v))/level_steps);
       colormap(gca,'jet');
       title('Velocity magnitude');
        
     subplot(1,2,2);
     contour(x_t,y_t,T_, 'fill', 'on', 'levelstep', (max(T)-min(T))/level_steps);
       colormap(gca,cmap.cmap1);
        title('Temperature') 
       
    frame = getframe(gcf);
    writeVideo(video,frame);

    
    disp(ii/(no_steps - 1));
end
close(video);

