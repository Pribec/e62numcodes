set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)

cmake_minimum_required(VERSION 2.8.12)
project(fluidFlow)
include_directories(${CMAKE_SOURCE_DIR}/../../src/)
link_directories(/usr/lib/x86_64-linux-gnu/hdf5/serial/)
add_subdirectory(${CMAKE_SOURCE_DIR}/../../ build_util)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -O3 -fopenmp")

# be lazy and use all utils libs, you can specify one by one
add_executable(main main.cpp)
target_link_libraries(main pde_utils)
