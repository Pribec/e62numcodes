set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)

cmake_minimum_required(VERSION 2.8.12)
project(lbm)
include_directories(${CMAKE_SOURCE_DIR}/../../src/)
link_directories(/usr/lib/x86_64-linux-gnu/hdf5/serial/)
add_subdirectory(${CMAKE_SOURCE_DIR}/../../ build_util)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -O3")

function (register name)
    add_executable(${name} "${name}.cpp")
    target_link_libraries(${name} pde_utils)
endfunction(register)

register(procedural_lbm)
