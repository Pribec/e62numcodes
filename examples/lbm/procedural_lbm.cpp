#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "domain_relax_engines.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "types.hpp"
#include <omp.h>
#include "integrators.hpp"

using namespace mm;

typedef Vec<double,2> vec_t;
typedef Vec<double,7> pdf_t;

namespace llbm {

void get_d2q7_parameters(Range<double>& w, Range<vec_t>& c, double& csqr)
{

    w.reserve(7);
    c.reserve(7);

    w.push_back(1./2.);
    c.push_back({0.,0.});
    
    double phi0 = 0.0;//0.1*M_PI;
    for (int i = 1; i < 7; ++i)
    {
        w.push_back(1./12.);
        double phi = phi0 + (i-1)*M_PI/3;
        c.push_back(vec_t{std::cos(phi),std::sin(phi)});
    }

    csqr = 1./4.;
}

template< typename vec_t, typename pdf_t >
class BgkEq
{
public:

    static const int dim = vec_t::dimension;  ///< dimension of the domain
    static const int q = pdf_t::dimension;

    explicit BgkEq( Range<double> w, Range<vec_t> c, double csqr ) : 
        w_( w ), c_( c ), csqr_( csqr ) 
    {
        invcsqr = 1./csqr_;
    };

    pdf_t operator()(const double rho, const vec_t u)
    {
        pdf_t feq;
        double usqr = u.squaredNorm();
        for (int i = 0; i < q; ++i)
        {
            double cu = c_[i].dot(u);
            feq[i] = w_[i]*rho*(1.0 + invcsqr*cu + 0.5*invcsqr*invcsqr*cu*cu - 0.5*invcsqr*usqr);
        }
        return feq;
    }

private:

    Range<double> w_;
    Range<vec_t> c_;
    double csqr_;
    double invcsqr;
}; // class BgkEquilibriumDistribution


std::tuple<double,vec_t> getDensityAndVelocity(const pdf_t f, const Range<vec_t> c)
{
    static const int dim = pdf_t::dimension;

    double rho = f.sum();
    
    vec_t u = {0.0,0.0};
    for (int i = 1; i < dim; i++ )
    {
        u[0] += f[i]*c[i][0];
        u[1] += f[i]*c[i][1];
    }
    u[0] /= rho;
    u[1] /= rho;
    return std::make_tuple(rho,u);
}

void getRhoAndU(const Range<vec_t> c, const Range<pdf_t> f, Range<double>& rho, Range<vec_t>& u)
{
    for (int i = 0; i < f.size(); ++i)
    {
        std::tie(rho[i],u[i]) = getDensityAndVelocity(f[i],c);
    }
}


template<typename mlsm_t>
pdf_t cdotnablaf(const Range<vec_t>& c, const mlsm_t& op, 
                 const Eigen::VectorXd& f, int node)
{
    static const int q = pdf_t::dimension;

    int size = f.size()/q;

    pdf_t cnf = 0.0;

    for (int k = 1; k < q; ++k)
    {
        Range<double> f_k = reshape(f.segment(k*size,size)); // this line will kill performance
        cnf[k] = c[k].dot(op.grad(f_k,node));
    }

    return cnf;
}

void rescale_domain(Range<vec_t>& positions, double factor)
{
    for (int i = 0; i < positions.size(); ++i)
    {
        positions[i] *= factor;
    }
}

/*template< typename MLSM_t, typename pdf_t >
class LBMop
{
public:

    // typedefs
    typedef typename MLSM_t::vec_t vec_t;  ///< type of vectors
    typedef typename MLSM_t::scalar_t scalar_t;  ///< type of scalars
    static const int dim = vec_t::dimension;  ///< dimension of the domain
    static const int q = pdf_t::dimension; ///< number of lattice vectors
    
    // constructor
    LBMop(MLSM_t& MLSM) : MLSM_( MLSM ) {
        size_ = MLSM_.getSize();
    }

    Eigen::Matrix< scalar_t, q, dim > fgrad(Eigen::VectorXd f, int node) const
    {
        Eigen::Matrix< scalar_t, q, dim > ret(q,dim);
        ret.setZero();
        for (int k = 0; k < q; ++k)
        {
            Range< scalar_t > fk = reshape( f.segment(k*size_,size_) );
            ret.row( k ) = MLSM_.grad( fk, node );
        }
        return ret;
    }

private:
    MLSM_t MLSM_;
    int size_;
};*/
} // namespace llbm


using namespace llbm;

int main(int arg_num, char* arg[]) {
   
    
    // Create domain
    vec_t centre = {0.0,0.0};
    double radius = 0.5;
    CircleDomain<vec_t> domain(centre,radius);

    int nbounds = 60;
    int ninter = 400;
    domain.fillUniform(ninter, nbounds);

    BasicRelax relax; relax.iterations(200);
    domain.apply(relax);

    Range<int> all = domain.types.filter([](vec_t p) { return true; });
    Range<int> interior = domain.types > 0;
    Range<int> boundary = domain.types < 0;

    Range<int> top_half;
    for (auto b : boundary)
    {
        vec_t p = domain.positions[b];
        if (p[1] > centre[1]) top_half.push_back(b);
    }

    int supp_size = 12;
    domain.findSupport(supp_size,all);

    double dx = radius;
    for (int i = 0; i < domain.distances.size(); ++i)
    {
        double dist = std::sqrt(domain.distances[i][1]);
        dx = std::min(dx,dist);
    }
    prn(dx)

    rescale_domain(domain.positions,1.0/dx);
    domain.findSupport(supp_size,all);

/*    dx = radius/dx;
    for (int i = 0; i < domain.distances.size(); ++i)
    {
        double dist = std::sqrt(domain.distances[i][1]);
        dx = std::min(dx,dist);
    }
    prn(dx)*/

    double sigma = domain.characteristicDistance();
    EngineMLS<vec_t, Monomials, NNGaussians> mls(3,radius/dx);

    auto op = make_mlsm(domain, mls, all, false);

    // initialize fields
    Range<pdf_t> fnew(domain.size(), 0);
    Range<pdf_t> fold(domain.size(), 0);
    
    Range<double> density(domain.size(), 1.);
    Range<vec_t> velocity(domain.size(), vec_t{0.,0.});

    Range<double> w; // weights
    Range<vec_t> c; // velocity lattice
    double csqr; // speed of sound

    get_d2q7_parameters(w,c,csqr); // D2Q7
    prn(w)
    prn(c)
    prn(csqr)

    double Re = 1;
    double umax = 0.1;
    double nu = Re/(umax*(radius/dx));
    double dt = 0.1/nu; //0.1*dx^2
    csqr = 0.25*(1*1)/(dt*dt);
    double omega = 1./(nu/csqr/dt + 0.0);

    prn(Re)
    prn(umax)
    prn(nu)
    prn(dt)
    prn(dx)
    prn(csqr)
    prn(omega)
    double courant = umax*dt/1.0;
    prn(courant)

/*    for (int i = 0; i < c.size(); ++i)
    {
        c[i] *= dt/1.0;
    }*/
    prn(c)
    BgkEq<vec_t,pdf_t> feq(w,c,csqr); // initalize equilibrium function
    
    // initalize internal nodes
    for (auto i : interior)
    {
        fold[i] = feq(density[i],velocity[i]);
    }

    // initalize boundary nodes
    for (auto i : boundary)
    {
        fold[i] = feq(density[i],velocity[i]); // currently set to zero
    }

    Eigen::Rotation2D<double> rot2(M_PI/2.);

    for (auto i : top_half)
    {
        vec_t p = domain.positions[i];
        p /= p.norm();
        vec_t t = rot2*p;
        fold[i] = feq(1.0,umax*vec_t{t[0],std::abs(t[1])});
    }
    fnew = fold;

    getRhoAndU(c,fnew,density,velocity);

    auto df_dt = [&](double, const Eigen::VectorXd & f)
    {
        Range<pdf_t> rf = reshape<7>(f); // transform vecX into range
        int n = rf.size();
        Range<pdf_t> der(n,pdf_t(0.0)); // initialize derivatives to zero

        for (auto j : interior) {
            double rho; vec_t u;
            std::tie(rho,u) = getDensityAndVelocity(rf[j],c);

            pdf_t fneq = rf[j] - feq(rho,u);
            pdf_t adv = cdotnablaf(c,op,f,j);
            der[j] = -omega*fneq - adv;
        }
        return reshape(der);
    };

    //prn(feq(1.0,vec_t{0.1,0.0}))

    double time = 1.0;

    int t_steps = 20000;//(int)(time/dt);

    auto integrator = integrators::Explicit::Midpoint().solve(
            df_dt, 0.0, t_steps*dt, dt, reshape(fold));

    auto stepper = integrator.begin();

    for (int step= 0; step <= t_steps; ++step)
    {
        ++stepper;
        fnew = reshape<7>(stepper.value());
        fold.swap(fnew);

    }

    getRhoAndU(c,fnew,density,velocity);
    prn(domain.types)
    prn(domain.positions)
    prn(velocity)
    prn(density)

}