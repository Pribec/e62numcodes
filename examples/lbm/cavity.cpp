#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "overseer.hpp"
#include "types.hpp"
#include <omp.h>
#include "integrators.hpp"
#include "lbm.hpp"

using namespace mm;
using namespace lbm;

typedef Vec2d vec_t;
typedef double scal_t;
typedef        pdf_t;

Overseer<scal_t, vec_t> O;

int main(int arg_num, char* arg[]) {
    O.timings.addCheckPoint("time_start");
    O(arg_num, arg);

    O.timings.addCheckPoint("domain");
    RectangleDomain<vec_t> domain(O.domain_lo, O.domain_hi);  // domain definition

    domain.fillUniformWithStep(O.d_space, O.d_space);

    Range<int> all = domain.types.filter([](vec_t p) { return true; });
    Range<int> interior = domain.types > 0;
    Range<int> boundary = domain.types < 0;

    Range<int> top = domain.positions.filter([](vec_t p) { return p[1] == 1.0; });
    Range<int> bottom = domain.positions.filter([](vec_t p) { return p[1] == 0.0; });
    Range<int> right = domain.positions.filter([](vec_t p) { return p[0] == 1.0; });
    Range<int> left = domain.positions.filter([](vec_t p) { return p[0] == 0.0; });

    Range<int> walls = (right.append(bottom)).append(left);

    // init state
    Range<pdf_t> f1(domain.size(), 0);
    Range<pdf_t> f2(domain.size(), 0);
    Range<scal_t> density(domain.size(), 0);
    Range<vec_t> velocity(domain.size(), 0);

    for (auto i : internal) {
        f2[i].initWithEquilibrium(1.0,0.0);
    }

    // boundary conditions
    for (auto i : top) {
        f2[i].initWithEquilibrium(1.0,vec_t(1.0,0.0));
    }

    for (auto i : walls) {
        f2[i].initWithEquilibrium(1.0,0.0);
    }

    f1 = f2;

    O.timings.addCheckPoint("setup");

    EngineMLS<vec_t, Monomials, NNGaussians> mls
            (O.m,
             O.sigmaW);
    auto op = make_mlsm(domain, mls, all, false);
    auto lbmop = LBMop(mlsm);

    // LBM settings
    BgkEquilibriumDistribution feq;
    scal_t omega =  1.0;

    O.timings.addCheckPoint("time loop");

    auto df_dt = [&](double, const VectorXd & f)
    {
        Range<pdf_t> rf = reshape<>(f);
        int n = rf.size();

        Range<pdf_t> der(n,pdf_t(0.0));

        for (auto j : interior) {
            scal_t rho; vec_t u;
            rho = rf[j].getDensityAndVelocity(u);
            pdf_t fneq = rf[j] - feq(rho,u);
            der[j] = -omega*fneq - ci*lbmop.fgrad(f,j);
        }
        return reshape(der);
    }

    auto integrator_f = integrators::Explicit::RK4().solve(
            df_dt, 0.0, O.time, O.dt, reshape(f1));

    auto f_stepper = integrator_f.begin();

    for (int step = 0; step <= O.t_steps; ++step) {
        ++f_stepper;
        f2 = reshape<2>(f_stepper.value());

        // time step
        f1.swap(f2);
    }