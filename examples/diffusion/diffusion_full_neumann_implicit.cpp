/*
 * The equation we would like to solve is
 *  laplace u = f
 *  du / dn = g
 *  with additional constraint  integral u dx = 0 for uniqueness
 *  Case is valid if integral f = integral g (sum of sources = sum of losses)
 */

#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "types.hpp"
#include <Eigen/Sparse>
#include <Eigen/SparseLU>

using namespace mm;
using namespace Eigen;
using namespace std;

int main() {
    int support_size = 12;  // support size
    int m = 3;  // monomial basis of second order, i.e. 6 monomials
    int nx = 10;  // num of discretization nodes
    double sigmaW = 1;  // weight shape

    // prep domain
    Vec2d low = {0, 0}, high = {1, 1};
    RectangleDomain<Vec2d> domain(low, high);
    domain.fillUniformWithStep(1. / nx, 1. / nx);
    domain.findSupport(support_size);
    int N = domain.size();

    double tol = 1e-10;
    Range<int> internal = domain.types > 0, boundary = domain.types < 0,
               top = domain.positions.filter(
                   [=](const Vec2d&p) { return std::abs(p[1] - high[1]) < tol; }),
               bottom = domain.positions.filter(
                   [=](const Vec2d&p) { return std::abs(p[1] - low[1]) < tol; }),
               right = domain.positions.filter([=](const Vec2d&p) {
                   return std::abs(p[0] - high[0]) < tol && std::abs(p[1] - high[1]) > tol &&
                          std::abs(p[1] - low[1]) > tol;
               }),
               left = domain.positions.filter([=](const Vec2d&p) {
                   return std::abs(p[0] - low[0]) < tol && std::abs(p[1] - high[1]) > tol &&
                          std::abs(p[1] - low[1]) > tol;
               }),
               all = domain.types != 0;

    EngineMLS<Vec2d, Monomials, NNGaussians> mls(m, sigmaW);
    auto op = make_mlsm<mlsm::d1 | mlsm::lap>(domain, mls, domain.types != 0, false);

    typedef SparseMatrix<double> matrix_t;
    matrix_t M(N + 1, N + 1);

    Range<int> size_per_row(N + 1, support_size + 1);
    size_per_row.back() = N;
    M.reserve(size_per_row);
    VectorXd rhs(N + 1);

    // case is valid
    for (int i : internal) {
        op.lap(M, i, 1.0);
        rhs(i) = 4;
    }
    for (int i : left) {
        op.der1(M, 0, 0, i, -1.0);
        rhs(i) = 1;
    }
    for (int i : right) {
        op.der1(M, 0, 0, i, 1.0);
        rhs(i) = 1;
    }
    for (int i : top) {
        op.der1(M, 0, 1, i, 1.0);
        rhs(i) = 1;
    }
    for (int i : bottom) {
        op.der1(M, 0, 1, i, -1.0);
        rhs(i) = 1;
    }
    // integral
    for (int i = 0; i < N; ++i) {
        M.coeffRef(N, i) = 1;
        M.coeffRef(i, N) = 1;
    }
    rhs(N) = 0;

    M.makeCompressed();

    SparseLU<matrix_t> solver;
    solver.compute(M);
    VectorXd sol = solver.solve(rhs);

    // draw
    VectorXd solu = -sol.head(N);
    cout << "Lagrange multiplier must be zero!" << endl;
    prn("lagrange multiplier", sol.tail(1));
    std::thread th([&] { draw2D(domain.positions, solu); });
    th.join();

    return 0;
}
