(* ::Package:: *)

tsteps
Import["data.m"]
Graphics3D[{PointSize[0.02], Point[pos, VertexColors->
  Map[Function[x,Hue[x,1,1,1-x]], (1 - Rescale[T])*2/3]]}]
