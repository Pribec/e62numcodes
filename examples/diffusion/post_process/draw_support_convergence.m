(* ::Package:: *)

sigmas = {0.000001, 0.000025, 0.000100, 0.000400, 0.000625, 0.000900, 0.001600, 0.002500, 0.003600, 0.010000, 1.000000};
support = {6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 25, 30, 50, 100};
errors = {{0.973746, 0.973746, 0.973746, 0.973746, 0.973746, 0.973746, 0.973746, 0.973746, 0.973746, 0.973746, 0.973746, 0.973746, 0.973746, 0.973746, 0.973746, 0.973746, 0.973746, 0.973746, 0.973746, 0.973746}, {0.002589, 0.002589, 0.002589, 0.002589, 0.002589, 0.002589, 0.002589, 0.002589, 0.002589, 0.002589, 0.002589, 0.002589, 0.002589, 0.002589, 0.002589, 0.002589, 0.002589, 0.002589, 0.002589, 0.002589}, {0.002589, 0.002580, 0.002586, 0.002544, 0.002544, 0.002544, 0.002544, 0.002545, 0.002545, 0.002545, 0.002545, 0.002545, 0.002545, 0.002545, 0.002545, 0.002545, 0.002545, 0.002545, 0.002545, 0.002545}, {0.002589, 0.002455, 0.002805, 0.002077, 0.002596, 0.003291, 0.003994, 0.004128, 0.004300, 0.004360, 0.004533, 0.004408, 0.004696, 0.004800, 0.004836, 0.004765, 0.004792, 0.004878, 0.004940, 0.004941}, {0.002589, 0.002972, 0.002899, 0.001982, 0.015163, 0.233770, 0.505839, 0.360197, 0.315508, 0.144419, 0.039536, 0.017127, 0.006577, 0.006530, 0.006697, 0.006029, 0.005967, 0.006536, 0.007130, 0.007170}, {0.002589, 0.004178, 0.002984, 0.982385, 63.568543, 436.705891, 181.945296, 59.683158, 23.418915, 5.673995, 1.329218, 0.446128, 0.103085, 0.041790, 0.015162, 0.008138, 0.008396, 0.007922, 0.008551, 0.008969}, {0.002589, 0.009050, 0.017877, 6970.186516, 85632.083125, 103196.427764, 9122.880826, 1099.375565, 206.582582, 48.897883, 12.468484, 8.436095, 1.055393, 0.727792, 0.197053, 0.112464, 0.335321, 0.077541, 0.022338, 0.021787}};


data = Table[Transpose[{support, Log[10,ee]}], {ee, errors}];
ListPlot[data,
  PlotRange -> {{5, 60},{-3, 6}},
  Frame -> True,
  FrameLabel -> {HoldForm[n],  HoldForm[Log[10,  error]]},
  PlotLabel -> HoldForm[Error with respect to support size],
  LabelStyle -> {FontFamily -> "Verdana",  GrayLevel[0]},
  PlotTheme -> "Scientific",
  GridLines->Automatic,
  PlotLegends -> Placed[
    PointLegend[Sqrt[sigmas],
      LegendFunction -> (Grid[{{#}},Frame -> True, FrameStyle -> Thickness[10^-4], Background->White]&),
      LegendLabel -> "N = 50\n\[Sigma]",
      LegendMarkerSize->10],
    {Right,Bottom}]
  ]
data = Table[Transpose[{Sqrt[sigmas], ee}], {ee, Transpose[errors]}];
ListPlot[Log[10, data],
  PlotRange -> {-1, -3.5},
  Frame -> True,
  FrameLabel -> {HoldForm[Log[10, \[Sigma]]],  HoldForm[Log[10,  error]]},
  PlotLabel -> HoldForm[Error with respect to support size],
  LabelStyle -> {FontFamily -> "Verdana",  GrayLevel[0]},
  PlotTheme -> "Scientific",
  GridLines->Automatic,
  PlotLegends -> Placed[
    PointLegend[support,
      LegendFunction -> (Grid[{{#}},Frame -> True, FrameStyle -> Thickness[10^-4], Background->White]&),
      LegendLabel -> "Support size",
      LegendMarkerSize->10],
    {Right,Bottom}]
  ]



