prepare

casename = 'one_domain_implicit_wip';
datafile = [datapath casename '.h5'];
pos = h5read(datafile, '/domain/pos');

R1 = h5readatt(datafile, '/', 'R1');
R2 = h5readatt(datafile, '/', 'R2');
lam1 = h5readatt(datafile, '/', 'lam1');
lam2 = h5readatt(datafile, '/', 'lam2');
q0 = h5readatt(datafile, '/', 'q0');

x = pos(1, :);
y = pos(2, :);
sol = h5read(datafile, '/sol');

setfig('b1');
x = double(x);
y = double(y);
v = double(sol);
xden = linspace(min(x), max(x), 501);
yden = linspace(min(y), max(y), 501);
[X,Y] = meshgrid(xden, yden);
I = find(abs(Y) < 1e-4);
Z = griddata(x, y, v, X, Y, 'linear');

[c, h] = contour(X, Y, Z, 'LineColor', 'r');
clabel(c, h);

casename = 'two_domain_implicit_wip';
datafile = [datapath casename '.h5'];

TR1 = h5readatt(datafile, '/', 'R1');
TR2 = h5readatt(datafile, '/', 'R2');
Tlam1 = h5readatt(datafile, '/', 'lam1');
Tlam2 = h5readatt(datafile, '/', 'lam2');
Tq0 = h5readatt(datafile, '/', 'q0');
assert(TR1 == R1, '%f != %f', TR1, R1);
assert(TR2 == R2, '%f != %f', TR2, R2);
assert(Tlam1 == lam1, '%f != %f', Tlam1, lam1);
assert(Tlam2 == lam2, '%f != %f', Tlam2, lam2);
assert(Tq0 == q0, '%f != %f', Tq0, q0);

pos = h5read(datafile, '/domain/pos');
x = pos(1, :);
y = pos(2, :);
types = h5read(datafile, '/domain/types');
N = length(x);

pos2 = h5read(datafile, '/circ/pos');
x2 = pos2(1, :);
y2 = pos2(2, :);
N2 = length(x2);

sol = h5read(datafile, '/sol');

Z2 = griddata(double([x x2]), double([y y2]), sol, X, Y, 'linear');
[c, h] = contour(X, Y, Z2, 'LineColor', 'k');
clabel(c, h);
legend('one domain', 'two domain')

setfig('b3');
plot(X(I), Z(I), '-'); 
plot(X(I), Z2(I), '-');

A = heat_analytical(X(I), 0*X(I), R1, R2, lam1, lam2, q0);
plot(X(I), A, '-');

legend('one domain', 'two domain', 'analytical')