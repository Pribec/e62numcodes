prepare
format compact

casename = '22';
casename = '51';
% casename = '15';
casename1 = ['one_domain_implicit_' casename];
casename2 = ['two_domain_implicit_' casename];
datafile1 = [datapath casename1 '.h5'];
datafile2 = [datapath casename2 '.h5'];

R1 = h5readatt(datafile1, '/', 'R1');
R2 = h5readatt(datafile1, '/', 'R2');
lam1 = h5readatt(datafile1, '/', 'lam1');
lam2 = h5readatt(datafile1, '/', 'lam2');
q0 = h5readatt(datafile1, '/', 'q0');
TR1 = h5readatt(datafile2, '/', 'R1');
TR2 = h5readatt(datafile2, '/', 'R2');
Tlam1 = h5readatt(datafile2, '/', 'lam1');
Tlam2 = h5readatt(datafile2, '/', 'lam2');
Tq0 = h5readatt(datafile2, '/', 'q0');
assert(TR1 == R1, '%f != %f', TR1, R1);
assert(TR2 == R2, '%f != %f', TR2, R2);
assert(Tlam1 == lam1, '%f != %f', Tlam1, lam1);
assert(Tlam2 == lam2, '%f != %f', Tlam2, lam2);
assert(Tq0 == q0, '%f != %f', Tq0, q0);

info1 = h5info(datafile1);
ng1 = length(info1.Groups);
data1 = zeros(ng1, 2);
for i = 1:ng1
    name = info1.Groups(i).Name
    pos = h5read(datafile1, [name '/domain/pos']);
    x = pos(1, :);
    data1(i, 1) = length(x);
    data1(i, 2) = h5readatt(datafile1, [name '/timer'], 'total');
end

info2 = h5info(datafile2);
ng2 = length(info2.Groups);
data2 = zeros(ng2, 2);
for i = 1:ng2
    name = info2.Groups(i).Name
    pos = h5read(datafile2, [name '/domain/pos']);
    x = pos(1, :);
    pos2 = h5read(datafile2, [name '/circ/pos']);
    x2 = pos2(1, :);
    data2(i, 1) = length(x)+length(x2);
    data2(i, 2) = h5readatt(datafile2, [name '/timer'], 'total');
end


f1 = setfig('b1');
plot(data1(:, 1), data1(:, 2), 'o-')
plot(data2(:, 1), data2(:, 2), 'x-')
set(gca, 'xscale', 'log')
set(gca, 'yscale', 'log')
title(sprintf('time, $\\lambda_1 = %.2f$, $\\lambda_2 = %.2f$', lam1, lam2))
xlabel('$N$')
ylabel('seconds')
legend('one domain', 'two domain', 'Location', 'NW')

exportfig(f1, [imagepath 'time_' casename '.pdf'])
