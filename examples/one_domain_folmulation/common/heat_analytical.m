function v = heat_analytical(x, y, R1, R2, lam1, lam2, q0)
r = sqrt(x.^2+y.^2);
v = zeros(size(r));
s = r <= R1;
v(s) = q0/lam1/9*(R1^3-r(s).^3 + 3*lam1/lam2*R1^3*log(R2/R1));
% v(s) = q0/4/*(R1^2-r(s).^2 + 2*lam1/lam2*R1^2*log(R2/R1));
l = R1 < r & r <= R2;
v(l) = q0/3/lam2*R1^3*log(R2./r(l));
 %v(l) = lam1*q0/2/lam2*R1^2*log(R2./r(l));
end