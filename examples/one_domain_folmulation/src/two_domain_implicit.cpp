#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "domain_fill_engines.hpp"
#include "domain_relax_engines.hpp"
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "types.hpp"
#include "io.hpp"
#include <Eigen/Sparse>
#include <Eigen/SparseLU>
#include <Eigen/PardisoSupport>

using namespace std;
using namespace mm;
using namespace Eigen;

class Solver {
  public:
    template<typename vec_t, template<class> class domain_t, template<class> class domain_t2>
    Eigen::VectorXd solve(domain_t<vec_t>& domain, domain_t2<vec_t>& circ, const Range<int>& idx_map,
                          const XMLloader& conf, HDF5IO& out_file, Timer& timer) {
        int basis_size = conf.get<int>("params.mls.m");
        double basis_sigma = conf.get<double>("params.mls.sigmaB");
        double weight_sigma = conf.get<double>("params.mls.sigmaW");

        string basis_type = conf.get<string>("params.mls.basis_type");
        string weight_type = conf.get<string>("params.mls.weight_type");

        if (basis_type == "gau") {
            if (weight_type == "gau") {
                return solve_(domain, circ, idx_map, conf, out_file, timer, NNGaussians<vec_t>(basis_sigma, basis_size),
                              NNGaussians<vec_t>(weight_sigma));
            } else if (weight_type == "mon") {
                return solve_(domain, circ, idx_map, conf, out_file, timer, NNGaussians<vec_t>(basis_sigma, basis_size),
                              Monomials<vec_t>(1));
            }
        } else if (basis_type == "mon") {
            if (weight_type == "gau") {
                return solve_(domain, circ, idx_map, conf, out_file, timer, Monomials<vec_t>(basis_size),
                              NNGaussians<vec_t>(weight_sigma));
            } else if (weight_type == "mon") {
                return solve_(domain, circ, idx_map, conf, out_file, timer, Monomials<vec_t>(basis_size),
                              Monomials<vec_t>(1));
            }
        }
        assert_msg(false, "Unknown basis type '%s' or weight type '%s'.", basis_type, basis_sigma);
        throw "";
    }

  private:
    template<typename domain_t, typename basis_t, typename weight_t, class domain_t2>
    Eigen::VectorXd solve_(domain_t& domain, domain_t2& circ, const Range<int>& idx_map,
                           const XMLloader& conf, HDF5IO& out_file, Timer& timer,
                           const basis_t& basis, const weight_t& weight) {
        int N = domain.size();
        int N2 = circ.size();
        int support_size = conf.get<int>("params.mls.n");

        double a = conf.get<double>("params.case.a");
        double r = conf.get<double>("params.case.r");
        double lam_inner = conf.get<double>("params.case.lam_inner");
        double lam_outer = conf.get<double>("params.case.lam_outer");
        double q0 = conf.get<double>("params.case.q0");
        auto mls = make_mls(basis, weight);
        timer.addCheckPoint("shapes");
        RaggedShapeStorage<domain_t, decltype(mls), mlsm::lap|mlsm::d1> storage(
                domain, mls, domain.types != 0, false);
        MLSM<decltype(storage)> op(storage);  // All nodes, including boundary

        RaggedShapeStorage<domain_t2, decltype(mls), mlsm::lap|mlsm::d1> storage2(
                circ, mls, circ.types != 0, false);
        MLSM<decltype(storage2)> op2(storage2);  // All nodes, including boundary

        timer.addCheckPoint("matrix");
        SparseMatrix<double, RowMajor> M(N+N2, N+N2);
        Range<int> per_row = storage.support_sizes().join(storage2.support_sizes());
        // common boundary
        for (int i : (circ.types < 0)) {
            per_row[i] = storage2.support_size(i) + storage.support_size(idx_map[i]);
            per_row[idx_map[i]] = 2;
        }
        M.reserve(per_row);
        Eigen::VectorXd rhs = Eigen::VectorXd::Zero(N+N2);

        // heat eq. on outer
        for (int i : (domain.types > 0)) {
            op.lap(M, i, lam_outer);  // laplace in interior
            rhs(i) = 0;
        }
        // outside is at 0
        for (int i : (domain.types == -1)) {
            op.value(M, i);
            rhs(i) = 0;
        }

        // heat eq. on interior circle
        auto block2 = M.bottomRightCorner(N2, N2);
        for (int i : (circ.types > 0)) {
            op2.lap(block2, i, lam_inner);  // laplace in interior
            rhs(N+i) = -q0*circ.positions[i].norm();
        }
        // common boundary
        for (int i : (circ.types < 0)) {
            M.coeffRef(idx_map[i], idx_map[i]) = 1;
            M.coeffRef(idx_map[i], N+i) = -1;
            rhs(idx_map[i]) = 0;   //  same value

            op.neumann_implicit(M, idx_map[i], domain.normal(idx_map[i]), lam_outer, N+i);
            op2.neumann_implicit(block2, i, circ.normal(i), lam_inner);
            rhs(N+i) = 0;
        }

        SparseMatrix<double> M2(M);
        M2.makeCompressed();

//        out_file.reopenFile();
//        out_file.reopenFolder();
//        out_file.setSparseMatrix("M", M2);
//        out_file.setDoubleArray("rhs", rhs);
//        out_file.closeFile();

//        double droptol = conf.get<double>("params.solver.droptol");
//        int fill_factor = conf.get<int>("params.solver.fill_factor");
//        double errtol = conf.get<double>("params.solver.errtol");
//        int maxiter = conf.get<int>("params.solver.maxiter");
//        BiCGSTAB<SparseMatrix<double, RowMajor>, IncompleteLUT<double>> solver;
//        solver.preconditioner().setDroptol(droptol);
//        solver.preconditioner().setFillfactor(fill_factor);
//        solver.setMaxIterations(maxiter);
//        solver.setTolerance(errtol);
//        PardisoLU<SparseMatrix<double>> solver;
        timer.addCheckPoint("solve");
        SparseLU<SparseMatrix<double>> solver;
        solver.compute(M2);
        return solver.solve(rhs);
    }
};

int main() {
    XMLloader conf("params/two_domain_implicit.xml");
    std::string name = conf.get<std::string>("params.meta.filename");
    HDF5IO file("data/"+name+"_wip.h5", HDF5IO::DESTROY);

    double a = conf.get<double>("params.case.a");
    double r = conf.get<double>("params.case.r");
    double lam_inner = conf.get<double>("params.case.lam_inner");
    double lam_outer = conf.get<double>("params.case.lam_outer");
    double q0 = conf.get<double>("params.case.q0");
    file.openFolder("/");
    file.setDoubleAttribute("R1", r);
    file.setDoubleAttribute("R2", a);
    file.setDoubleAttribute("lam1", lam_inner);
    file.setDoubleAttribute("lam2", lam_outer);
    file.setDoubleAttribute("q0", q0);
    file.closeFile();

    vector<string> nxs = split(conf.get<std::string>("params.num.nxs"), ",");
    for (const string& snx : nxs) {
        int nx = std::stoi(snx);
        prn(nx);
        Timer timer;
        timer.addCheckPoint("begin");

        double dx = 2. * a / nx;
        CircleDomain<Vec2d> domain(0, a);
        domain.fillUniformBoundaryWithStep(dx);

        CircleDomain<Vec2d> circ(0, r);
        circ.fillUniformBoundary(circ.surface_area() / dx);
        circ.types = -2;

        PoissonDiskSamplingFill fill;
        circ.apply(fill, dx);
        Range<int> idx_map = domain.subtract(circ);
        domain.apply(fill, dx);

        BasicRelax relax;
        int riter = conf.get<int>("params.num.riter");
        relax.iterations(riter).projectionType(0).initialHeat(0.5);
        domain.apply(relax, dx);
        circ.apply(relax, dx);

//    double heat = conf.get<double>("params.num.heat");
//    int riter = conf.get<int>("params.num.riter");
//    DiffuseAnnealingRelax relax;
//    relax.iterations(riter).numNeighbours(3).withInitHeat(heat);
//    domain.apply(relax);
//    circ.apply(relax);

        int support_size = conf.get<int>("params.mls.n");
        domain.findSupport(support_size);
        circ.findSupport(support_size);

        int N = domain.size();
        prn(N);

        file.reopenFile();
        file.openFolder(format("/%04d", nx));
        file.setDomain("domain", domain);
        file.setDomain("circ", circ);
        file.setIntArray("idx_map", idx_map);
        file.closeFile();

        Solver solver;
        auto solution = solver.solve(domain, circ, idx_map, conf, file, timer);

        file.reopenFile();
        file.reopenFolder();
        file.setDoubleArray("sol", solution);
        file.closeFile();

        timer.addCheckPoint("end");
        prn(timer.getTime("begin", "end"));

        file.reopenFile();
        file.reopenFolder();
        file.setTimer("timer", timer);
        file.closeFile();
    }

    return 0;
}
