#include "domain.hpp"
#include "domain_extended.hpp"
#include "draw.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include <Eigen/Sparse>

using namespace mm;
using namespace Eigen;

int main(int argc, char* argv[]) {
    // Physical parameters
    const double E = 72.1e9;
    const double v = 0.33;
    const double P = 1000;
    const double D = 5;
    const double L = 30;
    // Derived parameters
    const double I = D*D*D/12;
    double lam = E * v / (1 - 2 * v) / (1 + v);
    const double mu = E / 2 / (1 + v);
    lam = 2*mu*lam / (2*mu + lam);  // plane stress

    // Closed form solution -- refer to cantilever_beam.nb for reference.
    std::function<Vec2d(Vec2d)> analytical = [=](const Vec2d& p) {
        double x = p[0], y = p[1];
        double ux = (P*y*(3*D*D*(1+v) - 4*(3*L*L - 3*x*x + (2 + v)*y*y)))/(2.*D*D*D*E);
        double uy = -(P*(3*D*D*(1+v)*(L-x) + 4*(L-x)*(L-x)*(2*L+x) + 12*v*x*y*y))/(2.*D*D*D*E);
        return Vec2d(ux, uy);
    };

    // Domain definition
    Vec2d low(0, -D/2), high(L, D/2);
    RectangleDomain<Vec2d> domain(low, high);
    double step = L / 100;
    domain.fillUniformWithStep(step, step);
    int N = domain.size();

    // Set indices for different domain parts
    double tol = 1e-10;
    Range<int> internal = domain.types > 0,
               boundary = domain.types < 0,
               top    = domain.positions.filter([=](const Vec2d &p) {
                            return std::abs(p[1] - high[1]) < tol;
                       }),
               bottom = domain.positions.filter([=](const Vec2d &p) {
                            return std::abs(p[1] - low[1]) < tol;
                        }),
               right  = domain.positions.filter([=](const Vec2d &p) {
                            return std::abs(p[0] - high[0]) < tol &&
                                   std::abs(p[1] - high[1]) > tol &&
                                   std::abs(p[1] - low[1]) > tol;
                        }),
               left   = domain.positions.filter([=](const Vec2d &p) {
                            return std::abs(p[0] - low[0]) < tol &&
                                   std::abs(p[1] - high[1]) > tol &&
                                   std::abs(p[1] - low[1]) > tol;
                        }),
               all = domain.types != 0;
    int support_size = 9;
    domain.findSupport(support_size);

    // Approximation
    double sigmaW = 1.0;
    Monomials<Vec2d> mon9({{0, 0}, {0, 1}, {0, 2}, {1, 0}, {1, 1}, {1, 2}, {2, 0}, {2, 1}, {2, 2}});
    NNGaussians<Vec2d> weight(sigmaW * domain.characteristicDistance());
    EngineMLS<Vec2d, Monomials, NNGaussians> mls(mon9, weight);

    // Initialize operators on all nodes
    auto op = make_mlsm(domain, mls, all);

    typedef SparseMatrix<double, RowMajor> matrix_t;
    matrix_t M(2 * N, 2 * N);
    M.reserve(Range<int>(2*N, 2 * support_size));
    VectorXd rhs(2*N);

    // Set equation on interior
    for (int i : internal) {
        op.graddiv(M, i, lam + mu);  // graddiv + laplace in interior
        op.lapvec(M, i,  mu);
        rhs(i) = 0;
        rhs(i+N) = 0;
    }

    // Set bottom boundary conditions - traction free
    for (int i : bottom) {
        op.der1(M, 0, 1, i, mu, 0);
        op.der1(M, 1, 0, i, mu, 0);
        rhs(i) = 0;
        op.der1(M, 0, 0, i, lam, 1);
        op.der1(M, 1, 1, i, 2.*mu+lam, 1);
        rhs(i+N) = 0;
    }

    // Set left boundary conditions - given traction
    for (int i : left) {
        op.der1(M, 1, 1, i, lam, 0);
        op.der1(M, 0, 0, i, 2.*mu+lam, 0);
        rhs(i) = 0;
        double y = domain.positions[i][1];
        op.der1(M, 0, 1, i, mu, 1);
        op.der1(M, 1, 0, i, mu, 1);
        rhs(i+N) = P*(D*D - 4*y*y)/(8.*I);
    }

    // Set right boundary conditions - given displacement
    for (int i : right) {
        double y = domain.positions[i][1];
        M.coeffRef(i,  i) = 1;
        rhs(i) = (P*y*(3*D*D*(1 + v) - 4*(2 + v)*y*y))/(24.*E*I);
        M.coeffRef(i+N,  i+N) = 1;
        rhs(i+N) = -(L*v*P*y*y)/(2.*E*I);
    }

    // set top boundary conditions - traction free
    for (int i : top) {
        op.der1(M, 0, 1, i, mu, 0);
        op.der1(M, 1, 0, i, mu, 0);
        rhs(i) = 0;
        op.der1(M, 0, 0, i, lam, 1);
        op.der1(M, 1, 1, i, 2.*mu+lam, 1);
        rhs(i+N) = 0;
    }
    M.makeCompressed();

    BiCGSTAB<matrix_t, IncompleteLUT<double>> solver;
    solver.preconditioner().setDroptol(1e-5);
    solver.preconditioner().setFillfactor(20);
    solver.setMaxIterations(300);
    solver.setTolerance(1e-15);
    solver.compute(M);
    VectorXd sol = solver.solve(rhs);

    Range<Vec2d> displacement(N, 0);
    std::vector<std::array<double, 3>> stress_field(N);
    for (int i : all) {
        displacement[i] = Vec2d({sol[i], sol[i+N]});
    }
    for (int i : all) {
        auto grad = op.grad(displacement, i);
        stress_field[i][0] = (2*mu + lam)*grad(0, 0) + lam*grad(1, 1);
        stress_field[i][1] = lam*grad(0, 0) + (2*mu+lam)*grad(1, 1);
        stress_field[i][2] = mu*(grad(0, 1)+grad(1, 0));
    }

    Range<double> error(N);
    double maxuv = 0;
    for (int i = 0; i < N; ++i) {
        Vec2d uv = analytical(domain.positions[i]);
        maxuv = std::max(maxuv, std::max(std::abs(uv[0]), std::abs(uv[1])));
        error[i] = std::max(std::abs(uv[0] - sol[i]), std::abs(uv[1] - sol[i + N]));
    }

    double L_inf_error = *std::max_element(error.begin(), error.end()) / maxuv;
    prn(L_inf_error);
    std::cout << "If you choose a smaller discretization step, the error decreases." << std::endl;
}

