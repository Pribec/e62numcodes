# Problems

We will be solving 3 increasingly difficult linear elasticity problems:

* Point Contact
* Cantilever Beam
* Hertzian Contact

An in-depth description of the problems can be found [here](http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Solid_Mechanics#Case_studies).
Each problem is solved in the corresponding file using implicit MLSM method.
Each problem also has an accompanying notebook demonstrating the correctness fo the closed form
solution.
